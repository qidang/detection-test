import _init
import sys
import os
import argparse
import pickle
import platform
import progressbar
import numpy as np
import datetime
from tools import Logger
import torchvision
import math
from torch import nn
import json
import random

from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval
import datetime

from PIL.ImageDraw import Draw

from rcnn_config import config
from tools import torch_tools
from data import data_feeder

from data.datasets import COCOPersonDataset, ModanetDataset
from data.data_loader import DataLoader
from data.collate import yolo_collate
from rcnn_dnn.networks import networks
from rcnn_dnn.data.collate_fn import collate_fn, CollateFnRCNN
from rcnn_dnn.data.transforms import RandomMirror

import torch
torch.multiprocessing.set_sharing_strategy('file_system')
#import torchvision.transforms as transforms
import torch.optim as optim
from torchcore.data.transforms import Compose, Resize, ResizeAndPadding, ResizeMinMax, ToTensor, Normalize

from dnn import trainer
from benchmark import Benchmark

from torchcore.dnn.networks.faster_rcnn import FasterRCNN

def parse_commandline():
    parser = argparse.ArgumentParser(description="Training the Model")
    #parser.add_argument('-c','--config',help='Configuration Name', required=True)
    #parser.add_argument('--patchsize',help='Patch Size', required=True)
    parser.add_argument('-b','--batch_size',help='Batch size', required=True)
    parser.add_argument('-t','--tag',help='Model tag', required=True)
    #parser.add_argument('-g','--gpu',help='GPU Index', default='0')
    #parser.add_argument('--datasetpath',help='Path to the dataset',required=True)
    #parser.add_argument('--projectpath',help='Path to the project',required=True)
    return vars(parser.parse_args())

class my_trainer(trainer):
    def __init__( self, cfg, model, device, trainset, testset=None, benchmark=None, criterias=None ):
        self._cfg = cfg
        self._device = device
        self._optimizer = None
        self._model = model

        self._trainset = trainset
        self._testset = testset
        self._benchmark = benchmark
        self._criterias = criterias
        #self._trainset_feeder = data_feeder( trainset )

        #if testset is not None :
        #    self._testset_feeder = data_feeder( testset )

        self._model['net'].to(device)
        self._set_optimizer()

        self.init_logger()

    def train( self ):
        #if self._testset is not None :
        #    self._validate()

        for i in range( self._niter ):
            print("Epoch %d/%d" % (i+1,self._niter))
            self._logger.info('epoch {}\n'.format(i))
            self._train()

            if self._testset is not None :
                self._validate()

    def train_one_epoch(self):
        self._train()

    def validate_onece(self):
        self._validate()

    def _validate( self ):
        print('start to validate')
        self._model.eval()

        results = []
        with torch.no_grad() :
            for idx,(inputs, targets) in enumerate(self._testset):
                inputs = self._set_device( inputs )
                output = self._model( inputs)
                for i, im in enumerate(output):
                    if len(im['boxes']) == 0:
                        continue
                    # convert to xywh
                    im['boxes'][:,2] -= im['boxes'][:,0]
                    im['boxes'][:,3] -= im['boxes'][:,1]
                    for j in range(len(im['boxes'])):
                        results.append({'image_id':targets[i]['image_id'].detach().cpu().numpy().tolist(), 
                                        'category_id':im['labels'][j].cpu().numpy().tolist(), 
                                        'bbox':im['boxes'][j].cpu().numpy().tolist(), 
                                        'score':im['scores'][j].cpu().numpy().tolist()})
                if idx % 1000==0:
                    print('{}, {} / {}'.format(datetime.datetime.now(), idx, len(self._testset)))
                #output = self._model['net']( inputs, just_embedding=True) # debug
                #bench.update( targets, output )
        with open('temp_result.json','w') as f:
            json.dump(results,f)
        self.eval_result()
    
    def eval_result(self):
        gt_json='/ssd/data/datasets/COCO/annotations/instances_val2014.json'
        dt_json='temp_result.json'
        annType = 'bbox'
        cocoGt=COCO(gt_json)
        cocoDt=cocoGt.loadRes(dt_json)

        imgIds=sorted(cocoGt.getImgIds())

        # running evaluation
        cocoEval = COCOeval(cocoGt,cocoDt,annType)
        cocoEval.params.catIds = [1]
        cocoEval.params.imgIds = imgIds
        cocoEval.evaluate()
        cocoEval.accumulate()
        cocoEval.summarize()

    def _train( self ):
        self._model.train()

        #widgets = [ progressbar.Percentage(), ' ', progressbar.ETA(), ' ',
        #            '(',progressbar.DynamicMessage('loss'),')' ]
        #bar = progressbar.ProgressBar(widgets=widgets,max_value=len(self._trainset)).start()

        loss_values = []
        average_losses = {}

        for idx, (inputs, targets) in enumerate(self._trainset):
            #print('inputs:', inputs)
            #print('targets:', targets)

            inputs = self._set_device( inputs )
            targets = self._set_device( targets )
            #print('input device:', inputs[0].device)

            outputs = self._model['net'](inputs)
            loss_dict = self._model['loss'](outputs, targets)

            # add the losses for each part
            #loss_sum = sum(loss for loss in loss_dict.values())
            loss_sum=0
            for single_loss in loss_dict:
                loss_sum += loss_dict[single_loss]
                if single_loss in average_losses:
                    average_losses[single_loss]+= loss_dict[single_loss]
                else:
                    average_losses[single_loss] = loss_dict[single_loss]

            if not math.isfinite(loss_sum):
                #print("Loss is {}, stopping training".format(loss_sum))
                print("Loss is {}, skip this batch".format(loss_sum))
                print(loss_dict)
                continue
                #sys.exit(1)

            # Computing gradient and do SGD step
            self._optimizer.zero_grad()
            loss_sum.backward()
            self._optimizer.step()
            loss_values.append( loss_sum.cpu().detach().numpy() )

            if idx%1000 == 0:
                self._logger.info('{} '.format(idx+1))
                loss_str = ''
                for loss in average_losses:
                    if idx==0:
                        loss_num = average_losses[loss]
                    else:
                        loss_num = average_losses[loss] / 100
                    self._logger.info('{} '.format(loss_num))
                    loss_str += (' {} loss:{}, '.format(loss, loss_num))
                print(loss_str[:-2])
                average_losses = {}
                self._logger.info('\n')

            #bar.update(idx+1,loss=loss_sum.item())
        #bar.finish()

        print('Average loss : ', np.mean(loss_values))

    def _set_device( self, blobs ):
        if type(blobs) == list:
            for i in range(len(blobs)):
                blobs[i] = self._set_device(blobs[i])
        elif type(blobs) == dict:
            for key, data in blobs.items():
                blobs[key] = self._set_device(data)
        else:
            blobs = blobs.to(self._device)
        return blobs

    def _set_optimizer( self ):
        params = self._cfg.dnn.OPTIMIZER
        if params['type'] == 'GD':
            self._optimizer = optim.SGD( self._model['net'].parameters(),
                                        lr=params['lr'],
                                        momentum=params.get('momentum',0.9),
                                        weight_decay=params.get('weight_decay',0))
        elif params['type'] == 'Adam':
            self._optimizer = optim.Adam(self._model.parameters(),
                                         lr = params['lr'],
                                         betas=params.get('betas',(0.9, 0.999)),
                                         eps = params.get('eps', 1e-8)
                                         )
        else:
            raise ValueError('Optimiser type wrong, {} is not a valid optimizer type!')

        self._scheduler = optim.lr_scheduler.StepLR( self._optimizer,
                                                    step_size=params['decay_step'],
                                                    gamma=params['decay_rate'] )
        self._niter = self._cfg.dnn.NITER

    def init_logger(self):
        train_path = self._cfg.path['LOG']

        console_formatter = '{} {{}}'.format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        self._logger = Logger(level='info', file=train_path, console=False, console_formatter=console_formatter)

        print('Loss log path is: {}'.format(train_path))

    def forward_once(self):
        for idx, (inputs, targets) in enumerate(self._trainset):
            #print('inputs:', inputs)
            #print('targets:', targets)

            inputs = self._set_device( inputs )
#            targets = self._set_device( targets )
            #print('input device:', inputs[0].device)

            outputs = self._model['net'](inputs)
            loss_dict = self._model['loss'](outputs, targets)
            print('outputs are: ', outputs)
            print('loss dict is: ', loss_dict)


def test(dataloader):
    for inputs, targets in dataloader:
        print(inputs.keys())
        print(targets.keys())
        for im, out in zip(inputs, targets):
            image = im['data']
            boxes = out['boxes']
            im_id = out['image_id']
            image = draw_boxes(image, boxes)
            image.save('./test_im/{}.jpg'.format(im_id))

def draw_boxes(image, boxes):
    draw = Draw(image)
    for box in boxes:
        draw.rectangle(box)
    return image

def test_data_transform():
    anno_path = '/ssd/data/annotations/modanet2018_instances_revised.pkl'
    root = '/ssd/data/datasets/modanet/Images'
    transforms = []
    transforms.append(Resize((600, 800)))
    #transforms.append(Resize_and_Padding(800))
    transforms.append(ResizeMinMax(600, 1000))
    tranform = Compose(transforms)
    dataset = ModanetDataset(root, anno_path, 'val', transforms=tranform)
    for i,(inputs, targets) in enumerate(dataset):
        im = inputs['data']
        im_id = targets['image_id']
        boxes = targets['boxes']
        im = draw_boxes(im, boxes)
        im.save('./test_im/{}.jpg'.format(im_id))
        if i == 10:
            break

def get_dataset():
    anno_path = '/ssd/data/annotations/modanet2018_instances_revised.pkl'
    root = '/ssd/data/datasets/modanet/Images'
    transforms = []
    transforms.append(Resize((600, 800)))
    #transforms.append(Resize_and_Padding(800))
    transforms.append(ResizeMinMax(600, 1000))
    tranform = Compose(transforms)
    dataset = ModanetDataset(root, anno_path, 'val', transforms=tranform)
    return dataset

def get_rcnn_dataset():
    anno_path = '/ssd/data/annotations/modanet2018_instances_revised.pkl'
    root = '/ssd/data/datasets/modanet/Images'
    gt_json = '/ssd/data/datasets/modanet/Annots/modanet_instances_val.json'

    use_cuda = torch.cuda.is_available()
    device_name = "cuda" if use_cuda else "cpu"
    device = torch.device( device_name )

    #transforms = RandomMirror()
    train_dataset = ModanetDataset(anno=anno_path, part='train', root=root, transforms=None) # the default transform in faster rcnn will be used
    #val_dataset = ModanetDataset( anno_path=anno_path, part='val', root=root, transforms=None) # the default transform in faster rcnn will be used

    collate_fn_rcnn = CollateFnRCNN(min_size=800, max_size=800,device=device)

    data_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=2, shuffle=True, num_workers=0,
        collate_fn=collate_fn_rcnn)

    return data_loader

def get_rcnn_cfg():
    params = {}
    params['config'] = 'frcnn_modanet'
    params['batch_size'] = 4
    params['nclasses'] = 13
    params['gpu'] = '0'
    params['tag'] = '20191207_frcnn_modanet_800'
    params['path'] = {}
    
    params['path']['project'] = os.path.expanduser('~/Vision/data')
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = os.path.expanduser('~/Vision/data')

    device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )
    return cfg

def get_network():
    cfg = get_rcnn_cfg()
    backbone = torchvision.models.detection.backbone_utils.resnet_fpn_backbone('resnet101', pretrained=True)
    model = FasterRCNN(backbone, 14, cfg=cfg.dnn)
    model.train()
    return model


if __name__=="__main__" :
    params = {}
    params['config'] = 'yolo'
    params['batch_size'] = 4
    params['nclasses'] = 2
    params['gpu'] = '0'
    params['tag'] = '20191107_yolo'
    params['path'] = {}

    #criterias = ['match']
    #criterias = ['category_accuracy', 'pair_match_accuracy', 'top_k_retrieval_accuracy']
    #criterias = ['pair_match_accuracy', 'top_k_retrieval_accuracy']
    criterias = ['pair_match_accuracy', 'category_accuracy']
    #criterias = ['category_accuracy', 'top_k_retrieval_accuracy']
    #criterias = ['top_k_retrieval_accuracy']
    #criterias = ['category_accuracy']
    #path for save data
    
    if platform.system() == 'Linux':
        params['path']['project'] = '/home/qi/Vision/data'
    else:
        params['path']['project'] = '/Users/qida0163/Vision/data'
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )



    #set the paths to save all the results (model, val result)
    cfg.build_path( params['tag'], 'deepfashion2', model_hash='resnet_50' )

    benchmark = Benchmark(cfg, criterias=criterias)
    #benchmark = None

    anno_path = '/ssd/data/annotations/coco2014_instances_person.pkl'
    root = '/ssd/data/datasets/COCO'
    train_dataset = COCOPersonDataset(root=root, anno=anno_path, part='train2014', transforms=None) # the default transform in faster rcnn will be used
    #val_dataset = COCODataset(anno_path, part='val2014', root=root, transforms=None)

    co_fn = yolo_collate(size=448)
    data_loader_train = DataLoader(train_dataset, batch_size=2, shuffle=True, collate_fn=co_fn, drop_last=True)
    #test(data_loader_train)
    #data_loader = torch.utils.data.DataLoader(
    #    train_dataset, batch_size=2, shuffle=True, num_workers=4,
    #    collate_fn=collate_fn)

    #data_loader_test = torch.utils.data.DataLoader(
    #    val_dataset, batch_size=1, shuffle=False, num_workers=4,
    #    collate_fn=collate_fn)

    use_cuda = torch.cuda.is_available()
    device_name = "cuda" if use_cuda else "cpu"
    device = torch.device( device_name )

    model = {}
    model['net'] = networks.yolo.net(class_num=2)
    model['loss'] = networks.yolo.loss()

    #backbone = torchvision.models.resnet50(pretrained=True)
    #resnet = backbone
    #modules = list(resnet.children())[:-2]      # delete the last fc layer and the adaptive pool layere.
    #backbone = nn.Sequential(*modules)
    #backbone.out_channels=2048
    #model = torchvision.models.detection.FasterRCNN(backbone, num_classes=2, min_size=400, max_size=400)
    #model.to(device)

    t = my_trainer( cfg, model, device, data_loader_train, testset=None, benchmark=None )
    ##loss = t.trainstep()
    ##print('loss is {}'.format(loss))
    ##t.train_one_epoch()
    ##t.validate_onece()
    #t.train()

    ##train_feeder.exit()
    ##test_feeder.exit()

    #torch.save({'state_dict':model.state_dict()}, cfg.path.MODEL)

    #print('Model is saved to :', cfg.path.MODEL)
