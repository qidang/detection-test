import _init
import sys
import os
import argparse
import pickle
import platform
import progressbar
import numpy as np
import datetime
from tools import Logger
import torchvision
import math
from torch import nn
import json

from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval
import datetime
from rcnn_dnn.data.transforms import RandomMirror


#if __name__=="__main__" :
#    parser = argparse.ArgumentParser(description='Detector Training')
#    parser.add_argument('-c','--config',help='Configuration Name',required=True)
#    parser.add_argument('-b','--batchsize',help='Batch size', default=16 )
#    parser.add_argument('-t','--tag',help='Data Tag', default=None)
#    parser.add_argument('--datasetpath', help='Path to the image dataset', required=True )
#    parser.add_argument('--projectpath', help='Path to save the data', required=True )
#    parser.add_argument('-g','--gpu',help='GPU Index',default='0')
#    parser.set_defaults(finetune=False)
#    args = parser.parse_args()
#    #params = _init.parse_commandline()
#
#    os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
#    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu


from PIL.ImageDraw import Draw

from rcnn_config import config
from tools import torch_tools
from data import data_feeder

from rcnn_dnn.data.imageset_modanet import ModanetDataset
from rcnn_dnn.networks import networks
from rcnn_dnn.data.collate_fn import collate_fn

import torch
torch.multiprocessing.set_sharing_strategy('file_system')
import torchvision.transforms as transforms
import torch.optim as optim

from dnn import trainer
from benchmark import Benchmark

def parse_commandline():
    parser = argparse.ArgumentParser(description="Training the Model")
    #parser.add_argument('-c','--config',help='Configuration Name', required=True)
    #parser.add_argument('--patchsize',help='Patch Size', required=True)
    parser.add_argument('-b','--batch_size',help='Batch size', required=True)
    parser.add_argument('-t','--tag',help='Model tag', required=True)
    #parser.add_argument('-g','--gpu',help='GPU Index', default='0')
    #parser.add_argument('--datasetpath',help='Path to the dataset',required=True)
    #parser.add_argument('--projectpath',help='Path to the project',required=True)
    return vars(parser.parse_args())

class my_trainer(trainer):
    def __init__( self, cfg, model, device, trainset, testset=None, gt_json=None, benchmark=None, criterias=None ):
        self._cfg = cfg
        self._device = device
        self._optimizer = None
        self._model = model

        self._trainset = trainset
        self._testset = testset
        self._benchmark = benchmark
        self._criterias = criterias
        self._gt_json = gt_json
        self._epoch = 0
        #self._trainset_feeder = data_feeder( trainset )

        #if testset is not None :
        #    self._testset_feeder = data_feeder( testset )

        self._set_optimizer()

        self.init_logger()

    def train( self ):
        #if self._testset is not None :
        #    self._validate()

        for i in range( self._epoch+1, self._niter+1 ):
            print("Epoch %d/%d" % (i,self._niter))
            self._logger.info('epoch {}\n'.format(i))
            self._train()

            if self._testset is not None :
                self._validate()
            self._epoch = i
            self.save_training(self._cfg.path.CHECKPOINT.format(self._epoch))

    def train_one_epoch(self):
        self._train()

    def validate_onece(self):
        self._validate()

    def _validate( self ):
        print('start to validate')
        self._model.eval()

        results = []
        with torch.no_grad() :
            for idx,(inputs, targets) in enumerate(self._testset):
                inputs = self._set_device( inputs )
                output = self._model( inputs)
                for i, im in enumerate(output):
                    if len(im['boxes']) == 0:
                        continue
                    # convert to xywh
                    im['boxes'][:,2] -= im['boxes'][:,0]
                    im['boxes'][:,3] -= im['boxes'][:,1]
                    for j in range(len(im['boxes'])):
                        results.append({'image_id':targets[i]['image_id'].detach().cpu().numpy().tolist(), 
                                        'category_id':im['labels'][j].cpu().numpy().tolist(), 
                                        'bbox':im['boxes'][j].cpu().numpy().tolist(), 
                                        'score':im['scores'][j].cpu().numpy().tolist()})
                if idx % 1000==0:
                    print('{}, {} / {}'.format(datetime.datetime.now(), idx, len(self._testset)))
                #output = self._model['net']( inputs, just_embedding=True) # debug
                #bench.update( targets, output )
        with open('temp_result.json','w') as f:
            json.dump(results,f)
        self.eval_result(dataset='modanet')
    
    def eval_result(self, dataset='coco'):
        if dataset not in ['coco', 'modanet']:
            raise ValueError('only support coco and modanet dataset')
        if dataset == 'coco':
            gt_json='/ssd/data/datasets/COCO/annotations/instances_val2014.json'
        else:
            gt_json='/ssd/data/datasets/modanet/Annots/modanet_instances_val.json'
        dt_json='temp_result.json'
        annType = 'bbox'
        cocoGt=COCO(gt_json)
        cocoDt=cocoGt.loadRes(dt_json)

        imgIds=sorted(cocoGt.getImgIds())

        # running evaluation
        cocoEval = COCOeval(cocoGt,cocoDt,annType)
        if dataset == 'coco':
            cocoEval.params.catIds = [1]
        cocoEval.params.imgIds = imgIds
        cocoEval.evaluate()
        cocoEval.accumulate()
        cocoEval.summarize()

    #def eval_result(self, gt_json):
    #    dt_json='temp_result.json'
    #    annType = 'bbox'
    #    cocoGt=COCO(gt_json)
    #    cocoDt=cocoGt.loadRes(dt_json)

    #    imgIds=sorted(cocoGt.getImgIds())

    #    # running evaluation
    #    cocoEval = COCOeval(cocoGt,cocoDt,annType)
    #    cocoEval.params.catIds = [1]
    #    cocoEval.params.imgIds = imgIds
    #    cocoEval.evaluate()
    #    cocoEval.accumulate()
    #    cocoEval.summarize()

    def _train( self ):
        self._model.train()

        #widgets = [ progressbar.Percentage(), ' ', progressbar.ETA(), ' ',
        #            '(',progressbar.DynamicMessage('loss'),')' ]
        #bar = progressbar.ProgressBar(widgets=widgets,max_value=len(self._trainset)).start()

        loss_values = []
        average_losses = {}

        for idx, (inputs, targets) in enumerate(self._trainset):
            #print('inputs:', inputs)
            #print('targets:', targets)

            inputs = self._set_device( inputs )
            targets = self._set_device( targets )
            #print('input device:', inputs[0].device)

            loss_dict = self._model(inputs, targets)

            # add the losses for each part
            #loss_sum = sum(loss for loss in loss_dict.values())
            loss_sum=0
            for single_loss in loss_dict:
                loss_sum += loss_dict[single_loss]
                if single_loss in average_losses:
                    average_losses[single_loss]+= loss_dict[single_loss]
                else:
                    average_losses[single_loss] = loss_dict[single_loss]

            if not math.isfinite(loss_sum):
                #print("Loss is {}, stopping training".format(loss_sum))
                print("Loss is {}, skip this batch".format(loss_sum))
                print(loss_dict)
                continue
                #sys.exit(1)

            # Computing gradient and do SGD step
            self._optimizer.zero_grad()
            loss_sum.backward()
            self._optimizer.step()
            loss_values.append( loss_sum.cpu().detach().numpy() )

            if idx%1000 == 0:
                self._logger.info('{} '.format(idx+1))
                loss_str = ''
                for loss in average_losses:
                    if idx==0:
                        loss_num = average_losses[loss]
                    else:
                        loss_num = average_losses[loss] / 100
                    self._logger.info('{} '.format(loss_num))
                    loss_str += (' {} loss:{}, '.format(loss, loss_num))
                print(loss_str[:-2])
                average_losses = {}
                self._logger.info('\n')

            #bar.update(idx+1,loss=loss_sum.item())
        #bar.finish()

        print('Average loss : ', np.mean(loss_values))

    def _set_device( self, blobs ):
        if type(blobs) == list:
            for i in range(len(blobs)):
                blobs[i] = self._set_device(blobs[i])
        elif type(blobs) == dict:
            for key, data in blobs.items():
                blobs[key] = self._set_device(data)
        else:
            blobs = blobs.to(self._device)
        return blobs

    def _set_optimizer( self ):
        params = self._cfg.dnn.OPTIMIZER
        if params['type'] == 'GD':
            self._optimizer = optim.SGD( self._model.parameters(),
                                        lr=params['lr'],
                                        momentum=params.get('momentum',0.9),
                                        weight_decay=params.get('weight_decay',0))
        elif params['type'] == 'Adam':
            self._optimizer = optim.Adam(self._model.parameters(),
                                         lr = params['lr'],
                                         betas=params.get('betas',(0.9, 0.999)),
                                         eps = params.get('eps', 1e-8)
                                         )
        else:
            raise ValueError('Optimiser type wrong, {} is not a valid optimizer type!')

        self._scheduler = optim.lr_scheduler.StepLR( self._optimizer,
                                                    step_size=params['decay_step'],
                                                    gamma=params['decay_rate'] )
        self._niter = self._cfg.dnn.NITER

    def init_logger(self):
        train_path = self._cfg.path['LOG']

        console_formatter = '{} {{}}'.format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        self._logger = Logger(level='info', file=train_path, console=False, console_formatter=console_formatter)

        print('Loss log path is: {}'.format(train_path))

    def save_training(self, path, to_print=True):
        torch.save({
            'epoch': self._epoch,
            'model_state_dict': self._model.state_dict(),
            'optimizer_state_dict': self._optimizer.state_dict()
        }, path)
        if to_print:
            print('The checkpoint has been saved to {}'.format(path))

    def load_training(self, path, to_print=True):
        checkpoint = torch.load(path)
        self._epoch = checkpoint['epoch']
        self._model.load_state_dict(checkpoint['model_state_dict'])
        self._optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        if to_print:
            print('Chekpoint has been loaded from {}'.format(path))

if __name__=="__main__" :
    params = {}
    params['config'] = 'frcnn_modanet'
    params['batch_size'] = 4
    params['nclasses'] = 13
    params['gpu'] = '0'
    params['tag'] = '20191207_frcnn_modanet_800'
    params['path'] = {}

    #criterias = ['match']
    #criterias = ['category_accuracy', 'pair_match_accuracy', 'top_k_retrieval_accuracy']
    #criterias = ['pair_match_accuracy', 'top_k_retrieval_accuracy']
    criterias = ['pair_match_accuracy', 'category_accuracy']
    #criterias = ['category_accuracy', 'top_k_retrieval_accuracy']
    #criterias = ['top_k_retrieval_accuracy']
    #criterias = ['category_accuracy']
    #path for save data
    
    params['path']['project'] = os.path.expanduser('~/Vision/data')
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )



    #set the paths to save all the results (model, val result)
    cfg.build_path( params['tag'], 'modanet', model_hash='darknet' )

    #benchmark = Benchmark(cfg, criterias=criterias)
    #benchmark = None

    anno_path = '/ssd/data/annotations/modanet2018_instances_revised.pkl'
    root = '/ssd/data/datasets/modanet/Images'
    gt_json = '/ssd/data/datasets/modanet/Annots/modanet_instances_val.json'
    transforms = RandomMirror()
    train_dataset = ModanetDataset(anno_path=anno_path, part='train', root=root, transforms=transforms) # the default transform in faster rcnn will be used
    val_dataset = ModanetDataset( anno_path=anno_path, part='val', root=root, transforms=None) # the default transform in faster rcnn will be used

    data_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=2, shuffle=True, num_workers=4,
        collate_fn=collate_fn)

    data_loader_test = torch.utils.data.DataLoader(
        val_dataset, batch_size=1, shuffle=False, num_workers=4,
        collate_fn=collate_fn)

    use_cuda = torch.cuda.is_available()
    device_name = "cuda" if use_cuda else "cpu"
    device = torch.device( device_name )

    backbone = torchvision.models.detection.backbone_utils.resnet_fpn_backbone('resnet101', pretrained=True)
    model = torchvision.models.detection.FasterRCNN(backbone, num_classes=14, min_size=800, max_size=800)
    model.to(device)

    t = my_trainer( cfg, model, device, data_loader, testset=data_loader_test, gt_json=gt_json, benchmark=None )
    #loss = t.trainstep()
    #print('loss is {}'.format(loss))
    #t.train_one_epoch()
    #t.validate_onece()
    t.load_training(cfg.path.CHECKPOINT.format(10))
    t.train()

    #train_feeder.exit()
    #test_feeder.exit()

    #torch.save({'state_dict':model.state_dict()}, cfg.path.MODEL)

    print('Model is saved to :', cfg.path.MODEL)
