import numpy as np
import os
import json

def extend_batch_boxes(batch_boxes, images, extend):
    '''
        extend boxes in a batch
        batch_boxes: list(np.array(n*4))
    '''
    extended_boxes = []
    for image, boxes in zip(images, batch_boxes):
        width, height = image.size
        if len(boxes) == 0:
            extended_boxes.append(boxes)
        else:
            extended_boxes.append(extend_boxes(boxes, extend, width, height))
    return extended_boxes

def extend_boxes(boxes, extend, width, height):
    assert extend>0

    boxes_extended = np.zeros_like(boxes)
    box_w = boxes[:,2] - boxes[:,0]
    box_h = boxes[:,3] - boxes[:,1]

    boxes_extended[:,0] = np.clip(boxes[:,0]-box_w*extend, a_min=0, a_max=None)
    boxes_extended[:,2] = np.clip(boxes[:,2]+box_w*extend, a_min=None, a_max=width)
    boxes_extended[:,1] = np.clip(boxes[:,1]-box_h*extend, a_min=0, a_max=None)
    boxes_extended[:,3] = np.clip(boxes[:,3]+box_h*extend, a_min=None, a_max=height)

    return boxes_extended

def extend_box(box, extend, width, height):
    '''
        extend the box of extend percent to both side
    '''
    assert extend>0
    box_w = box[2] - box[0]
    box_h = box[3] - box[1]
    x1 = int(max(0, box[0]-box_w*extend))
    y1 = int(max(0, box[1]-box_h*extend))
    x2 = int(min(width-1, box[2]+box_w*extend))
    y2 = int(min(height-1, box[3]+box_h*extend))
    return (x1, y1, x2, y2)


def crop_images(images, batch_boxes, extend=0):
    out_ims = []
    batch_id = []
    for i, (image, boxes) in enumerate(zip(images, batch_boxes)):
        width, height = image.size
        for box in boxes:
            if extend != 0:
                box = extend_box(box, extend, width, height)
            out_ims.append(image.crop(box))
            batch_id.append(i)
    return out_ims, batch_id 

def path_to_file(paths):
    file_names = []
    for path in paths:
        file_names.append(os.path.basename(path))
    return file_names

class COCOAnnotation(object):
    def __init__(self):
        anno = {}
        anno['images'] = []
        anno['annotations'] = []
        anno['type'] = 'instances'

        self.anno = anno
        self.id_ind = 0

    def add_batch_anno(self, batch_boxes, batch_class_ids, image_ids):
        for boxes, class_ids, image_id in zip(batch_boxes, batch_class_ids, image_ids):
            for box, class_id in zip(boxes, class_ids):
                anno = {}
                anno['bbox'] = box.astype(int).tolist()
                anno['area'] = ((box[2]-box[0])*(box[3]-box[1])).astype(int)
                anno['category_id'] = int(class_id)
                anno['image_id'] = image_id
                anno['id'] = self.id_ind
                self.id_ind += 1
                self.anno['annotations'].append(anno)

    def add_batch_image(self, images, image_ids, paths):
        ['file_name', 'width', 'height', 'id']
        for image, image_id, path in zip(images, image_ids, paths):
            im_info = {}
            im_info['file_name'] = os.path.basename(path)
            width, height = image.size
            im_info['width'] = width
            im_info['height'] = height
            im_info['id'] = image_id
            self.anno['images'].append(im_info)

    def save_anno(self, path):
        with open(path, 'w') as f:
            json.dump(self.anno, f)
            print('The annotation is successfully saved in {}'.format(path))
            return
        print('Fail to save the annotation to {} !'.format(path))

                