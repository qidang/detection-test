from easydict import EasyDict as edict
import sys
import os
import platform
import numpy as np
from pprint import pprint
from inspect import isfunction

def mkdir( d ):
    if not os.path.isdir( d ) :
        os.mkdir( d )

class config :
    def __init__( self, cfg_name, mode, datasetpath, projectpath ):
        self._cfg_name = cfg_name
        self._cfg = None
        self._mcfg = None

        self.initialize( mode )
        #self.initialize_dataset( datasetpath )
        self.initialize_data( projectpath )

        #self._init_detector()

    def initialize( self, mode ):
        from .configs import configurations
        assert self._cfg_name in configurations, "Unknown config"

        cfg = configurations[ self._cfg_name ].get_cfg()
        assert cfg.NAME == self._cfg_name, "Config name does not match"

        self._cfg = cfg
        if hasattr(cfg, 'rpn'):
            self.rpn = cfg.rpn
        #if hasattr(cfg, 'second_rpn'):
        #    self.second_rpn = cfg.second_rpn
        if hasattr(cfg, 'roi_pool'):
            self.roi_pool = cfg.roi_pool
        if hasattr(cfg, 'roi_head'):
            self.roi_head = cfg.roi_head
        #if hasattr(cfg, 'second_roi_head'):
        #    self.second_roi_head = cfg.second_roi_head
        #if hasattr(cfg, 'second_head'):
        #    self.second_head = cfg.second_head
        if hasattr(cfg, 'optimizer'):
            self.optimizer = cfg.optimizer
        if hasattr(cfg, 'scheduler'):
            self.scheduler = cfg.scheduler
        #if hasattr(cfg, 'centernet_head'):
        #    self.centernet_head = cfg.centernet_head
        #if hasattr(cfg, 'scheduler'):
        #    self.scheduler = cfg.scheduler
        #if hasattr(cfg, 'first_head'):
        #    self.first_head = cfg.first_head
        #if hasattr(cfg,'inputs_converter'):
        #    self.inputs_converter = cfg.inputs_converter
        #if hasattr(cfg, 'targets_converter'):
        #    self.targets_converter = cfg.targets_converter
        #if hasattr(cfg, 'roi_pooler'):
        #    self.roi_pooler = cfg.roi_pooler

        self._cfg.NAME = cfg.NAME
        self._cfg.DATA = cfg.DATA
        self._cfg.DATASET = cfg.DATASET

        self._cfg.DNN = cfg.DNN[ mode ]
        self._cfg.DNN.NETWORK = cfg.NETWORK

    def initialize_dataset( self, datasetpath ):
        self._cfg.DATASET.ROOT = datasetpath
        self._cfg.DATASET.ANNOTS_TMP = "%s/annotations/%s.pkl" % ( self._cfg.DATASET.ROOT, '%s' )
        self._cfg.DATASET.IMAGES_TMP = "%s/datasets/%s" % ( self._cfg.DATASET.ROOT,'%s' )
        self._cfg.DATASET.WEIGHTS_TMP = "%s/weights/%s" % ( self._cfg.DATASET.ROOT, '%s' )
        self._cfg.DATASET.PRETRAINED = "%s/pretrained/%s.ckpt" % ( self._cfg.DATASET.ROOT, '%s' )
        self._cfg.DATASET.WORKSET_SETTINGS = []

        if self._cfg.DNN.NETWORK.PRETRAINED is not None :
            self._cfg.DATASET.PRETRAINED = self._cfg.DATASET.PRETRAINED % ( self._cfg.DNN.NETWORK.PRETRAINED )

    def initialize_data( self, projectpath ):
        tmp = edict()

        tmp.PROJECT = "embedding"
        tmp.TAG = self._cfg.NAME

        tmp.DIRS = edict()
        tmp.DIRS.ROOT = projectpath
        tmp.DIRS.BASE = os.path.join( tmp.DIRS.ROOT, tmp.PROJECT )

        mkdir( tmp.DIRS.ROOT )
        mkdir( tmp.DIRS.BASE )

        tmp.DIRS.BASE = os.path.join( tmp.DIRS.BASE, tmp.TAG )
        mkdir( tmp.DIRS.BASE )
        tmp.DIRS.LOGS = os.path.join( tmp.DIRS.BASE, "logs" )
        mkdir( tmp.DIRS.LOGS )
        #tmp.DIRS.DETS = os.path.join( tmp.DIRS.BASE, "dets" )
        #mkdir( tmp.DIRS.DETS )
        tmp.DIRS.CHECKPOINTS = os.path.join( tmp.DIRS.BASE, "checkpoints" )
        mkdir( tmp.DIRS.CHECKPOINTS )
        #tmp.DIRS.BENCHMARK = os.path.join( tmp.DIRS.BASE, "benchmark" )
        #mkdir( tmp.DIRS.BENCHMARK )
        #tmp.DIRS.HDF5 = os.path.join( tmp.DIRS.BASE, "hdf5" )
        #mkdir( tmp.DIRS.HDF5 )

        self._cfg.DATA = tmp


    def build_path( self, tag, data_hash, model_hash=None ):
        assert( self._cfg.DATA is not None )
        data = self._cfg.DATA
        dnn = self._cfg.DNN

        if model_hash is None :
            model_hash = data_hash

        path = edict()
        #path.MODEL = '%s/model_%s_%s_%s.pkl' % ( data.DIRS.MODELS, dnn.NETWORK.FEAT_NAME, model_hash, tag )
        #path.HDF5 = '%s/%s.hdf5' % ( data.DIRS.HDF5, '%s' )
        path.LOG = '%s/log_%s_%s_%s.csv' % ( data.DIRS.LOGS, dnn.NETWORK.FEAT_NAME, model_hash, tag )
        #path.RES  = '%s/results_model_%s_dset_%s_%s.pkl' % ( data.DIRS.DETS, model_hash, data_hash, tag )
        #path.BENCHMARK = '%s/bench_model_%s_dset_%s_%s_%s.pkl' % ( data.DIRS.BENCHMARK, model_hash, data_hash, tag, '%s' )
        #path.BENCHMARK_LOG = '%s/bench_log_%s_%s_%s.csv' % ( data.DIRS.BENCHMARK, dnn.NETWORK.FEAT_NAME, model_hash, tag )
        path.CHECKPOINT = '{}/checkpoints_{}_{{}}.pkl'.format(data.DIRS.CHECKPOINTS, tag)

        self._cfg.PATH = path

    def update( self, params ):
        nclasses = params.get('nclasses',None)
        if nclasses is not None :
            self._cfg.DNN.NETWORK.NCLASSES = int(nclasses)

        batch_size = params.get('batch_size',None)
        if batch_size is not None :
            self._cfg.DNN.NETWORK.BATCH_SIZE = int(batch_size)

        niter = params.get('niter',None)
        if niter is not None :
            self._cfg.DNN.TRAIN.NITER = int(niter)

        prefix = params.get('prefix',None)
        if prefix is not None :
            self._cfg.DNN.NETWORK.PREFIX = prefix

        usenegatives = params.get('usenegatives',None)
        if usenegatives is not None :
            self._cfg.DNN.USE_NEGATIVES = usenegatives
    
    def __str__(self):
        for attri in dir(self):
            if isfunction(self.__getattribute__(attri)) or callable(self.__getattribute__(attri)) or attri.startswith('__'):
                continue
            else:
                print("{}: {}".format(attri, self.__getattribute__(attri)))
        return ''


    @property
    def dnn( self ):
        return self._cfg.DNN

    @property
    def data( self ):
        return self._cfg.DATA

    @property
    def dataset( self ):
        return self._cfg.DATASET

    @property
    def path( self ):
        return self._cfg.PATH

