from . import base as baseline
from . import frcnn_coco as frcnn_coco
from . import frcnn_modanet as frcnn_modanet
from . import yolo
from . import center_net_coco
from . import frcnn_coco_person
from . import mix_rcnn
from . import roi_centernet
from . import double_rcnn
from . import frcnn_deepfashion2
#from . import double_rcnn_new


configurations = {}

configurations['baseline'] = baseline
configurations['frcnn_coco'] = frcnn_coco
configurations['frcnn_coco_person'] = frcnn_coco_person
configurations['frcnn_modanet'] = frcnn_modanet
configurations['frcnn_deepfashion2'] = frcnn_deepfashion2
configurations['yolo'] = yolo
configurations['center_net_coco'] = center_net_coco
configurations['mix_rcnn'] = mix_rcnn
configurations['double_rcnn'] = double_rcnn
#configurations['double_rcnn_new'] = double_rcnn_new
configurations['roi_centernet'] = roi_centernet
