from easydict import EasyDict as edict
import numpy as np
from . import base as baseline

def get_cfg():
    cfg = baseline.get_cfg()

    # Modifications to baseline config
    cfg.NAME = 'center_net_coco'

    optimizer = edict()
    #optimizer.type = 'Adam'
    #optimizer.lr = 0.0001
    optimizer.type = 'SGD'
    optimizer.lr = 1e-3
    #optimizer.lr = 0.00025
    #optimizer.n_iter = 18
    optimizer.n_iter = 150
    optimizer.weight_decay=1e-4
    cfg.optimizer = optimizer

    scheduler = edict()
    scheduler.type = 'multi_step'
    #scheduler.milestones = [11, 15]
    scheduler.milestones = [90, 130]
    scheduler.gamma = 0.1
    cfg.scheduler = scheduler

    return cfg
