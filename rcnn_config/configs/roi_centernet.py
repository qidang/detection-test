from easydict import EasyDict as edict
import numpy as np
from . import base as baseline

def get_cfg():
    cfg = baseline.get_cfg()

    # Modifications to baseline config
    cfg.NAME = 'roi_centernet'
    cfg.DATASET.WORKSET_SETTING = []


    roi_pool = edict()
    roi_pool.pool_w = 7
    roi_pool.pool_h = 7
    cfg.roi_pool = roi_pool

    centernet_head = edict()
    centernet_head.train_box_iou_thre = 0.7
    centernet_head.min_area = 256 # minimum human box area
    centernet_head.dataset_label = None
    centernet_head.roi_pool_h = 112
    centernet_head.roi_pool_w = 56
    #centernet_head.roi_pool_h = 104
    #centernet_head.roi_pool_w = 69
    centernet_head.max_obj = 128
    centernet_head.nms_thresh = 0.5
    centernet_head.loss_parts = ['heatmap', 'offset', 'width_height']
    #centernet_head.loss_weight = None # {'heatmap':1., 'offset':0.5, 'width_height':0.01}
    #centernet_head.loss_weight = {'heatmap':1., 'offset':0.5, 'width_height':0.001}
    centernet_head.loss_weight = {'heatmap':1., 'offset':1., 'width_height':0.1}
    #centernet_head.loss_weight = {'heatmap':0.01, 'offset':0.005, 'width_height':0.0001}
    #centernet_head.loss_weight = {'heatmap':0.1, 'offset':0.05, 'width_height':0.001}
    cfg.centernet_head = centernet_head

    optimizer = edict()
    #optimizer.type = 'Adam'
    #optimizer.lr = 0.0001
    optimizer.type = 'SGD'
    optimizer.lr = 1e-4
    #optimizer.lr = 0.00025
    #optimizer.n_iter = 18
    optimizer.n_iter = 150
    optimizer.weight_decay=1e-4
    cfg.optimizer = optimizer

    scheduler = edict()
    scheduler.type = 'multi_step'
    #scheduler.milestones = [11, 15]
    scheduler.milestones = [90, 130]
    scheduler.gamma = 0.1
    cfg.scheduler = scheduler

    #cfg.DNN['train'].PATCH_SIZE = 64
    #cfg.DNN['test'].PATCH_SIZE = 64

    #cfg.DNN['train'].NITER = 15
    ##old one
    ##cfg.DNN['train'].OPTIMIZER = {'type':'GD','lr':0.01,'decay_step':5,'decay_rate':0.1}
    ##cfg.DNN['train'].OPTIMIZER = {'type':'GD','lr':0.001,'decay_step':12,'decay_rate':0.1}
    #cfg.DNN['train'].OPTIMIZER = {'type':'Adam','lr':0.0001, 'milestones':[10,13], 'gamma':0.1}

    #cfg.NETWORK.FEATURE_DIM = 1024

    ##cfg.DNN.PROPOSAL.TRAIN.SELECTION_BATCH_SIZE = {'s1':128,'s2':128}
    ##cfg.DNN.FRCNN.TRAIN.SELECTION_BATCH_SIZE = {'s1':256,'s2':256,'s2_hinge':2000}

    #cfg.NETWORK.INTERFACE = 'patch'
    #cfg.NETWORK.FEAT_NAME = 'vgg16'

    return cfg
