import _init
import os
import argparse
import pickle
import platform
import progressbar
import numpy as np
from torch import nn
import datetime
import json
import tqdm

from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval

from PIL.ImageDraw import Draw

from data.datasets import load_dataset
from rcnn_config import config
from tools import torch_tools
from data import data_feeder


#from data.datasets import ModanetDataset
#from rcnn_dnn.data.imageset_modanet import ModanetDataset
from torchcore.data.datasets import ModanetDataset
from rcnn_dnn.data.imageset_coco import COCODataset
from rcnn_dnn.data.imageset_test import TestDataset
from torchcore.data.datasets import ListDataset, COCOPersonDataset
from rcnn_dnn.networks import networks
from rcnn_dnn.data.collate_fn import collate_fn, collate_fn_test, collate_fn_modahuman, CollateFnRCNN
from torchcore.data.transforms import GeneralRCNNTransform
from torchcore.dnn.networks.faster_rcnn import FasterRCNN
from torchcore.tools.visulize_tools import draw_and_save

import torch
import torchvision
import time
torch.multiprocessing.set_sharing_strategy('file_system')
import torchvision.transforms as transforms
import torch.optim as optim

from sklearn.metrics import precision_recall_curve
from sklearn.metrics import average_precision_score
from inspect import signature
import matplotlib.pyplot as plt

from dnn import trainer
import random
from PIL import Image

CLASS_NAMES = ['bag', 'belt', 'boots', 'footwear', 'outer', 'dress',
               'sunglasses', 'pants', 'top', 'shorts', 'skirt',
               'headware', 'scarf&tie'
               ]

CLASS_NAME_HUMAN = ['human']

FONT_PATH = '/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf'
def parse_commandline():
    parser = argparse.ArgumentParser(description="Test the Model")
    parser.add_argument('--dataset', type=str, help='Dataset to test, can be coco or modanet')
    parser.add_argument('--image_size', type=int, help='The size of image to test')
    parser.add_argument('--model_size', type=int, help='The size of image the model trained on')
    parser.add_argument('-g','--gpu',help='GPU Index', default='0')
    #parser.add_argument('-c','--config',help='Configuration Name', required=True)
    #parser.add_argument('--patchsize',help='Patch Size', required=True)
    #parser.add_argument('-b','--batch_size',help='Batch size', required=True)
    #parser.add_argument('-t','--tag',help='Model tag', required=True)
    #parser.add_argument('--datasetpath',help='Path to the dataset',required=True)
    #parser.add_argument('--projectpath',help='Path to the project',required=True)
    return parser.parse_args()


def get_max_score_box(boxes):
    if len(boxes)== 0:
        return []
    ind = np.argmax(boxes[:,4])
    box = boxes[ind][:4]
    return box
    
def crop_images(images, batch_boxes):
    out_ims = []
    batch_id = []
    for i, (image, boxes) in enumerate(zip(images, batch_boxes)):
        for box in boxes:
            out_ims.append(image.crop(box))
            batch_id.append(i)
    return out_ims, batch_id

class my_trainer(trainer):

    def _validate( self ):
        print('start to validate')
        self._model.eval()

        results = []
        total_time = 0
        total_sample = 0
        with torch.no_grad() :
            for idx,(inputs, targets) in enumerate(tqdm.tqdm(self._testset, desc='Testing')):
                start = time.time()
                inputs = self._set_device( inputs )
                output = self._model( inputs)
                total_time += time.time() - start
                total_sample += len(output)
                for i, im in enumerate(output):
                    if len(im['boxes']) == 0:
                        continue
                    # convert to xywh
                    im['boxes'][:,2] -= im['boxes'][:,0]
                    im['boxes'][:,3] -= im['boxes'][:,1]
                    for j in range(len(im['boxes'])):
                        results.append({'image_id':targets[i]['image_id'], 
                                        'category_id':im['labels'][j].cpu().numpy().tolist(), 
                                        'bbox':im['boxes'][j].cpu().numpy().tolist(), 
                                        'score':im['scores'][j].cpu().numpy().tolist()})

        print('total detection time is {}s'.format(total_time))
        print('Detection time per image is {}s'.format(total_time/total_sample))
        with open('temp_result.json','w') as f:
            json.dump(results,f)
        self.eval_result()
    
    def test_speed( self ):
        print('start to validate')
        self._model.eval()

        start = time.time()
        im_num = 0
        with torch.no_grad() :
            for idx,(inputs, targets) in enumerate(self._testset):
                im_num+= len(inputs)
                inputs = self._set_device( inputs )
                output = self._model( inputs)

            time_per_im = (time.time() - start) / im_num
            print('Detection time per image is {}'.format(time_per_im))
            print('Detecting speed is {} FPS'.format(1.0 / time_per_im))

    def eval_result(self):
        gt_json='/ssd/data/datasets/COCO/annotations/instances_val2014.json'
        dt_json='temp_result.json'
        annType = 'bbox'
        cocoGt=COCO(gt_json)
        cocoDt=cocoGt.loadRes(dt_json)

        imgIds=sorted(cocoGt.getImgIds())

        # running evaluation
        cocoEval = COCOeval(cocoGt,cocoDt,annType)
        cocoEval.params.catIds = [1]
        cocoEval.params.imgIds = imgIds
        cocoEval.evaluate()
        cocoEval.accumulate()
        cocoEval.summarize()

    def test(self):
        self._validate()

    def test_two_stage(self, path, human_size, garment_size):
        self._model_human.eval()
        self._model_garment.eval()
        for idx, (inputs, targets) in enumerate(tqdm.tqdm(self._testset, desc='Detecting Objects')):
            #images = self.extract_imgs(inputs)
            image_path = inputs['path']
            images = [Image.open(im_path).convert('RGB') for im_path in image_path]
            self._set_device(inputs)
            outputs = self._model_human(inputs)
            human_boxes = []
            for output in outputs:
                #print('boxes size: ', output['boxes'].size())
                #print('scores size: ', output['scores'].size())
                #print('labels size: ', output['labels'].size())
                boxes = output['boxes'].detach().cpu().numpy()
                scores = output['scores'].detach().cpu().numpy()
                labels = output['labels'].detach().cpu().numpy() - 1
                human_boxes.append(boxes)
            
            cropped_im, batch_id = crop_images(images, human_boxes)
            inputs_garment = [{'data': im} for im in cropped_im]
            targets = None
            transforms = GeneralRCNNTransform(min_size=garment_size, max_size=garment_size)
            inputs_garment, _ = transforms(inputs_garment)
            self._set_device(inputs_garment)
            outputs_garments = self._model_garment(inputs_garment)

            batch_boxes = [[] for i in range(len(images))]
            batch_scores = [[] for i in range(len(images))]
            batch_labels = [[] for i in range(len(images))]
            for j, output in enumerate(outputs):
                #print('boxes size: ', output['boxes'].size())
                #print('scores size: ', output['scores'].size())
                #print('labels size: ', output['labels'].size())
                boxes = output['boxes'].detach().cpu().numpy()
                scores = output['scores'].detach().cpu().numpy()
                labels = output['labels'].detach().cpu().numpy() - 1
                #boxes = torch.cat((output['boxes'], output['scores'].unsqueeze(1),output['labels'].type(torch.cuda.FloatTensor).unsqueeze(1)), dim=-1).detach().cpu().numpy()
                ind = batch_id[j]
                batch_boxes[ind].append(boxes)
                batch_scores[ind].append(scores)
                batch_labels[ind].append(labels)
            
            batch_size = len(images)
            ind_start = batch_size*idx + 1
            
            #self.draw_and_save(images, batch_boxes, path, targets)
            draw_boxes(images, human_boxes, CLASS_NAME_HUMAN)
            draw_and_save(images, batch_boxes, batch_scores, batch_labels, path, ind_start, CLASS_NAMES)
            
            



        

    def draw_result(self, path=None):
        self._model.eval()
        for idx, (inputs, targets) in enumerate(tqdm.tqdm(self._testset, desc='Detecting Objects')):
            #images = self.extract_imgs(inputs)
            image_path = inputs['path']
            images = [Image.open(im_path).convert('RGB') for im_path in image_path]
            self._set_device(inputs)
            outputs = self._model(inputs)

            '''
            [{'labels': tensor([1, 1, 1, 1, 1, 1]), 'scores': tensor([0.9955, 0.9817, 0.8980, 0.2855, 0.1070, 0.0641],
                grad_fn=<IndexBackward>), 'boxes': tensor([[281.0141,   0.0000, 442.9664, 224.5284],
                    [135.6355,  42.3642, 573.4156, 465.7547],
                    [595.5556,   7.3492, 636.2258,  88.0141],
                    [284.8833,  27.2350, 515.8875, 298.1581],
                    [591.5268,  39.6823, 623.6944, 105.6075],
                    [191.0274,  34.2148, 447.3389, 257.3041]], grad_fn=<StackBackward>)}]
            '''
            batch_boxes = []
            batch_scores = []
            batch_labels = []
            for output in outputs:
                #print('boxes size: ', output['boxes'].size())
                #print('scores size: ', output['scores'].size())
                #print('labels size: ', output['labels'].size())
                boxes = output['boxes'].detach().cpu().numpy()
                scores = output['scores'].detach().cpu().numpy()
                labels = output['labels'].detach().cpu().numpy() - 1
                #boxes = torch.cat((output['boxes'], output['scores'].unsqueeze(1),output['labels'].type(torch.cuda.FloatTensor).unsqueeze(1)), dim=-1).detach().cpu().numpy()
                batch_boxes.append(boxes)
                batch_scores.append(scores)
                batch_labels.append(labels)
            
            batch_size = len(images)
            ind_start = batch_size*idx + 1
            
            #self.draw_and_save(images, batch_boxes, path, targets)
            draw_and_save(images, batch_boxes, batch_scores, batch_labels, path, ind_start, CLASS_NAMES)
            #print(output)

    def save_human_result(self, path):
        print('running save human result')
        self._model.eval()
        results=[]
        for inputs, targets in tqdm.tqdm(self._testset,desc='gen human result'):
            #images = self.extract_imgs(inputs)
            self._set_device(inputs)
            outputs = self._model(inputs)

            '''
            [{'labels': tensor([1, 1, 1, 1, 1, 1]), 'scores': tensor([0.9955, 0.9817, 0.8980, 0.2855, 0.1070, 0.0641],
                grad_fn=<IndexBackward>), 'boxes': tensor([[281.0141,   0.0000, 442.9664, 224.5284],
                    [135.6355,  42.3642, 573.4156, 465.7547],
                    [595.5556,   7.3492, 636.2258,  88.0141],
                    [284.8833,  27.2350, 515.8875, 298.1581],
                    [591.5268,  39.6823, 623.6944, 105.6075],
                    [191.0274,  34.2148, 447.3389, 257.3041]], grad_fn=<StackBackward>)}]
            '''
            batch_boxes = []
            for output, target in zip(outputs, targets):
                im_id = target['image_id']
                #print('boxes size: ', output['boxes'].size())
                #print('scores size: ', output['scores'].size())
                #print('labels size: ', output['labels'].size())
                boxes = torch.cat((output['boxes'], output['scores'].unsqueeze(1),output['labels'].type(torch.cuda.FloatTensor).unsqueeze(1)), dim=-1).detach().cpu().numpy()
                #batch_boxes.append(torch.cat((output['boxes'], output['scores'].unsqueeze(1),output['labels'].type(torch.cuda.FloatTensor).unsqueeze(1)), dim=-1))
                #box = get_max_score_box(boxes)
                #results.append({'image_id':im_id, 'human_box':box})
                results.append({'image_id':im_id, 'human_box':boxes})
        with open(path, 'wb') as f:
            pickle.dump(results, f)

    def extract_imgs(self, im_tensor):
        images = []
        for im in im_tensor:
            image = transforms.ToPILImage()(im).convert("RGB")
            images.append(image)
        return images

    def _train( self ):
        self._model['net'].train()

        widgets = [ progressbar.Percentage(), ' ', progressbar.ETA(), ' ',
                    '(',progressbar.DynamicMessage('loss'),')' ]
        bar = progressbar.ProgressBar(widgets=widgets,max_value=len(self._trainset)).start()

        loss_values = []

        for idx in range( len(self._trainset) ):
            inputs, targets = self._trainset.next()

            inputs = self._set_device( inputs )
            targets = self._set_device( targets )

            outputs = self._model['net']( inputs)
            loss = self._model['loss']( outputs, targets )

            loss_values.append( loss.cpu().detach().numpy() )

            # Computing gradient and do SGD step
            self._optimizer.zero_grad()
            loss.backward()
            self._optimizer.step()

            bar.update(idx+1,loss=loss.item())
        bar.finish()

        print('Average loss : ', np.mean(loss_values))

    def _set_device( self, blobs ):
        if type(blobs) == list:
            for i in range(len(blobs)):
                blobs[i] = self._set_device(blobs[i])
        elif type(blobs) == dict:
            for key, data in blobs.items():
                if key in ['scale', 'image_sizes', 'path', 'ori_image']:
                    continue
                blobs[key] = self._set_device(data)
        else:
            blobs = blobs.to(self._device)
        return blobs

    def _set_optimizer( self ):
        params = self._cfg.dnn.OPTIMIZER
        if params['type'] == 'GD':
            self._optimizer = optim.SGD( self._model.parameters(),
                                        lr=params['lr'],
                                        momentum=params.get('momentum',0.9),
                                        weight_decay=params.get('weight_decay',0))
        elif params['type'] == 'Adam':
            self._optimizer = optim.Adam(self._model.parameters(),
                                         lr = params['lr'],
                                         betas=params.get('betas',(0.9, 0.999)),
                                         eps = params.get('eps', 1e-8)
                                         )
        else:
            raise ValueError('Optimiser type wrong, {} is not a valid optimizer type!')

        self._scheduler = optim.lr_scheduler.StepLR( self._optimizer,
                                                    step_size=params['decay_step'],
                                                    gamma=params['decay_rate'] )
        self._niter = self._cfg.dnn.NITER

def get_input(train_feeder):
    data = train_feeder.next()
    for key in data.keys():
        for a_key in data[key].keys():
            print('Key: {}, shape: {}'.format(a_key, data[key][a_key].shape))
    #return data

if __name__=="__main__" :
    '''
        Warninig: remeber to edic the roi head threshold from 0.01 to 0.8 
        if need the human detection for modanet, otherwise it should be 0.01
        rcnn_configs/configs/base.py line 80
    '''
    args = parse_commandline()
    print(args)
    image_size = args.image_size
    
    #image_size = 800
    params = {}
    params['config'] = 'frcnn_modanet'
    params['nclasses'] = 13
    params['tag'] = '20191218_frcnn_modanet_800'
    params['path'] = {}
    

    criterias = ['pair_match_accuracy', 'category_accuracy']
    
    params['path']['project'] = os.path.expanduser('~/Vision/data')
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    pic_root = '/hdd/fashion_videos/out_frames'
    pic_root = '/ssd/data/datasets/modanet/Images/'
    list_path = '/ssd/data/datasets/fashion_videos/testim.txt'

    device = torch_tools.get_device( args.gpu )

    cfg = config( params['config'], 'test', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )

    #set the paths to save all the results (model, val result)
    cfg.build_path( params['tag'], 'deepfashion2', model_hash='resnet_50' )

    #set up parameters for model and resolution for test
    if args.dataset == 'coco_person':
        anno_path = '/ssd/data/annotations/coco2014_instances_person.pkl'
        root = '/ssd/data/datasets/COCO'
        test_dataset = COCOPersonDataset(root, anno_path,'val2014',transforms=None)
        num_classes = 2
        if args.model_size == 416:
            model_path = os.path.expanduser('~/Vision/data/embedding/frcnn_modanet/checkpoints/checkpoints_20200113_frcnn_human_416_12.pkl')
        elif args.model_size == 800:
            model_path = os.path.expanduser('~/Vision/data/embedding/frcnn_modanet/checkpoints/checkpoints_20200114_frcnn_human_800_10.pkl')
        else:
            raise ValueError('Invalid model size')
    elif args.dataset == 'modanet':
        anno_path = '/ssd/data/annotations/modanet2018_instances_revised.pkl'
        root = '/ssd/data/datasets/modanet/Images'
        test_dataset = ModanetDataset(root, anno_path, part='val', transforms=None)
        num_classes = 14
        if args.model_size == 416:
            model_path_garment = os.path.expanduser('~/Vision/data/embedding/frcnn_modanet/checkpoints/checkpoints_20200110_frcnn_modanet_416_10.pkl')
        elif args.model_size == 800:
            model_path_garment = os.path.expanduser('~/Vision/data/embedding/frcnn_modanet/checkpoints/checkpoints_20191218_frcnn_modanet_800_10.pkl')
        else:
            raise ValueError('Invalid model size')
    else:
        raise ValueError('Invalid dataset')


        #test_dataset = ModanetDataset( anno_path=anno_path, part='val', root=pic_root, transforms=None) # the default transform in faster rcnn will be used
    #anno_path = '/ssd/data/annotations/modanet2018_instances_val.pkl'
    #train_dataset = COCODataset(anno_path, part='train2014', root=root, transforms=None) # the default transform in faster rcnn will be used
    #val_dataset = COCODataset(anno_path, part='val2014', root=root, transforms=None)
    #test_dataset = ModanetDataset(anno=anno_path, part='train', root=root, transforms=None) # the default transform in faster rcnn will be used
    #transforms = GeneralRCNNTransform(min_size=800, max_size=800, device=device)
    #test_dataset = ListDataset(list_path) # the rcnn transform are integrad into collate fn
    collate_fn_rcnn = CollateFnRCNN(min_size=image_size, max_size=image_size)
    #test_dataset = TestDataset(pic_root=pic_root )
    #print('test dataset length is {}'.format(len(test_dataset)))

    #data_loader = torch.utils.data.DataLoader(
    #    train_dataset, batch_size=2, shuffle=True, num_workers=4,
    #    collate_fn=collate_fn)

    data_loader_test = torch.utils.data.DataLoader(
        test_dataset, batch_size=1, shuffle=False, num_workers=8,
        collate_fn=collate_fn_rcnn)

    #data_loader_test = torch.utils.data.DataLoader(
    #    test_dataset, batch_size=1, shuffle=False, num_workers=4,
    #    collate_fn=collate_fn_test)

    #data_loader_test = torch.utils.data.DataLoader(
    #    test_dataset, batch_size=1, shuffle=False, num_workers=4,
    #    collate_fn=collate_fn_modahuman)
    #use_cuda = torch.cuda.is_available()
    #device_name = "cuda" if use_cuda else "cpu"
    ##device_name = 'cpu'
    #device = torch.device( device_name )

    #backbone = torchvision.models.resnet50(pretrained=True)
    #resnet = backbone
    #modules = list(resnet.children())[:-2]      # delete the last fc layer and the adaptive pool layere.
    #backbone = nn.Sequential(*modules)
    #backbone.out_channels=2048
    #model = torchvision.models.detection.FasterRCNN(backbone, num_classes=2, min_size=400, max_size=400)
    backbone = torchvision.models.detection.backbone_utils.resnet_fpn_backbone('resnet101', pretrained=True)
    #model = torchvision.models.detection.FasterRCNN(backbone, num_classes=2, min_size=400, max_size=400)
    #model = torchvision.models.detection.FasterRCNN(backbone, num_classes=14, min_size=800, max_size=800, box_score_thresh=0.001)
    #model = torchvision.models.detection.FasterRCNN(backbone, num_classes=14, min_size=416, max_size=416, box_score_thresh=0.001)
    model = FasterRCNN(backbone, num_classes=num_classes, cfg=cfg.dnn)
    #get workbench

    #model_path = cfg.path.CHECKPOINT.format(10)
    print('Load model from {}'.format(model_path))
    state_dict = torch.load(model_path, map_location=device)['model_state_dict']
    model.load_state_dict(state_dict, strict=False)
    model.to(device)

    t = my_trainer( cfg, model, device, trainset=None, testset=data_loader_test )

    start = time.time()
    print('start testing')
    #t.eval_result()
    t.test()
    #t.test_speed()
    #path = '/hdd/fashion_videos/frcnn_frames'
    #path = '/hdd/data/datasets/modanet/human_result'
    #path = 'out_path/modanet_frcnn_{}'.format(image_size)
    #if not os.path.isdir(path):
    #    os.mkdir(path)
    #t.draw_result(path)
    #t.test_one(path=path)
    #human_path = 'modanet_human_train.pkl'
    #t.save_human_result(human_path)
    #print('Used {:.2f} seconds to test'.format(time.time()-start))
    #with open('result.pkl', 'wb') as f:
    #    pickle.dump(out, f)

