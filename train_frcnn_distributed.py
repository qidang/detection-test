import _init
import sys
import os
import argparse
import pickle
import platform
import progressbar
import numpy as np
import datetime
from tools import Logger
import torchvision
import math
from torch import nn
import json
import tqdm
import time
from pprint import pprint

from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval
import datetime
from torchcore.data.transforms import RandomMirror

import torch.multiprocessing as mp
import torch.distributed as dist

from torch.nn.parallel import DistributedDataParallel as DDP
from train_frcnn import my_trainer, parse_commandline, get_model

from PIL.ImageDraw import Draw

from rcnn_config import config
from tools import torch_tools
#from data import data_feeder

from torchcore.data.datasets import ModanetDataset, ModanetHumanDataset, COCOPersonDataset, COCODataset, COCOTorchVisionDataset
from torchcore.data.collate import CollateFnRCNN, collate_fn_torchvision

import torch
#torch.multiprocessing.set_sharing_strategy('file_system')
import torchvision.transforms as transforms
import torch.optim as optim

from torchcore.dnn import trainer
from torchcore.dnn.networks.faster_rcnn_fpn import FasterRCNNFPN
from torchcore.dnn.networks.roi_net import RoINet
from torchcore.dnn.networks.rpn import MyAnchorGenerator, MyRegionProposalNetwork
from torchcore.dnn.networks.heads import RPNHead
from torchcore.dnn import networks
from torchcore.data.transforms import Compose, RandomCrop, RandomScale, RandomMirror, ToTensor, Normalize
from torchcore.dnn.networks.tools.data_parallel import DataParallel
from torchcore.engine.launch import launch

def get_absolute_box(human_box, box):
    #box[2]+=int(human_box[0])+box[0]
    #box[3]+=int(human_box[1])+box[1]
    box[0]+=int(human_box[0])
    box[1]+=int(human_box[1])
    return box

def setup(rank, world_size):
    os.environ['MASTER_ADDR'] = 'localhost'
    os.environ['MASTER_PORT'] = '12355'

    # initialize the process group
    dist.init_process_group("nccl", rank=rank, world_size=world_size)


def cleanup():
    dist.destroy_process_group()

class my_trainer_dist(my_trainer):
    def __init__( self, cfg, model, device, trainset, testset=None, dataset_name='modanet', train_sampler=None, rank=None, benchmark=None, criterias=None, val_dataset=None ):
        self._cfg = cfg
        self._device = device
        self._optimizer = None
        self._model = model

        if isinstance(self._model, DDP):
            self.distributed = True
        else:
            self.distributed = False

        self.rank = rank
        self._train_sampler = train_sampler

        self._trainset = trainset
        self._testset = testset
        self._benchmark = benchmark
        self._criterias = criterias
        self._dataset_name = dataset_name
        self._epoch = 0
        self._val_dataset = val_dataset
        self._niter = cfg.optimizer.n_iter

        self._set_optimizer()
        self._set_scheduler()
        if cfg.resume:
            path = os.path.join(os.path.dirname(cfg.path.CHECKPOINT), 'last.pth') 
            self.resume_training(path, device)

        #model = DataParallel(model,
        #    device_ids=None, 
        #    chunk_sizes=None)
        #model.to(device)
        #self._model = model

        if rank==0:
            self.init_logger()

    def train( self, resume=False ):
        #if self._testset is not None :
        #    self._validate()

        for i in range( self._epoch+1, self._niter+1 ):
            if self._train_sampler is not None:
                self._train_sampler.set_epoch(i)
            if self.rank==0 or self.rank==None:
                print("Epoch %d/%d" % (i,self._niter))
                self._logger.info('epoch {}\n'.format(i))
            self._train()

            if self._testset is not None and i%1 == 0 :
                if self.distributed:
                    dist.barrier()
                if not self.distributed or self.rank==0:
                    self._validate()
                if self.distributed:
                    dist.barrier()
            self._epoch = i
            self.save_training(self._cfg.path.CHECKPOINT.format(self._epoch))
            if hasattr(self, '_scheduler'):
                self._scheduler.step()

    def validate_onece(self):
        self._validate()

    def _validate( self ):
        if isinstance(self._model, DDP):
            if self.rank != 0:
                return
        print('start to validate')
        self._model.eval()

        results = []
        with torch.no_grad() :
            for idx,(inputs, targets) in enumerate(tqdm.tqdm(self._testset, 'evaluating')):
            #for idx,(inputs, targets) in enumerate(self._testset):
                inputs = self._set_device( inputs )
                output = self._model.module( inputs)
                batch_size = len(output['boxes'])
                #for i, im in enumerate(output):
                for i in range(batch_size):
                    if len(output['boxes'][i]) == 0:
                        continue
                    # convert to xywh
                    output['boxes'][i][:,2] -= output['boxes'][i][:,0]
                    output['boxes'][i][:,3] -= output['boxes'][i][:,1]
                    for j in range(len(output['boxes'][i])):
                        results.append({'image_id':int(targets[i]['image_id']), 
                                        'category_id':output['labels'][i][j].cpu().numpy().tolist(), 
                                        'bbox':output['boxes'][i][j].cpu().numpy().tolist(), 
                                        'score':output['scores'][i][j].cpu().numpy().tolist()})
                #if idx == 10:
                #    break
                #output = self._model['net']( inputs, just_embedding=True) # debug
                #bench.update( targets, output )
        with open('temp_result.json','w') as f:
            json.dump(results,f)
        self.eval_result(dataset=self._dataset_name)
    

    #def eval_result(self, dataset='coco_person'):
    #    if dataset not in ['coco_person', 'modanet']:
    #        raise ValueError('only support coco_person and modanet dataset')
    #    if dataset == 'coco_person':
    #        gt_json=os.path.expanduser('~/data/datasets/COCO/annotations/instances_val2014.json')
    #    else:
    #        gt_json=os.path.expanduser('~/data/datasets/modanet/Annots/modanet_instances_val.json')
    #    dt_json='temp_result.json'

    #    # we need to map the category ids back
    #    if dataset == 'coco':
    #        print('revise coco dataset label to gt')
    #        cat_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 27, 28, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 67, 70, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 84, 85, 86, 87, 88, 89, 90]
    #        with open(dt_json) as f:
    #            results = json.load(f)
    #            for result in results:
    #                temp_id = result['category_id']
    #                result['category_id'] = cat_ids[temp_id-1]
    #        with open('temp_result.json','w') as f:
    #            json.dump(results,f)

    #    annType = 'bbox'
    #    cocoGt=COCO(gt_json)
    #    cocoDt=cocoGt.loadRes(dt_json)

    #    imgIds=sorted(cocoGt.getImgIds())

    #    # running evaluation
    #    cocoEval = COCOeval(cocoGt,cocoDt,annType)
    #    if dataset == 'coco_person':
    #        cocoEval.params.catIds = [1]
    #    cocoEval.params.imgIds = imgIds
    #    cocoEval.evaluate()
    #    cocoEval.accumulate()
    #    cocoEval.summarize()

    def _train( self ):
        self._model.train()

        loss_values = []
        average_losses = {}

        self._optimizer.zero_grad()
        for idx, (inputs, targets) in enumerate(tqdm.tqdm(self._trainset, desc='Training')):
            #print('inputs:', inputs)
            #print('targets:', targets)

            inputs = self._set_device( inputs )
            targets = self._set_device( targets )
            #print('input device:', inputs[0].device)

            loss_dict = self._model(inputs, targets)

            # add the losses for each part
            #loss_sum = sum(loss for loss in loss_dict.values())
            loss_sum=0
            for single_loss in loss_dict:
                loss_sum = loss_sum + loss_dict[single_loss]
                if self.rank==0 or not self.distributed:
                    if single_loss in average_losses:
                        average_losses[single_loss]+= loss_dict[single_loss].mean()
                    else:
                        average_losses[single_loss] = loss_dict[single_loss].mean()

            loss_sum = loss_sum.mean()
            if not math.isfinite(loss_sum):
                #print("Loss is {}, stopping training".format(loss_sum))
                self._optimizer.zero_grad()
                print('wrong targets:',targets)
                print("Loss is {}, skip this batch".format(loss_sum))
                print(loss_dict)
                continue
                #sys.exit(1)

            # Computing gradient and do SGD step
            loss_sum.backward()
            if idx % self._cfg.accumulation_step == 0:
                self._optimizer.step()
                self._optimizer.zero_grad()
            loss_values.append( loss_sum.cpu().detach().numpy() )

            if idx%100 == 0:
                if self.rank == 0 or not self.distributed:
                    self._logger.info('{} '.format(idx+1))
                    loss_str = ''
                    for loss in average_losses:
                        if idx==0:
                            loss_num = average_losses[loss]
                        else:
                            loss_num = average_losses[loss] / 100
                        self._logger.info('{} '.format(loss_num))
                        loss_str += (' {} loss:{}, '.format(loss, loss_num))
                    print(loss_str[:-2])
                    average_losses = {}
                    self._logger.info('\n')

            #if idx > 20:
            #    break
            

            #bar.update(idx+1,loss=loss_sum.item())
        #bar.finish()

        print('Average loss : ', np.mean(loss_values))

    def _set_device( self, blobs ):
        if type(blobs) == list:
            for i in range(len(blobs)):
                blobs[i] = self._set_device(blobs[i])
        elif type(blobs) == dict:
            for key, data in blobs.items():
                blobs[key] = self._set_device(data)
        elif torch.is_tensor(blobs):
            blobs = blobs.to(self._device, non_blocking=True)
        return blobs

    def _set_scheduler(self):
        scheduler = self._cfg.scheduler
        if scheduler.type == 'multi_step':
            self._scheduler = optim.lr_scheduler.MultiStepLR( self._optimizer,
                                                            milestones=scheduler['milestones'],
                                                            gamma=scheduler.get('gamma', 0.1))
        else:
            raise ValueError('unknow scheduler {}'.format(scheduler.type))

    def init_logger(self):
        train_path = self._cfg.path['LOG']

        console_formatter = '{} {{}}'.format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        self._logger = Logger(level='info', file=train_path, console=False, console_formatter=console_formatter)

        print('Loss log path is: {}'.format(train_path))

    def save_training(self, path, to_print=True):
        if isinstance(self._model, DDP):
            if self.rank == 0:
                state_dict = self._model.state_dict()
            else:
                return
        elif isinstance(self._model, torch.nn.DataParallel):
            state_dict = self._model.module.state_dict()
        else:
            state_dict =self._model.state_dict()
        torch.save({
            'epoch': self._epoch,
            'model_state_dict': state_dict,
            'optimizer_state_dict': self._optimizer.state_dict(),
            'scheduler':self._scheduler.state_dict(),
            'niter':self._niter
        }, path)
        folder = os.path.dirname(path)
        torch.save({
            'epoch': self._epoch,
            'model_state_dict': state_dict,
            'optimizer_state_dict': self._optimizer.state_dict(),
            'scheduler':self._scheduler.state_dict(),
            'niter':self._niter
        }, os.path.join(folder, 'last.pth'))
        if to_print:
            print('The checkpoint has been saved to {}'.format(path))

    def resume_training(self, path, device, to_print=True):
        if isinstance(self._model, DDP):
            dist.barrier()
        checkpoint = torch.load(path, map_location=device)
        self._epoch = checkpoint['epoch']
        self._model.load_state_dict(checkpoint['model_state_dict'])
        self._optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        self._niter = self._niter
        if 'scheduler' in checkpoint:
            self._scheduler.load_state_dict(checkpoint['scheduler'])
        if to_print:
            print('Chekpoint has been loaded from {}'.format(path))

def load_checkpoint(model, path, device, to_print=True):
    #checkpoint = torch.load(path)
    state_dict_ = torch.load(path, map_location=device)['model_state_dict']
    state_dict = {}
    for k in state_dict_:
        if k.startswith('module') and not k.startswith('module_list'):
            state_dict[k[7:]] = state_dict_[k]
        else:
            state_dict[k] = state_dict_[k]
    model.load_state_dict(state_dict, strict=True )
    #self._epoch = checkpoint['epoch']
    #self._model.load_state_dict(checkpoint['model_state_dict'])
    #self._optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    if to_print:
        print('Chekpoint has been loaded from {}'.format(path))

def run(args):
    rank = dist.get_rank()
    #setup(rank, world_size)
    params = {}
    params['config'] = 'frcnn_{}'.format(args.dataset)
    params['tag'] = args.tag
    params['path'] = {}
    batch_size_per_gpu_per_accumulation = args.batch_size

    params['path']['project'] = os.path.expanduser('~/Vision/data')
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    #device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )
    cfg.resume = args.resume
    cfg.out_feature_num = 256
    cfg.accumulation_step = args.accumulation_step
    cfg.nms_thresh = 0.5
    cfg.batch_size = args.batch_size
    cfg.optimizer.lr = cfg.optimizer.lr / args.accumulation_step
    #cfg.min_size = (640, 672, 704, 736, 768, 800)
    cfg.min_size = 416
    cfg.max_size = 416
    cfg.torchvision_model = args.torchvision_model

    #set the paths to save all the results (model, val result)
    cfg.build_path( params['tag'], args.dataset, model_hash='frcnn' )
    if rank==0:
        print(cfg)

    transforms = RandomMirror(probability=0.5)
    xyxy = True

    if args.dataset == 'coco_person':
        root = os.path.expanduser('~/data/datasets/COCO')
        anno = os.path.expanduser('~/data/annotations/coco2014_instances_person_all.pkl')
        dataset = COCOPersonDataset(root, anno, part='train2014', transforms=transforms)
        test_dataset = COCOPersonDataset(root, anno, part='val2014', transforms=None)
        cfg.class_num = 1
    elif args.dataset == 'modanet':
        anno_path = os.path.expanduser('~/data/annotations/modanet2018_instances_revised.pkl')
        root = os.path.expanduser('~/data/datasets/modanet/Images')
        dataset = ModanetDataset(anno=anno_path, part='train', root=root, transforms=transforms) # the default transform in faster rcnn will be used
        test_dataset = ModanetDataset(anno=anno_path, part='val', root=root, transforms=None) # the default transform in faster rcnn will be used
        cfg.class_num = 13
    elif args.dataset == 'coco':
        anno_path = os.path.expanduser('~/data/annotations/coco2017_instances.pkl')
        root = os.path.expanduser('~/data/datasets/COCO')
        if args.torchvision_model:
            dataset = COCOTorchVisionDataset(anno=anno_path, part='train2017', root=root, transforms=transforms ) # the default transform in faster rcnn will be used
            test_dataset = COCOTorchVisionDataset(anno=anno_path, part='val2017', root=root, transforms=None ) # the default transform in faster rcnn will be used
            cfg.class_num = 81
        else:
            dataset = COCODataset(anno=anno_path, part='train2017', root=root, transforms=transforms, xyxy=xyxy) # the default transform in faster rcnn will be used
            test_dataset = COCODataset(anno=anno_path, part='val2017', root=root, transforms=None, xyxy=xyxy) # the default transform in faster rcnn will be used
            cfg.class_num = 80
    cfg.roi_head.class_num = cfg.class_num
    collate_fn_rcnn = CollateFnRCNN(min_size=cfg.min_size, max_size=cfg.max_size)

    if args.torchvision_model:
        collate_fn_rcnn = collate_fn_torchvision



    #use_cuda = torch.cuda.is_available()
    #device_name = "cuda" if use_cuda else "cpu"
    #device = torch.device( device_name )


    train_sampler = torch.utils.data.distributed.DistributedSampler(dataset)

    data_loader = torch.utils.data.DataLoader(
            dataset, 
            batch_size=batch_size_per_gpu_per_accumulation, 
            shuffle=(train_sampler is None), 
            num_workers=2,
            sampler=train_sampler,
            pin_memory=True,
            drop_last=False,
            collate_fn=collate_fn_rcnn)

    test_data_loader = torch.utils.data.DataLoader(
      test_dataset, 
      batch_size=1, 
      shuffle=False,
      num_workers=2,
      pin_memory=True,
      drop_last=False,
      collate_fn=collate_fn_rcnn
    )

    #data_loader_test = torch.utils.data.DataLoader(
    #    val_dataset, batch_size=1, shuffle=False, num_workers=4,
    #    collate_fn=collate_fn_rcnn)

    #use_cuda = torch.cuda.is_available()
    #device_name = "cuda" if use_cuda else "cpu"
    device = torch.device( rank )

    model = get_model(cfg, torchvision_model=args.torchvision_model)
    model = model.to(rank)
    ddp_model = DDP(model, device_ids=[rank])

    #load_checkpoint(model, cfg.path.CHECKPOINT.format(9), device)

    t = my_trainer_dist( cfg, ddp_model, device, data_loader, testset=test_data_loader, dataset_name=args.dataset, train_sampler=train_sampler, rank=rank, benchmark=None, val_dataset=None )
    t.train()

    cleanup()

def main(world_size, args):
    launch(run, num_gpus_per_machine=2, args=(args,), dist_url='auto')
    #mp.spawn(run,
    #         args=(world_size, args),
    #         nprocs=world_size,
    #         join=True)

if __name__=="__main__" :
    args = parse_commandline()
    world_size = 2
    main(world_size, args)
    
