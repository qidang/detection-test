from __future__ import division

import sys
sys.path.append('..')
sys.path.append('../pytorch_yolov3')
from pytorch_yolov3.models import *
from pytorch_yolov3.utils.utils import *
from pytorch_yolov3.utils.datasets import *
from pytorch_yolov3.utils.parse_config import *
from pytorch_yolov3.utils.data_prefetcher import data_prefetcher

import os
import time
import datetime
import argparse
import tqdm

import torch
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision import transforms
from torch.autograd import Variable
import torch.optim as optim

from PIL.ImageDraw import Draw
from PIL import ImageFont

import json
from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval
from pytorch_yolov3.utils.modanet_human_dataset import ModanetHumanDataset, crop_pad_img

from torchvision.ops import nms

from torchcore.tools.visulize_tools import draw_boxes, save_images, draw_and_save

CLASS_NAMES = ['bag', 'belt', 'boots', 'footwear', 'outer', 'dress',
               'sunglasses', 'pants', 'top', 'shorts', 'skirt',
               'headware', 'scarf&tie'
               ]

CLASS_NAME_HUMAN = ['human']

FONT_PATH = '/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf'

def get_max_score_box(boxes):
    if boxes == []:
        return []
    score = boxes[:,4]*boxes[:,5]
    ind = np.argmax(score)
    return boxes[ind]

def get_biggest_box(boxes):
    if boxes == []:
        return []
    area = (boxes[:,2]-boxes[:,0]) * (boxes[:,3]-boxes[:,1])
    ind = np.argmax(area)
    return boxes[ind]

# mainly for generate pickle file
def reconstruct_boxes(batch_boxes, pads, scales):
    reconstructed_boxes = []
    for pad, scale, boxes in zip(pads, scales, batch_boxes):
        if len(boxes)==0:
            reconstructed_boxes.append([])
            continue
        boxes = boxes.detach().cpu().numpy()
        #bboxes = boxes[:,:4]
        #obj_score = boxes[:,4]
        #cat_score = boxes[:,5]
        #category = boxes[:,6]
        boxes[:,:4] = boxes[:,:4]*scale
        boxes[:, 0] = np.maximum(0, boxes[:,0]-pad[0])
        boxes[:, 2] = np.maximum(0, boxes[:,2]-pad[0])
        boxes[:, 1] = boxes[:,1]-pad[2]
        boxes[:, 3] = boxes[:,3]-pad[2]
        reconstructed_boxes.append(boxes)
    return reconstructed_boxes


        

def get_absolute_xywh_box(human_box, box):
    box = box[:4].tolist()
    box[2] = int(box[2] - box[0])
    box[3] = int(box[3] - box[1])
    box[0]+=int(human_box[0])
    box[1]+=int(human_box[1])
    return box

def get_absolute_box(human_box, box):
    box[:,0] = box[:,0]+human_box[0]
    box[:,1] = box[:,1]+human_box[1]
    box[:,2] = box[:,2]+human_box[0]
    box[:,3] = box[:,3]+human_box[1]
    return box
    

def test_two_stage_speed(model_human, model_garment, dataloader, conf_thres, nms_thres, batch_size, image_size, out_path, class_num ):
    print('start to use two stage model to evaluate the data')
    model_human.eval()
    model_garment.eval()

    Tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor

    start = time.time()
    im_num = 0
    time_crop = 0
    time_run_modanet = 0
    time_human_detection = 0
    #for batch_i, ( paths, imgs, targets) in enumerate(tqdm.tqdm(dataloader, desc="Detecting objects")):
    for batch_i, (inputs, targets) in enumerate(dataloader):
        #images = extract_imgs(imgs)
        images = inputs['ori_image']
        imgs = inputs['data']

        pads = inputs['pad']
        scales = inputs['scale']
        im_ids = inputs['im_id']
        im_num+=len(imgs)

        human_start = time.time()
        imgs = Variable(imgs.type(Tensor), requires_grad=False)

        with torch.no_grad():
            outputs = model_human(imgs)
            batch_boxes, batch_class_scores, batch_pred_scores, batch_pred_class = nms_split(outputs, conf_thres=conf_thres, nms_thres=nms_thres)
            #outputs = nms_new(outputs, conf_thres=conf_thres, nms_thres=nms_thres)
            '''
            outputs:
            list([(x1, y1, x2, y2, object_conf, class_score, class_pred)...]...)
            '''

            # if there is no human detection
            out_temp = [x for x in batch_boxes if x is not None]
            if out_temp == []:
                time_human_detection += time.time() - human_start
                continue

            human_boxes = reconstruct_boxes(batch_boxes, pads, scales)
            time_human_detection += time.time() - human_start

            start_crop = time.time()
            # get new inputs for second model with human boxes and paths
            cropped_imgs, params = crop_pad_img(human_boxes, images, image_size)

            pads_cropped = params['pad']
            scale_cropped = params['scale']
            batch_id_cropped = params['batch_id']

            cropped_imgs = Variable(cropped_imgs.type(Tensor), requires_grad=False)
            time_crop+= time.time()-start_crop
            garment_start = time.time()

            # second stage detection
            garment_batch_size = 10
            crop_images_batch = [cropped_imgs[x:x+garment_batch_size] for x in range(0, len(cropped_imgs), garment_batch_size)]
            outputs_garments = []
            for single_batch in crop_images_batch:
                single_batch = Variable(single_batch.type(Tensor), requires_grad=False)
                # second stage detection
                outputs_single =  model_garment(single_batch)
                outputs_garments.extend(nms_new(outputs_single, conf_thres=conf_thres, nms_thres=nms_thres))
            #garment_boxes =  model_garment(cropped_imgs)
            #outputs_garments = nms_new(outputs_garments, conf_thres=conf_thres, nms_thres=nms_thres)
            #batch_boxes_garment, batch_class_scores_garment, batch_pred_scores_garment, batch_pred_class_garment = nms_split(outputs_garments, conf_thres=conf_thres, nms_thres=nms_thres)

            garment_boxes = reconstruct_boxes(outputs_garments, pads_cropped, scale_cropped)
            
            boxes_draw = [[] for _ in range(len(imgs))]
            human_boxes_new = [box for box in human_boxes if box != []]
            human_boxes_new = np.row_stack(human_boxes_new)
            for box, batch_id, human_box in zip(garment_boxes, batch_id_cropped, human_boxes_new):
                # using human boxes to recover the garment boxes
                if box == []:
                    continue
                box = get_absolute_box(human_box, box)
                boxes_draw[batch_id].extend(box)
            time_run_modanet += time.time() - garment_start
    total_time = time.time() - start
    time_per_im = total_time / im_num
    time_per_crop = time_crop / im_num
    time_per_human = time_human_detection / im_num
    time_per_garment = time_run_modanet / im_num
    print('Using {} seconds to detect all the images'.format(total_time))
    print('time per image is {}'.format(time_per_im))
    print('time per crop is {}'.format(time_per_crop))
    print('time per human is {}'.format(time_per_human))
    print('time for garment is {}'.format(time_per_garment))
    print('Processing speed is {}'.format(1/time_per_im))
    print('image number is {}'.format(im_num))

def test_two_stage(model_human, model_garment, dataloader, conf_thres, nms_thres, batch_size, image_size, out_path, class_num ):
    print('start to use two stage model to evaluate the data')
    model_human.eval()
    model_garment.eval()

    Tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor

    im_num = 0
    time_crop = 0
    time_garment_detection = 0
    time_human_detection = 0
    start = time.time()
    for batch_i, (inputs, targets) in enumerate(tqdm.tqdm(dataloader, desc="Detecting objects")):
        ind_start = batch_i * batch_size + 1
        #images = extract_imgs(imgs)
        images = inputs['ori_image']
        imgs = inputs['data']
        #for path in paths:
        #    images.append(Image.open(path).convert('RGB'))

        pads = inputs['pad']
        scales = inputs['scale']
        im_ids = inputs['im_id']
        im_num+=len(imgs)

        imgs = Variable(imgs.type(Tensor), requires_grad=False)

        with torch.no_grad():
            human_start = time.time()
            outputs = model_human(imgs)
            human_boxes, human_class_scores, human_pred_scores, human_pred_class = nms_split(outputs, conf_thres=conf_thres, nms_thres=nms_thres)
            #outputs = nms_new(outputs, conf_thres=conf_thres, nms_thres=nms_thres)
            '''
            outputs:
            list([(x1, y1, x2, y2, object_conf, class_score, class_pred)...]...)
            '''

            # if there is no human detection
            out_temp = [x for x in human_boxes if len(x)>0]
            if out_temp == []:
                time_human_detection += time.time() - human_start
                save_images(images, out_path, ind_start)
                continue

            human_boxes = reconstruct_boxes(human_boxes, pads, scales)
            human_scores = [pre_score*class_score for pre_score, class_score in zip(human_pred_scores, human_class_scores) if len(pre_score)>0]
            time_human_detection += time.time() - human_start

            crop_start = time.time()
            # get new inputs for second model with human boxes and paths
            cropped_imgs, params = crop_pad_img(human_boxes, images, image_size)
            time_crop += time.time() - crop_start

            pads_cropped = params['pad']
            scale_cropped = params['scale']
            batch_id_cropped = params['batch_id']

            garment_batch_size = 10
            crop_images_batch = [cropped_imgs[x:x+garment_batch_size] for x in range(0, len(cropped_imgs), garment_batch_size)]
            outputs_garments = []
            garment_scores = []
            garment_classes = []
            garment_start = time.time()
            for single_batch in crop_images_batch:
                single_batch = Variable(single_batch.type(Tensor), requires_grad=False)
                # second stage detection
                outputs_single =  model_garment(single_batch)
                garment_boxes, garment_class_scores, garment_pred_scores, garment_pred_class = nms_split(outputs_single, conf_thres=conf_thres, nms_thres=nms_thres)
                outputs_garments.extend(garment_boxes)
                temp_score = [class_score*garment_score if len(class_score)>0 else [] for class_score, garment_score in zip(garment_class_scores, garment_pred_scores)]
                garment_scores.extend(temp_score)
                garment_classes.extend(garment_pred_class)

            garment_boxes = reconstruct_boxes(outputs_garments, pads_cropped, scale_cropped)
            time_garment_detection += time.time() - garment_start
            
            boxes_draw = [[] for _ in range(len(imgs))]
            human_boxes_new = [box for box in human_boxes if box != []]
            human_boxes_new = np.row_stack(human_boxes_new)
            for box, batch_id, human_box in zip(garment_boxes, batch_id_cropped, human_boxes_new):
                # using human boxes to recover the garment boxes
                if box == []:
                    continue
                box = get_absolute_box(human_box, box)
                boxes_draw[batch_id].extend(box)
        draw_boxes(images, human_boxes, human_scores, human_pred_class, CLASS_NAME_HUMAN)
        draw_and_save(images, boxes_draw, garment_scores, garment_classes, out_path, ind_start, CLASS_NAMES)
    total_time = time.time() - start
    time_per_im = total_time / im_num
    time_per_crop = time_crop / im_num
    time_per_human = time_human_detection / im_num
    time_per_garment = time_garment_detection / im_num
    print('Using {} seconds to detect all the images'.format(total_time))
    print('time per image is {}'.format(time_per_im))
    print('time per crop is {}'.format(time_per_crop))
    print('time per human is {}'.format(time_per_human))
    print('time for garment is {}'.format(time_per_garment))
    print('Processing speed is {}'.format(1/time_per_im))
    print('image number is {}'.format(im_num))

def evaluate_modanet_human(model, anno_path, image_root, iou_thres, conf_thres, nms_thres, img_size, batch_size):
    model.eval()

    # Get dataloader
    #dataset = ListDataset(path, img_size=img_size, augment=False, multiscale=False)
    #dataset = ModanetListDataset(anno_path, image_root, part='val', img_size=416)
    dataset = ModanetHumanDataset(anno_path, image_root, 'val', image_size=img_size)
    dataloader = torch.utils.data.DataLoader(
        dataset, batch_size=batch_size, shuffle=False, num_workers=1, collate_fn=dataset.collate_fn
    )

    Tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor

    labels = []
    sample_metrics = []  # List of tuples (TP, confs, pred)
    for batch_i, (_, imgs, targets) in enumerate(tqdm.tqdm(dataloader, desc="Detecting objects")):
        targets = targets['origin']

        # Extract labels
        labels += targets[:, 1].tolist()
        # Rescale target
        targets[:, 2:] = xywh2xyxy(targets[:, 2:])
        targets[:, 2:] *= img_size

        imgs = Variable(imgs.type(Tensor), requires_grad=False)

        with torch.no_grad():
            outputs = model(imgs)
            outputs = non_max_suppression(outputs, conf_thres=conf_thres, nms_thres=nms_thres)

        sample_metrics += get_batch_statistics(outputs, targets, iou_threshold=iou_thres)

    # Concatenate sample statistics
    true_positives, pred_scores, pred_labels = [np.concatenate(x, 0) for x in list(zip(*sample_metrics))]
    precision, recall, AP, f1, ap_class = ap_per_class(true_positives, pred_scores, pred_labels, labels)

    return precision, recall, AP, f1, ap_class
def evaluate_modanet(model, anno_path, image_root, iou_thres, conf_thres, nms_thres, img_size, batch_size):
    model.eval()

    # Get dataloader
    #dataset = ListDataset(path, img_size=img_size, augment=False, multiscale=False)
    dataset = ModanetListDataset(anno_path, image_root, part='val', img_size=416)
    dataloader = torch.utils.data.DataLoader(
        dataset, batch_size=batch_size, shuffle=False, num_workers=1, collate_fn=dataset.collate_fn
    )

    Tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor

    labels = []
    sample_metrics = []  # List of tuples (TP, confs, pred)
    for batch_i, (_, imgs, targets) in enumerate(tqdm.tqdm(dataloader, desc="Detecting objects")):
        targets = targets['origin']

        # Extract labels
        labels += targets[:, 1].tolist()
        # Rescale target
        targets[:, 2:] = xywh2xyxy(targets[:, 2:])
        targets[:, 2:] *= img_size

        imgs = Variable(imgs.type(Tensor), requires_grad=False)

        with torch.no_grad():
            outputs = model(imgs)
            outputs = non_max_suppression(outputs, conf_thres=conf_thres, nms_thres=nms_thres)

        sample_metrics += get_batch_statistics(outputs, targets, iou_threshold=iou_thres)

    # Concatenate sample statistics
    true_positives, pred_scores, pred_labels = [np.concatenate(x, 0) for x in list(zip(*sample_metrics))]
    precision, recall, AP, f1, ap_class = ap_per_class(true_positives, pred_scores, pred_labels, labels)

    return precision, recall, AP, f1, ap_class

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--batch_size", type=int, default=8, help="size of each image batch")
    parser.add_argument("--model_human", type=str, default="config/yolov3.cfg", help="path to model definition file")
    parser.add_argument("--model_garment", type=str, default="config/yolov3-garment.cfg", help="path to model definition file")
    parser.add_argument("--data_config", type=str, default="config/coco.data", help="path to data config file")
    parser.add_argument("--weights_path_human", type=str, default="weights/yolov3.weights", help="path to weights file")
    parser.add_argument("--weights_path_garment", type=str, default="weights/yolov3.weights", help="path to weights file")
    parser.add_argument("--class_path", type=str, default="data/coco.names", help="path to class label file")
    parser.add_argument("--iou_thres", type=float, default=0.5, help="iou threshold required to qualify as detected")
    parser.add_argument("--conf_thres", type=float, default=0.001, help="object confidence threshold")
    parser.add_argument("--nms_thres", type=float, default=0.5, help="iou thresshold for non-maximum suppression")
    parser.add_argument("--n_cpu", type=int, default=8, help="number of cpu threads to use during batch generation")
    parser.add_argument("--human_size", type=int, default=416, help="size of each image dimension")
    parser.add_argument("--garment_size", type=int, default=416, help="size of each image dimension")
    parser.add_argument("--extend_box", type=float, default=0, help="percentage of box extention")
    opt = parser.parse_args()
    print(opt)
    #out_path = '/hdd/fashion_videos/yolo_frames'
    #out_path = '/hdd/data/datasets/modanet/person_detection_result'
    list_path = 'data/coco/valperson_all.txt'
    out_path = '/hdd/data/datasets/modanet/detection_result'
    out_path = 'out_path/human_yolo_{}_modanet_yolo_{}_max'.format(opt.human_size, opt.garment_size)
    if not os.path.isdir(out_path):
        os.mkdir(out_path)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    data_config = parse_data_config(opt.data_config)
    valid_path = data_config["valid"]
    class_names = load_classes(data_config["names"])
    class_num = int(data_config["classes"])

    if 'anno_path' in data_config and 'image_root' in data_config:
        anno_path = data_config['anno_path']
        image_root = data_config['image_root']

    # Initiate model
    model_human = Darknet(opt.model_human).to(device)
    model_garment = Darknet(opt.model_garment).to(device)
    if opt.weights_path_human.endswith(".weights"):
        # Load darknet weights
        model.load_darknet_weights(opt.weights_path)
    else:
        # Load checkpoint weights
        model_human.load_state_dict(torch.load(opt.weights_path_human))
        model_garment.load_state_dict(torch.load(opt.weights_path_garment))

    # Get dataloader
    #dataset = ModanetHumanDataset(anno_path, image_root, 'val', use_revised_box=False, image_size=416, box_extend=opt.extend_box, augment=False, multiscale=False)
    dataset = NewListDataset(valid_path, img_size=opt.human_size, augment=False, multiscale=False)
    dataloader = torch.utils.data.DataLoader(
        dataset, batch_size=opt.batch_size, shuffle=False, num_workers=1, collate_fn=dataset.collate_fn
    )
    test_two_stage(
                model_human, 
                model_garment, 
                dataloader, 
                opt.conf_thres, 
                opt.nms_thres, 
                batch_size=opt.batch_size,
                image_size=opt.garment_size,
                out_path=out_path,
                class_num=13
                )
