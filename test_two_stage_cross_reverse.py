import _init
import os
import argparse
import pickle
import platform
import progressbar
import numpy as np
from torch import nn
import datetime
import json
import tqdm

from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval

from PIL.ImageDraw import Draw

from data.datasets import load_dataset
from rcnn_config import config
from tools import torch_tools
from data import data_feeder


#from data.datasets import ModanetDataset
from rcnn_dnn.data.imageset_modanet import ModanetDataset
from rcnn_dnn.data.imageset_coco import COCODataset
from rcnn_dnn.data.imageset_test import TestDataset
#from torchcore.data.datasets import ListDataset
from rcnn_dnn.networks import networks
from rcnn_dnn.data.collate_fn import collate_fn, collate_fn_test, collate_fn_modahuman, CollateFnRCNN
from torchcore.data.transforms import GeneralRCNNTransform
from torchcore.dnn.networks.faster_rcnn import FasterRCNN
from torchcore.tools.visulize_tools import draw_and_save, draw_boxes

import sys
sys.path.append('..')
sys.path.append('../pytorch_yolov3')
from pytorch_yolov3.utils.modanet_human_dataset import crop_pad_img
from pytorch_yolov3.test_two_stage import reconstruct_boxes
from pytorch_yolov3.utils.utils import nms_split
from pytorch_yolov3.models import Darknet
from pytorch_yolov3.utils.datasets import NewListDataset

import torch
import torchvision
import time
torch.multiprocessing.set_sharing_strategy('file_system')
import torchvision.transforms as transforms
import torch.optim as optim

from sklearn.metrics import precision_recall_curve
from sklearn.metrics import average_precision_score
from inspect import signature
import matplotlib.pyplot as plt

from dnn import trainer
import random
from PIL import Image

CLASS_NAMES = ['bag', 'belt', 'boots', 'footwear', 'outer', 'dress',
               'sunglasses', 'pants', 'top', 'shorts', 'skirt',
               'headware', 'scarf&tie'
               ]

CLASS_NAME_HUMAN = ['human']

FONT_PATH = '/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf'
def parse_commandline():
    parser = argparse.ArgumentParser(description="Test the Model")
    #parser.add_argument('-c','--config',help='Configuration Name', required=True)
    #parser.add_argument('--patchsize',help='Patch Size', required=True)
    parser.add_argument('-b','--batch_size',help='Batch size', required=True)
    parser.add_argument('-t','--tag',help='Model tag', required=True)
    #parser.add_argument('-g','--gpu',help='GPU Index', default='0')
    #parser.add_argument('--datasetpath',help='Path to the dataset',required=True)
    #parser.add_argument('--projectpath',help='Path to the project',required=True)
    return vars(parser.parse_args())


def get_max_score_box(boxes):
    if len(boxes)== 0:
        return []
    ind = np.argmax(boxes[:,4])
    box = boxes[ind][:4]
    return box
    
def crop_images(images, batch_boxes):
    out_ims = []
    batch_id = []
    for i, (image, boxes) in enumerate(zip(images, batch_boxes)):
        for box in boxes:
            out_ims.append(image.crop(box))
            batch_id.append(i)
    return out_ims, batch_id

def get_absolute_box(human_box, box):
    box[:,0] = box[:,0]+human_box[0]
    box[:,1] = box[:,1]+human_box[1]
    box[:,2] = box[:,2]+human_box[0]
    box[:,3] = box[:,3]+human_box[1]
    return box

class my_trainer(trainer):

    def _validate( self ):
        print('start to validate')
        self._model.eval()

        results = []
        with torch.no_grad() :
            for idx,(inputs, targets) in enumerate(self._testset):
                inputs = self._set_device( inputs )
                output = self._model( inputs)
                for i, im in enumerate(output):
                    if len(im['boxes']) == 0:
                        continue
                    # convert to xywh
                    im['boxes'][:,2] -= im['boxes'][:,0]
                    im['boxes'][:,3] -= im['boxes'][:,1]
                    for j in range(len(im['boxes'])):
                        results.append({'image_id':targets[i]['image_id'].detach().cpu().numpy().tolist(), 
                                        'category_id':im['labels'][j].cpu().numpy().tolist(), 
                                        'bbox':im['boxes'][j].cpu().numpy().tolist(), 
                                        'score':im['scores'][j].cpu().numpy().tolist()})
                if idx % 1000==0:
                    print('{}, {} / {}'.format(datetime.datetime.now(), idx, len(self._testset)))
                #output = self._model['net']( inputs, just_embedding=True) # debug
                #bench.update( targets, output )
        with open('val_result.json','w') as f:
            json.dump(results,f)
        self.eval_result()
    
    def test_speed( self ):
        print('start to validate')
        self._model.eval()

        start = time.time()
        im_num = 0
        with torch.no_grad() :
            for idx,(inputs, targets) in enumerate(self._testset):
                im_num+= len(inputs)
                inputs = self._set_device( inputs )
                output = self._model( inputs)

            time_per_im = (time.time() - start) / im_num
            print('Detection time per image is {}'.format(time_per_im))
            print('Detecting speed is {} FPS'.format(1.0 / time_per_im))

    def eval_result(self):
        gt_json='/ssd/data/datasets/COCO/annotations/instances_val2014.json'
        dt_json='temp_result.json'
        annType = 'bbox'
        cocoGt=COCO(gt_json)
        cocoDt=cocoGt.loadRes(dt_json)

        imgIds=sorted(cocoGt.getImgIds())

        # running evaluation
        cocoEval = COCOeval(cocoGt,cocoDt,annType)
        cocoEval.params.catIds = [1]
        cocoEval.params.imgIds = imgIds
        cocoEval.evaluate()
        cocoEval.accumulate()
        cocoEval.summarize()

    def test(self):
        self._validate()


    def test_two_stage_cross(self, path, human_size, garment_size):
        print('Speed testing and result drawing for yolo human {} and frcnn garment {} detections'.format(human_size, garment_size))
        self._model_human.eval()
        self._model_garment.eval()
        image_num = 0
        human_detection_time = 0
        garment_detection_time = 0
        draw_image_time = 0
        crop_image_time = 0
        start =time.time()
        for idx, (inputs, targets) in enumerate(tqdm.tqdm(self._testset, desc='Detecting Objects')):
            #images = self.extract_imgs(inputs)
            #image_path = inputs['path']
            #image_path = paths
            #images = [Image.open(im_path).convert('RGB') for im_path in image_path]
            images = inputs['ori_image']
            image_num += len(images)
            pads = inputs['pad']
            scales = inputs['scale']
            inputs = self._set_device(inputs)
            human_start = time.time()
            outputs = self._model_human(inputs['data'])
            human_boxes, human_class_scores, human_pred_scores, human_pred_class = nms_split(outputs, conf_thres=0.8, nms_thres=0.5)
            '''
            outputs:
            list([(x1, y1, x2, y2, object_conf, class_score, class_pred)...]...)
            '''

            # if there is no human detection
            out_temp = [x for x in human_boxes if len(x)>0 ]
            if out_temp == []:
                human_detection_time += time.time() - human_start
                continue

            human_boxes = reconstruct_boxes(human_boxes, pads, scales)
            human_scores = [pre_score*class_score for pre_score, class_score in zip(human_pred_scores, human_class_scores) if len(pre_score)>0]
            human_detection_time += time.time() - human_start
            
            crop_start = time.time()
            cropped_im, batch_id = crop_images(images, human_boxes)
            crop_image_time += time.time() - crop_start
            inputs_garment = [{'data': im} for im in cropped_im]
            targets = None
            # split the inputs to several batches
            sub_batch_size = 10
            inputs_num = len(inputs_garment)
            inputs_garment_batch = [ inputs_garment[i:i+sub_batch_size] for i in range(0, inputs_num, sub_batch_size)]

            transforms = GeneralRCNNTransform(min_size=garment_size, max_size=garment_size)
            outputs_garments = []
            garment_start = time.time()
            for single_batch in inputs_garment_batch:
                single_batch, _ = transforms(single_batch)
                self._set_device(single_batch)
                outputs_batch = self._model_garment(single_batch)
                for output in outputs_batch:
                    for item in output:
                        output[item] = output[item].detach().cpu().numpy()
                outputs_garments.extend(outputs_batch)
            garment_detection_time += time.time() - garment_start

            human_boxes_new = [box for box in human_boxes if len(box)>0]
            human_boxes_new = np.row_stack(human_boxes_new)
            batch_boxes = [[] for i in range(len(images))]
            batch_scores = [[] for i in range(len(images))]
            batch_labels = [[] for i in range(len(images))]
            for j, (output,human_box) in enumerate(zip(outputs_garments, human_boxes_new)):
                #print('boxes size: ', output['boxes'].size())
                #print('scores size: ', output['scores'].size())
                #print('labels size: ', output['labels'].size())
                boxes = output['boxes']
                scores = output['scores']
                labels = output['labels'] - 1
                #boxes = torch.cat((output['boxes'], output['scores'].unsqueeze(1),output['labels'].type(torch.cuda.FloatTensor).unsqueeze(1)), dim=-1).detach().cpu().numpy()
                ind = batch_id[j]
                # recover the absolute box coordinates
                if len(boxes) == 0:
                    continue
                boxes = get_absolute_box(human_box, boxes)
                batch_boxes[ind].extend(boxes)
                batch_scores[ind].extend(scores)
                batch_labels[ind].extend(labels)
            
            draw_start = time.time()
            batch_size = len(images)
            ind_start = batch_size*idx + 1
            
            #self.draw_and_save(images, batch_boxes, path, targets)
            draw_boxes(images, human_boxes, human_scores, human_pred_class, CLASS_NAME_HUMAN)
            draw_and_save(images, batch_boxes, batch_scores, batch_labels, path, ind_start, CLASS_NAMES)
            draw_image_time += time.time() - draw_start

        total_time = time.time() - start
        time_per_im = (total_time) / image_num
        time_human_per_im = human_detection_time / image_num
        time_crop_per_im = crop_image_time / image_num
        time_garment_per_im = garment_detection_time / image_num
        time_image_draw_per_im = draw_image_time / image_num
        print('Total detection time is {}'.format(total_time))
        print('Detection time per image is {}'.format(time_per_im))
        print('Detection time for human per image is {}'.format(time_human_per_im))
        print('Detection time for crop human per image is {}'.format(time_crop_per_im))
        print('Detection time for garments per image is {}'.format(time_garment_per_im))
        print('Saving time for per image is {}'.format(time_image_draw_per_im))
        print('Detecting speed is {} FPS'.format(1.0 / time_per_im))

    def _set_device( self, blobs ):
        if type(blobs) == list:
            for i in range(len(blobs)):
                blobs[i] = self._set_device(blobs[i])
        elif type(blobs) == dict:
            for key, data in blobs.items():
                if key in ['scale', 'pad', 'im_id', 'image_sizes', 'path', 'ori_image']:
                    continue
                blobs[key] = self._set_device(data)
        else:
            blobs = blobs.to(self._device)
        return blobs

    def _set_optimizer( self ):
        params = self._cfg.dnn.OPTIMIZER
        if params['type'] == 'GD':
            self._optimizer = optim.SGD( self._model.parameters(),
                                        lr=params['lr'],
                                        momentum=params.get('momentum',0.9),
                                        weight_decay=params.get('weight_decay',0))
        elif params['type'] == 'Adam':
            self._optimizer = optim.Adam(self._model.parameters(),
                                         lr = params['lr'],
                                         betas=params.get('betas',(0.9, 0.999)),
                                         eps = params.get('eps', 1e-8)
                                         )
        else:
            raise ValueError('Optimiser type wrong, {} is not a valid optimizer type!')

        self._scheduler = optim.lr_scheduler.StepLR( self._optimizer,
                                                    step_size=params['decay_step'],
                                                    gamma=params['decay_rate'] )
        self._niter = self._cfg.dnn.NITER

def get_input(train_feeder):
    data = train_feeder.next()
    for key in data.keys():
        for a_key in data[key].keys():
            print('Key: {}, shape: {}'.format(a_key, data[key][a_key].shape))
    #return data

if __name__=="__main__" :
    #image_size = 416
    human_size = 416
    garment_size = 416
    params = {}
    params['config'] = 'frcnn_modanet'
    params['batch_size'] = 4
    params['nclasses'] = 13
    params['gpu'] = '1'
    #params['tag'] = '20191107_frcnn'
    params['tag'] = '20191218_frcnn_modanet_800'
    params['path'] = {}
    

    criterias = ['pair_match_accuracy', 'category_accuracy']
    
    params['path']['project'] = os.path.expanduser('~/Vision/data')
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    pic_root = '/hdd/fashion_videos/out_frames'
    pic_root = '/ssd/data/datasets/modanet/Images/'
    list_path = '/ssd/data/datasets/fashion_videos/testim.txt'
    model_garment = '../pytorch_yolov3/config/yolov3-modanet.cfg'
    model_human_path = '../pytorch_yolov3/config/yolov3-person1.cfg'

    device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'test', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )

    #set the paths to save all the results (model, val result)
    cfg.build_path( params['tag'], 'deepfashion2', model_hash='resnet_50' )

    anno_path = '/ssd/data/annotations/coco2014_instances_person.pkl'
    anno_path = '/ssd/data/annotations/modanet2018_instances_val.pkl'
    #test_dataset = ListDataset(list_path) # the rcnn transform are integrad into collate fn
    valid_path = '../pytorch_yolov3/data/test/testim_max.txt'
    dataset = NewListDataset(valid_path, img_size=human_size, augment=False, multiscale=False)
    dataloader = torch.utils.data.DataLoader(
        dataset, batch_size=1, shuffle=False, num_workers=1, collate_fn=dataset.collate_fn
    )

    backbone_garment = torchvision.models.detection.backbone_utils.resnet_fpn_backbone('resnet101', pretrained=True)

    #model_human = FasterRCNN(backbone_human, num_classes=2, cfg=cfg.dnn)
    model_garment = FasterRCNN(backbone_garment, num_classes=14, cfg=cfg.dnn)
    model_human = Darknet(model_human_path)

    model_path_garment = os.path.expanduser('~/Vision/data/embedding/frcnn_modanet/checkpoints/checkpoints_20200110_frcnn_modanet_416_10.pkl')
    #model_path_garment = os.path.expanduser('~/Vision/data/embedding/frcnn_modanet/checkpoints/checkpoints_20191218_frcnn_modanet_800_10.pkl')
    #model_path_garment = os.path.expanduser('~/Vision/data/embedding/frcnn_modanet/checkpoints/checkpoints_2020101_frcnn_modanet_human_mirror_416_10.pkl')
    #model_path_garment = os.path.expanduser('~/Vision/data/embedding/frcnn_modanet/checkpoints/checkpoints_20191220_frcnn_modanet_human_mirror_800_10.pkl')
    model_path_human = os.path.expanduser('~/Vision/git/pytorch_yolov3/checkpoints/yolov3_human_ckpt_416_30.pth')
    #model_path_human = os.path.expanduser('~/Vision/git/pytorch_yolov3/checkpoints/yolov3_human_ckpt_800_29_final.pth')
    state_dict_garment = torch.load(model_path_garment, map_location=device)['model_state_dict']
    state_dict_human = torch.load(model_path_human, map_location=device)
    model_human.load_state_dict(state_dict_human, strict=True)
    model_garment.load_state_dict(state_dict_garment, strict=True)
    model_human.to(device)
    model_garment.to(device)
    t = my_trainer( cfg, None, device, trainset=None, testset=dataloader )
    t._model_human = model_human
    t._model_garment = model_garment

    start = time.time()
    print('start testing')
    #path = '/hdd/fashion_videos/frcnn_frames'
    #path = '/hdd/data/datasets/modanet/human_result'
    path = 'out_path/human_yolo_{}_modanet_frcnn_{}_max'.format(human_size, garment_size)
    if not os.path.isdir(path):
        os.mkdir(path)
    t.test_two_stage_cross(path, human_size, garment_size)
    print('the images are saved in {}'.format(path))
    #t.test_one(path=path)
    #human_path = 'human_result.pkl'
    #t.save_human_result(human_path)
    #print('Used {:.2f} seconds to test'.format(time.time()-start))
    #with open('result.pkl', 'wb') as f:
    #    pickle.dump(out, f)

