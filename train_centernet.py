import _init
import sys
import os
import argparse
import pickle
import platform
import progressbar
import numpy as np
import datetime
from tools import Logger
import torchvision
import math
from torch import nn
import json
import tqdm
import time
#from apex import amp

from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval
import datetime
from torchcore.data.transforms import RandomMirror

from PIL.ImageDraw import Draw

from rcnn_config import config
from tools import torch_tools
#from data import data_feeder

from torchcore.data.datasets import ModanetDataset, COCODataset, ModanetHumanDataset, COCOPersonDataset, COCOCenterDataset
#from rcnn_dnn.networks import networks
from rcnn_dnn.data.collate_fn import collate_fn, CollateFnRCNN

import torch
#torch.multiprocessing.set_sharing_strategy('file_system')
import torchvision.transforms as transforms
import torch.optim as optim

from torchcore.dnn import trainer
from benchmark import Benchmark
from torchcore.dnn.networks.faster_rcnn import FasterRCNN
from torchcore.dnn import networks
from torchcore.data.transforms import Compose, RandomCrop, RandomScale, RandomMirror, ToTensor, Normalize, PadNumpyArray
from torchcore.data.datasets import COCOPersonCenterDataset
from torchcore.dnn.networks.center_net import CenterNet
from torchcore.dnn.networks.tools.data_parallel import DataParallel

def parse_commandline():
    parser = argparse.ArgumentParser(description="Training the Model")
    #parser.add_argument('-c','--config',help='Configuration Name', required=True)
    #parser.add_argument('--patchsize',help='Patch Size', required=True)
    #parser.add_argument('-b','--batch_size',help='Batch size', required=True)
    #parser.add_argument('-t','--tag',help='Model tag', required=False)
    parser.add_argument('-b','--batch_size',help='Batch size per step per gpu', required=True, type=int)
    parser.add_argument('-a','--accumulation_step',help='Accumulate size', required=False, default=1, type=int)
    parser.add_argument('-t','--tag',help='Model tag', required=False)
    parser.add_argument('--resume',help='resume the model', action='store_true', required=False)
    parser.add_argument('--dataset', help='The dataset we are going to use', default='coco')
    #parser.add_argument('-g','--gpu',help='GPU Index', default='0')
    #parser.add_argument('--datasetpath',help='Path to the dataset',required=True)
    #parser.add_argument('--projectpath',help='Path to the project',required=True)
    return parser.parse_args()

def get_absolute_box(human_box, box):
    #box[2]+=int(human_box[0])+box[0]
    #box[3]+=int(human_box[1])+box[1]
    box[0]+=int(human_box[0])
    box[1]+=int(human_box[1])
    return box

class my_trainer(trainer):
    def __init__( self, cfg, model, device, trainset, testset=None, dataset_name='modanet', benchmark=None, criterias=None, val_dataset=None, tag=None ):
        self._cfg = cfg
        self._device = device
        self._optimizer = None
        self._model = model
        self._tag = tag

        self._trainset = trainset
        self._testset = testset
        self._benchmark = benchmark
        self._criterias = criterias
        self._dataset_name = dataset_name
        self._epoch = 0
        self._val_dataset = val_dataset
        self._niter = cfg.optimizer.n_iter
        #self._trainset_feeder = data_feeder( trainset )

        #if testset is not None :
        #    self._testset_feeder = data_feeder( testset )

        self._set_optimizer()
        self._set_scheduler()
        if cfg.resume:
            path = os.path.join(os.path.dirname(cfg.path.CHECKPOINT), 'last.pth') 
            self.resume_training(path, device)

        model.to(device)
        #model, self._optimizer = amp.initialize(model, self._optimizer, opt_level="O1")
        #model = DistributedDataParallel(model)
        model = DataParallel(model,
            device_ids=None, 
            chunk_sizes=None)
        self._model = model

        self.init_logger()

    def train( self, resume=False ):
        #if self._testset is not None :
        #    self._validate()

        for i in range( self._epoch+1, self._niter+1 ):
            print("Epoch %d/%d" % (i,self._niter))
            self._logger.info('epoch {}\n'.format(i))
            self._train()

            if self._testset is not None and i%1 == 0 :
                self._validate()
            self._epoch = i
            self.save_training(self._cfg.path.CHECKPOINT.format(self._epoch))
            self._scheduler.step()

    def train_modanet_human( self ):
        #if self._testset is not None :
        #    self._validate_modanet_human()

        for i in range( self._epoch+1, self._niter+1 ):
            print("Epoch %d/%d" % (i,self._niter))
            self._logger.info('epoch {}\n'.format(i))
            self._train()

            if self._testset is not None :
                self._validate_modanet_human()
            self._epoch = i
            self.save_training(self._cfg.path.CHECKPOINT.format(self._epoch))
            self._scheduler.step()

    def train_one_epoch(self):
        self._train()

    def validate_onece(self):
        self._validate()

    def _validate( self ):
        print('start to validate')
        self._model.eval()

        results = []
        with torch.no_grad() :
            for idx,(inputs, targets) in enumerate(tqdm.tqdm(self._testset, 'evaluating')):
                inputs = self._set_device( inputs )
                output = self._model( inputs)
                batch_size = len(output['boxes'])
                #for i, im in enumerate(output):
                for i in range(batch_size):
                    if len(output['boxes'][i]) == 0:
                        continue
                    # convert to xywh
                    output['boxes'][i][:,2] -= output['boxes'][i][:,0]
                    output['boxes'][i][:,3] -= output['boxes'][i][:,1]
                    for j in range(len(output['boxes'][i])):
                        results.append({'image_id':int(targets['image_id'][i]), 
                                        'category_id':output['category'][i][j].cpu().numpy().tolist()+1, 
                                        'bbox':output['boxes'][i][j].cpu().numpy().tolist(), 
                                        'score':output['scores'][i][j].cpu().numpy().tolist()})
                #output = self._model['net']( inputs, just_embedding=True) # debug
                #bench.update( targets, output )
        with open('{}temp_result.json'.format(self._tag),'w') as f:
            json.dump(results,f)
        self.eval_result(dataset=self._dataset_name)
    
    def _validate_modanet_human( self ):
        print('start to validate')
        self._model.eval()
        if isinstance(self._model, nn.DataParallel):
            test_model = self._model.module
        else:
            test_model = self._model

        results = []
        id_human_map = self._val_dataset.get_human_boxes()
        with torch.no_grad() :
            for idx,(inputs, targets) in enumerate(self._testset):
                batch_boxes = []
                images = []
                im_ids = []
                inputs = self._set_device( inputs )
                output = test_model( inputs)
                for i, im in enumerate(output):
                    if len(im['boxes']) == 0:
                        continue
                    # convert to xywh
                    im['boxes'][:,2] -= im['boxes'][:,0]
                    im['boxes'][:,3] -= im['boxes'][:,1]
                    im_id = targets[i]['image_id']
                    boxes = im['boxes'].cpu().numpy().astype(int)
                    boxes = boxes.tolist()
                    pre_labels = im['labels'].cpu().numpy().tolist()
                    scores = im['scores'].cpu().numpy().tolist()

                    #im_path = targets[i]['image_path']
                    #images.append(Image.open(im_path).convert('RGB'))
                    #im_ids.append(im_id)
                    #singbatch = []
                    for j in range(len(im['boxes'])):
                        bbox = get_absolute_box(id_human_map[im_id], boxes[j] )
                        #singbatch.append(bbox)
                        results.append({'image_id':int(im_id), 
                                        'category_id': pre_labels[j], 
                                        'bbox': bbox,
                                        'score':scores[j]})
                        if pre_labels[j]<= 0 or pre_labels[j]>13:
                            print('wrong label')
                
                    #batch_boxes.append(np.array(singbatch).astype(int))
                #draw_and_save(images, batch_boxes, 'test_im', im_ids)
            
                if idx % 1000==0:
                    print('{}, {} / {}'.format(datetime.datetime.now(), idx, len(self._testset)))

        with open('temp_result.json','w') as f:
            json.dump(results,f)
        self.eval_result(dataset=self._dataset_name)

    def eval_result(self, dataset='coco_person'):
        if dataset not in ['coco','coco_person', 'modanet']:
            raise ValueError('only support coco, coco_person and modanet dataset')
        if dataset == 'coco_person':
            gt_json=os.path.expanduser('~/data/datasets/COCO/annotations/instances_val2014.json')
        elif dataset == 'coco':
            gt_json=os.path.expanduser('~/data/datasets/COCO/annotations/instances_val2017.json')
        else:
            gt_json=os.path.expanduser('~/data/datasets/modanet/Annots/modanet_instances_val.json')

        dt_json='{}temp_result.json'.format(self._tag)

        # we need to map the category ids back
        if dataset == 'coco':
            print('revise coco dataset label to gt')
            cat_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 27, 28, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 67, 70, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 84, 85, 86, 87, 88, 89, 90]
            with open(dt_json) as f:
                results = json.load(f)
                for result in results:
                    temp_id = result['category_id']
                    result['category_id'] = cat_ids[temp_id-1]
            with open(dt_json,'w') as f:
                json.dump(results,f)
        annType = 'bbox'
        cocoGt=COCO(gt_json)
        cocoDt=cocoGt.loadRes(dt_json)

        imgIds=sorted(cocoGt.getImgIds())

        # running evaluation
        cocoEval = COCOeval(cocoGt,cocoDt,annType)
        if dataset == 'coco_person':
            cocoEval.params.catIds = [1]
        cocoEval.params.imgIds = imgIds
        cocoEval.evaluate()
        cocoEval.accumulate()
        cocoEval.summarize()

    def _train( self ):
        self._model.train()

        loss_values = []
        average_losses = {}

        for idx, (inputs, targets) in enumerate(tqdm.tqdm(self._trainset, desc='Training')):
            #print('inputs:', inputs)
            #print('targets:', targets)

            inputs = self._set_device( inputs )
            targets = self._set_device( targets )
            #print('input device:', inputs[0].device)

            loss_dict = self._model(inputs, targets)

            # add the losses for each part
            #loss_sum = sum(loss for loss in loss_dict.values())
            loss_sum=0
            for single_loss in loss_dict:
                loss_sum = loss_sum + loss_dict[single_loss]
                if single_loss in average_losses:
                    average_losses[single_loss]+= loss_dict[single_loss].mean().item()
                else:
                    average_losses[single_loss] = loss_dict[single_loss].mean().item()

            loss_sum = loss_sum.mean()
            if not math.isfinite(loss_sum):
                #print("Loss is {}, stopping training".format(loss_sum))
                print("Loss is {}, skip this batch".format(loss_sum))
                print(loss_dict)
                continue
                #sys.exit(1)

            # Computing gradient and do SGD step
            self._optimizer.zero_grad()
            #with amp.scale_loss(loss_sum, optimiser) as scaled_loss:
            #    scaled_loss.backward()
            loss_sum.backward()
            self._optimizer.step()
            loss_values.append( loss_sum.cpu().detach().numpy() )

            if idx%100 == 0:
                self._logger.info('{} '.format(idx+1))
                loss_str = ''
                for loss in average_losses:
                    if idx==0:
                        loss_num = average_losses[loss]
                    else:
                        loss_num = average_losses[loss] / 100
                    self._logger.info('{} '.format(loss_num))
                    loss_str += (' {} loss:{}, '.format(loss, loss_num))
                print(loss_str[:-2])
                average_losses = {}
                self._logger.info('\n')
            

            #bar.update(idx+1,loss=loss_sum.item())
        #bar.finish()

        print('Average loss : ', np.mean(loss_values))

    def _set_device( self, blobs ):
        if type(blobs) == list:
            for i in range(len(blobs)):
                blobs[i] = self._set_device(blobs[i])
        elif type(blobs) == dict:
            for key, data in blobs.items():
                blobs[key] = self._set_device(data)
        elif torch.is_tensor(blobs):
            blobs = blobs.to(self._device)
        return blobs

    def _set_scheduler(self):
        scheduler = self._cfg.scheduler
        if scheduler.type == 'multi_step':
            self._scheduler = optim.lr_scheduler.MultiStepLR( self._optimizer,
                                                            milestones=scheduler['milestones'],
                                                            gamma=scheduler.get('gamma', 0.1))
        else:
            raise ValueError('unknow scheduler {}'.format(scheduler.type))

    #def _set_optimizer( self ):
    #    params = self._cfg.dnn.OPTIMIZER
    #    if params['type'] == 'GD':
    #        self._optimizer = optim.SGD( self._model.parameters(),
    #                                    lr=params['lr'],
    #                                    momentum=params.get('momentum',0.9),
    #                                    weight_decay=params.get('weight_decay',0))
    #    elif params['type'] == 'Adam':
    #        self._optimizer = optim.Adam(self._model.parameters(),
    #                                     lr = params['lr'],
    #                                     betas=params.get('betas',(0.9, 0.999)),
    #                                     eps = params.get('eps', 1e-8)
    #                                     )
    #    else:
    #        raise ValueError('Optimiser type wrong, {} is not a valid optimizer type!')

    #    self._scheduler = optim.lr_scheduler.StepLR( self._optimizer,
    #                                                step_size=params['decay_steps'],
    #                                                gamma=params['decay_rate'] )
    #    self._niter = self._cfg.dnn.NITER

    def init_logger(self):
        train_path = self._cfg.path['LOG']

        console_formatter = '{} {{}}'.format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        self._logger = Logger(level='info', file=train_path, console=False, console_formatter=console_formatter)

        print('Loss log path is: {}'.format(train_path))

    def save_training(self, path, to_print=True):
        if isinstance(model, torch.nn.DataParallel):
            state_dict = self._model.module.state_dict()
        else:
            state_dict =self._model.state_dict()
        torch.save({
            'epoch': self._epoch,
            'model_state_dict': state_dict,
            'optimizer_state_dict': self._optimizer.state_dict(),
            'scheduler':self._scheduler.state_dict(),
            'niter':self._niter
        }, path)
        folder = os.path.dirname(path)
        torch.save({
            'epoch': self._epoch,
            'model_state_dict': state_dict,
            'optimizer_state_dict': self._optimizer.state_dict(),
            'scheduler':self._scheduler.state_dict(),
            'niter':self._niter
        }, os.path.join(folder, 'last.pth'))
        if to_print:
            print('The checkpoint has been saved to {}'.format(path))

    def resume_training(self, path, device, to_print=True):
        checkpoint = torch.load(path)
        self._epoch = checkpoint['epoch']
        self._model.load_state_dict(checkpoint['model_state_dict'])
        self._optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        self._niter = self._niter
        #if 'scheduler' in checkpoint:
        self._scheduler.load_state_dict(checkpoint['scheduler'])
        if to_print:
            print('Chekpoint has been loaded from {}'.format(path))

def load_checkpoint(model, path, device, to_print=True):
    #checkpoint = torch.load(path)
    state_dict_ = torch.load(path, map_location=device)['model_state_dict']
    state_dict = {}
    for k in state_dict_:
        if k.startswith('module') and not k.startswith('module_list'):
            state_dict[k[7:]] = state_dict_[k]
        else:
            state_dict[k] = state_dict_[k]
    model.load_state_dict(state_dict, strict=True )
    #self._epoch = checkpoint['epoch']
    #self._model.load_state_dict(checkpoint['model_state_dict'])
    #self._optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    if to_print:
        print('Chekpoint has been loaded from {}'.format(path))

def get_data_loader():
    anno_path = os.path.expanduser('~/Vision/data/annotations/coco2014_instances_person_debug.pkl')
    root = os.path.expanduser('~/Vision/data/datasets/COCO')
    transform_list = []
    random_crop = RandomCrop((512,448)) #(width, height)
    #random_crop = RandomCrop(512)
    random_scale = RandomScale(0.6, 1.4)
    random_mirror = RandomMirror()
    to_tensor= ToTensor()
    normalize = Normalize()
    transform_list.append(random_scale)
    transform_list.append(random_crop)
    transform_list.append(random_mirror)
    #transform_list.append(to_tensor)
    #transform_list.append(normalize)
    transforms = Compose(transform_list)
    dataset = COCOPersonCenterDataset(root=root, anno=anno_path, part='val2014',transforms=transforms)

    data_loader = torch.utils.data.DataLoader(
      dataset, 
      batch_size=2, 
      shuffle=True,
      num_workers=2,
      pin_memory=True,
      drop_last=True
    )
    return data_loader

if __name__=="__main__" :
    args = parse_commandline()
    #image_size = 416
    params = {}
    params['config'] = 'center_net_coco'
    params['gpu'] = '0'
    params['tag'] = args.tag
    params['path'] = {}

    params['path']['project'] = os.path.expanduser('~/Vision/data')
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    #device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )
    cfg.resume = args.resume

    #set the paths to save all the results (model, val result)
    cfg.build_path( params['tag'], args.dataset, model_hash='centernet' )

    #benchmark = Benchmark(cfg, criterias=criterias)
    #benchmark = None
    transform_list = []
    random_crop = RandomCrop((512,512)) #(width, height)
    #random_crop = RandomCrop(512)
    random_scale = RandomScale(0.6, 1.4)
    random_mirror = RandomMirror()
    to_tensor= ToTensor()
    normalize = Normalize()
    pad_array = PadNumpyArray(target_len_dict={'boxes':128, 'cat_labels':128}, target_val_dict={'boxes':0, 'cat_labels':-1})
    transform_list.append(random_scale)
    transform_list.append(random_crop)
    transform_list.append(random_mirror)
    transform_list.append(pad_array)
    transform_list.append(to_tensor)
    transform_list.append(normalize)
    transforms = Compose(transform_list)
    xyxy = True

    test_transform = []
    test_transform.append(ToTensor())
    test_transform.append(Normalize())
    test_transforms = Compose(test_transform)

    if args.dataset == 'coco_person':
        root = os.path.expanduser('~/data/datasets/COCO')
        anno = os.path.expanduser('~/data/annotations/coco2014_instances_person_all.pkl')
        dataset = COCOPersonDataset(root, anno, part='train2014', transforms=transforms)
        test_dataset = COCOPersonDataset(root, anno, part='val2014', transforms=test_transforms)
        cfg.class_num = 1
    elif args.dataset == 'modanet':
        anno_path = os.path.expanduser('~/data/annotations/modanet2018_instances_revised.pkl')
        root = os.path.expanduser('~/data/datasets/modanet/Images')
        dataset = ModanetDataset(anno=anno_path, part='train', root=root, transforms=transforms) 
        test_dataset = ModanetDataset(anno=anno_path, part='val', root=root, transforms=test_transforms) # the default transform in faster rcnn will be used
        cfg.class_num = 13
    elif args.dataset == 'coco':
        anno_path = os.path.expanduser('~/data/annotations/coco2017_instances.pkl')
        root = os.path.expanduser('~/data/datasets/COCO')
        dataset = COCODataset(anno=anno_path, part='train2017', root=root, transforms=transforms, xyxy=xyxy) # the default transform in faster rcnn will be used
        test_dataset = COCODataset(anno=anno_path, part='val2017', root=root, transforms=test_transforms, xyxy=xyxy) # the default transform in faster rcnn will be used
        cfg.class_num = 80

    #anno_path = os.path.expanduser('./data/annotations/coco2014_instances_person.pkl')
    #root = os.path.expanduser('./data/datasets/COCO')
    #dataset = COCOPersonCenterDataset(root=root, anno=anno_path, part='train2014',transforms=transforms)
    #test_dataset = COCOPersonCenterDataset(root=root, anno=anno_path, part='val2014',transforms=None, training=False)

    data_loader = torch.utils.data.DataLoader(
      dataset, 
      batch_size=args.batch_size, 
      shuffle=True,
      num_workers=4,
      pin_memory=True,
      drop_last=True
    )

    test_data_loader = torch.utils.data.DataLoader(
      test_dataset, 
      batch_size=1, 
      shuffle=False,
      num_workers=4,
      pin_memory=True,
      drop_last=False
    )

    #data_loader_test = torch.utils.data.DataLoader(
    #    val_dataset, batch_size=1, shuffle=False, num_workers=4,
    #    collate_fn=collate_fn_rcnn)

    use_cuda = torch.cuda.is_available()
    device_name = "cuda" if use_cuda else "cpu"
    device = torch.device( device_name )

    backbone = networks.feature.resnet50()
    in_channel = backbone.out_channel
    neck = networks.neck['upsample_basic'](in_channel)
    #net = resnet50()
    backbone.multi_feature = False
    #backbone = torchvision.models.detection.backbone_utils.resnet_fpn_backbone('resnet50', pretrained=True)
    #backbone.out_channel = 256
    #neck = None
    parts = ['heatmap', 'offset', 'width_height']
    loss_weight = {'heatmap':1., 'offset':1., 'width_height':0.1}
    #parts = ['heatmap']
    model = CenterNet(backbone, num_classes=cfg.class_num, neck=neck, parts=parts, loss_weight=loss_weight)

    #load_checkpoint(model, cfg.path.CHECKPOINT.format(150), device)

    t = my_trainer( cfg, model, device, data_loader, testset=test_data_loader, dataset_name=args.dataset, benchmark=None, val_dataset=None, tag=args.tag )
    #loss = t.trainstep()
    #print('loss is {}'.format(loss))
    #t.train_one_epoch()
    #t.validate_onece()
    #t.load_training(cfg.path.CHECKPOINT.format(150), device)
    #t.validate_onece()
    t.train()
    #t.train_modanet_human()

    #train_feeder.exit()
    #test_feeder.exit()

    #torch.save({'state_dict':model.state_dict()}, cfg.path.MODEL)

    #print('Model is saved to :', cfg.path.MODEL)
