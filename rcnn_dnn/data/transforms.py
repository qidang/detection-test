from torchcore.data.transform_func import mirror, mirror_boxes
import random
class RandomMirror(object):
    def __init__(self):
        pass

    def __call__(self, image, targets):
        if random.random() < 0.5:
            image = mirror(image)
            
            if 'boxes' in targets:
                im_width = image.width
                targets['boxes'] = mirror_boxes(targets['boxes'], im_width)
        
        return image, targets