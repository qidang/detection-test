import numpy as np
import math
from tools import pil_tools
from PIL.ImageDraw import Draw
from dnn.blobs.blobs import data_blobs
from PIL import Image
import random

import torch
import torchvision.transforms as transforms
from torchvision.models.detection.image_list import ImageList

class patch_roi_pair_blobs( data_blobs ):
    def __init__( self, dnn_cfg ):
        super().__init__( dnn_cfg )
        self._toTensor = transforms.ToTensor()
        self._normalizeTensor = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                                     std=[0.229, 0.224, 0.225])

    def _add_data_box_cat( self, images, boxes_inds, blobs, training, random_mirror ):
        data = []
        the_images = [] # debug
        ori_images = [] # debug
        boxes = []
        scales = np.zeros(len(images), dtype=float)
        crops = np.zeros((len(images),4), dtype=int)
        cat_labels = np.zeros(len(images), dtype=int)

        max_size = self._dnn_cfg.PATCH_SIZE
        expand_ratio = 1.5 # the ratio cropped patch expand to each side
        if random_mirror:
            mirror_ind = np.random.choice([True, False], size=len(images))
            print(mirror_ind)

        # find scale for each image and padding for each image
        for i, image in enumerate(images) :
            #resize the image using scale in im_PIL
            if random_mirror:
                image._mirrored= mirror_ind[i]
            box = image.gtboxes[boxes_inds[i]]
            width = box[2] - box[0]
            height = box[3] - box[1]
            side_len = max(width, height) * expand_ratio
            
            x1 = max(0, box[0]-int((side_len-width)/2))
            y1 = max(0, box[1]-int((side_len-height)/2))
            x2 = min(image.width, box[2]+int((side_len-width)/2))
            y2 = min(image.height, box[3]+int((side_len-height)/2))
            crops[i] = [x1, y1, x2, y2]
            scale = max_size / max(x2-x1, y2-y1)
            scales[i]=scale

            im = image.im_PIL
            #ori_images.append(im) #debug
            #crop
            im = im.crop(crops[i])
            #resize
            w = int((x2-x1)*scale)
            h = int((y2-y1)*scale)
            im = im.resize( [w,h], Image.BILINEAR )
            #padding
            im_new = Image.new('RGB', (max_size, max_size))
            im_new.paste(im)

            pt_im = self._toTensor( im_new )
            pt_im = self._normalizeTensor( pt_im )
            data.append(pt_im)

            #the_images.append(im_new) #debug

            # for crop
            box[0] = box[0] - x1
            box[1] = box[1] - y1
            box[2] = box[2] - x1
            box[3] = box[3] - y1
            #for resize
            box = box * scale
            box = box.reshape((1,4))
            boxes.append(torch.from_numpy(box))

            cat_labels[i] = image.gtlabels.reshape((-1))[boxes_inds[i]]
        self._scales = scales
        self._crops = crops

        blobs['data'] = torch.stack(data)
        blobs['boxes'] = boxes
        blobs['cat_labels'] = torch.from_numpy(cat_labels) - 1
        #blobs['images'] = the_images #debug
        #blobs['ori_images'] = ori_images #debug

    def _add_pair_labels(self, pair_labels, blobs):
        pair_labels = pair_labels.reshape((-1,2))[:,0]
        blobs['pair_labels'] = torch.from_numpy(pair_labels)

    def _add_pair_indexes(self, images, blobs):
        inds = np.arange(len(images)).reshape((-1,2))
        blobs['pair_indexes'] = torch.from_numpy(inds)



    def get_blobs( self, images, boxes_inds, pair_labels, training, random_mirror=False ):
        blobs = {}

        # Converting the data to the propper format
        self._add_data_box_cat( images, boxes_inds, blobs, training, random_mirror=random_mirror )
        self._add_pair_labels(pair_labels, blobs)
        self._add_pair_indexes(images, blobs)

        inputs = {}
        inputs['data'] = blobs['data']
        inputs['boxes'] = blobs['boxes']
        inputs['pair_indexes'] = blobs['pair_indexes']
        #inputs['images'] = blobs['images'] #debug
        #inputs['ori_images'] = blobs['ori_images'] #debug

        targets = {}
        targets['category'] = blobs['cat_labels']
        targets['pair_labels'] = blobs['pair_labels']

        return inputs, targets
