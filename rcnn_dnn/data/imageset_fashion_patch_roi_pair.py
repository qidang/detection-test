import numpy as np
import h5py
import torch
import random
import time
import datetime
from data import imageset


class imageset_fashion_patch_roi_pair(imageset) :
    def __init__( self, cfg, dataset, blob_gen, randomize=True, is_training=False, benchmark=None, sample_num_pos = 10000, random_mirror=False ):
        self._cfg = cfg
        self._dataset = dataset
        self._blob_gen = blob_gen
        self._images = dataset.images
        self._batchsize = cfg.dnn.NETWORK.BATCH_SIZE
        self._file = None
        self._randomize = randomize
        self._is_training = is_training
        self._benchmark = benchmark
        self._sample_num_pos = sample_num_pos
        self._random_mirror = random_mirror

        self._last_id = self.set_unique_id()
        self.get_uid_dict()
        self._prepare()

    def set_unique_id(self):
        images = self._images
        # set unique id for each image
        pair_id_dict = {}
        unique_id = 1
        for image in images:
            image.unique_ids = []
            if image.pair_id not in pair_id_dict:
                pair_id_dict[image.pair_id] = {}
            for style in image.styles:
                if style == 0:
                    image.unique_ids.append(0)
                    continue
                else:
                    if style in pair_id_dict[image.pair_id]:
                        image.unique_ids.append(pair_id_dict[image.pair_id][style])
                    else:
                        pair_id_dict[image.pair_id][style] = unique_id
                        image.unique_ids.append(unique_id)
                        unique_id += 1
        last_id = unique_id - 1
        #self.check_unique_id(images, last_id)
        return last_id
    
    def get_uid_dict(self):
        uid_dict = {'shop':{}, 'user':{}}
        for uid in range(self._last_id+1):
            uid_dict['shop'][uid] = []
            uid_dict['user'][uid] = []
        for i, image in enumerate(self._images):
            for j, uid in enumerate(image.unique_ids):
                uid_dict[image.source][uid].append([i, j]) # the index to locate the position of the box
        self._uid_dict = uid_dict

    def _make_pos_pairs(self):
        total_num = self._sample_num_pos
        self._pos_pairs = []
        cur_num = 0
        uid_dict = self._uid_dict
        while cur_num < total_num:
            ind = random.choice(range(self._last_id+1))
            if len(uid_dict['shop'][ind]) == 0 or len(uid_dict['user'][ind])== 0:
                continue
            shop_item = random.choice(uid_dict['shop'][ind])
            user_item = random.choice(uid_dict['user'][ind])
            self._pos_pairs.append([shop_item, user_item])
            cur_num+=1
    
    def _make_neg_pairs(self, times=1):
        total_num = times*len(self._pos_pairs)
        self._neg_pairs = []
        cur_num = 0
        uid_dict = self._uid_dict
        while cur_num < total_num:
            shop_ind, user_ind = random.sample(range(self._last_id),2)
            if len(uid_dict['shop'][shop_ind]) == 0 or len(uid_dict['user'][user_ind])== 0:
                continue
            shop_item = random.choice(uid_dict['shop'][shop_ind])
            user_item = random.choice(uid_dict['user'][user_ind])
            self._neg_pairs.append([shop_item, user_item])
            cur_num+=1

                
    def _prepare(self):
        self._make_pos_pairs()
        
        self._make_neg_pairs(times=1)

        pos_label = np.ones((len(self._pos_pairs),2,1), dtype=int)
        neg_label = np.zeros((len(self._neg_pairs),2,1), dtype=int)

        pos_with_label = np.concatenate((self._pos_pairs, pos_label), axis=-1)
        neg_with_label = np.concatenate((self._neg_pairs, neg_label), axis=-1)

        self._pairs = np.concatenate((pos_with_label, neg_with_label), axis=0) # sample_num * 2 * 2

        if self._randomize:
            np.random.shuffle(self._pairs)

        im_inds = self._pairs[:,:,0].flatten()
        self._box_inds = self._pairs[:,:,1].flatten()
        self._pair_labels = self._pairs[:,:,2].flatten()

        ndata = len(im_inds)
        inds_label = np.arange(ndata)
        batchsize = self._batchsize
        
        
        self._chunks = [im_inds[i:i+batchsize] for i in np.arange(0, ndata, batchsize)]
        self._box_chunks = [inds_label[i:i+batchsize] for i in np.arange(0, ndata, batchsize)]
        self._label_chunks = [inds_label[i:i+batchsize] for i in np.arange(0, ndata, batchsize)]
        self._cur = 0

    def __len__( self ):
        return len(self._chunks)

    def __getitem__( self, idx ):
        images = self._images[self._chunks[idx]]
        box_inds = self._box_inds[self._box_chunks[idx]]
        pair_labels = self._pair_labels[self._label_chunks[idx]]
        blobs = self._blob_gen.get_blobs(images, box_inds, pair_labels, training=self._is_training, random_mirror=self._random_mirror)

        return blobs

    def next( self ):
        if self._cur >= len(self._chunks):
            self._cur = 0
            # regenerate the input pairs
            self._prepare()
        blobs = self[self._cur]
        self._cur = self._cur+1
        return blobs
