import numpy as np
from tools import pil_tools
from PIL.ImageDraw import Draw
from PIL import Image
from dnn.blobs.blobs import data_blobs

import torch
import torchvision.transforms as transforms
import random

class mnist_triplet_blobs( data_blobs ):
    def __init__( self, dnn_cfg ):
        super().__init__( dnn_cfg )
        self._toTensor = transforms.ToTensor()
        self._normalizeTensor = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                                     std=[0.229, 0.224, 0.225])

    def _add_triplet_data(self, images, cat_labels, blobs):
        data_a = []
        data_pos = []
        data_neg = []

        label_a = []
        label_pos = []
        label_neg = []
        #for image in images:
        #    print('pair id is: ',image.pair_id)

        tri_num = int(len(images) / 3)
        for i in range(tri_num):
            image_a = images[3*i]
            image_pos = images[3*i+1]
            image_neg = images[3*i+2]

            image_a = image_a.astype(np.float32)/255.0
            image_a = torch.from_numpy(image_a)

            image_pos = image_pos.astype(np.float32)/255.0
            image_pos = torch.from_numpy(image_pos)

            image_neg = image_neg.astype(np.float32)/255.0
            image_neg = torch.from_numpy(image_neg)
            
            category_a = cat_labels[3*i]
            category_pos = cat_labels[3*i+1]
            category_neg = cat_labels[3*i+2]

            data_a.append(image_a)
            data_pos.append(image_pos)
            data_neg.append(image_neg)

            label_a.append(category_a)
            label_pos.append(category_pos)
            label_neg.append(category_neg)

        data_a.extend(data_pos)
        data_a.extend(data_neg)
        category = np.array(label_a+label_pos+label_neg, dtype=np.long).flatten()
        #blobs['data'] = data1
        #blobs['category'] = category
        blobs['data'] = torch.stack(data_a)
        blobs['category'] = torch.from_numpy(category)
                    
    def get_blobs( self, images, cat_labels):
        blobs = {}
        self._add_triplet_data(images, cat_labels, blobs)

        inputs = {}
        inputs['data'] = blobs['data']

        targets = {}
        targets['category'] = blobs['category']
        return inputs, targets
