import numpy as np
import h5py
import torch
import random
import time
import datetime
from data import imageset


class imageset_fashion_patch_roi(imageset) :
    def __init__( self, cfg, dataset, blob_gen, mode, randomize=True, benchmark=None):
        self._cfg = cfg
        self._dataset = dataset
        self._blob_gen = blob_gen
        self._images = dataset.images
        self._batchsize = cfg.dnn.NETWORK.BATCH_SIZE
        self._file = None
        self._randomize = randomize
        self._benchmark = benchmark
        self._mode = mode

        self._last_id = self.set_unique_id()
        self.get_inds()
        self.get_vailid_unique_id()
        self._prepare()

    def set_unique_id(self):
        images = self._images
        # set unique id for each image
        pair_id_dict = {}
        unique_id = 1
        for image in images:
            image.unique_ids = []
            if image.pair_id not in pair_id_dict:
                pair_id_dict[image.pair_id] = {}
            for style in image.styles:
                if style == 0:
                    image.unique_ids.append(0)
                    continue
                else:
                    if style in pair_id_dict[image.pair_id]:
                        image.unique_ids.append(pair_id_dict[image.pair_id][style])
                    else:
                        pair_id_dict[image.pair_id][style] = unique_id
                        image.unique_ids.append(unique_id)
                        unique_id += 1
        last_id = unique_id - 1
        #self.check_unique_id(images, last_id)
        return last_id

    def get_vailid_unique_id(self):
        unique_id_stat = np.zeros(self._last_id+1, dtype=int)
        for image in self._images:
            if image.source=='user':
                continue
            for unique_id in image.unique_ids:
                unique_id_stat[unique_id] +=1
        unique_id_stat[0] = 0 # the id 0 are non-valid ids
        self.valid_uids = np.where(unique_id_stat>=1)[0]
    
    def get_inds(self):
        im_ids = []
        box_inds = []
        for i, image in enumerate(self._images):
            if self._mode == 'all':
                im_ids.append(i*np.ones(len(image.unique_ids), dtype=int))
                box_inds.append(np.arange(len(image.unique_ids)))
            elif image.source == self._mode:
                im_ids.append(i*np.ones(len(image.unique_ids), dtype=int))
                box_inds.append(np.arange(len(image.unique_ids)))
        self._im_inds = np.concatenate(im_ids, axis=-1)
        self._box_inds = np.concatenate(box_inds, axis=-1)

    def _prepare(self):

        if self._randomize:
            inds = np.arange(len(self._im_inds))
            np.random.shuffle(inds)
            self._im_inds = self._im_inds[inds]
            self._box_inds = self._box_inds[inds]

        ndata = len(self._im_inds)
        inds_label = np.arange(ndata)
        batchsize = self._batchsize
        
        
        self._chunks = [self._im_inds[i:i+batchsize] for i in np.arange(0, ndata, batchsize)]
        self._box_chunks = [inds_label[i:i+batchsize] for i in np.arange(0, ndata, batchsize)]
        self._cur = 0

    def __len__( self ):
        return len(self._chunks)

    def __getitem__( self, idx ):
        images = self._images[self._chunks[idx]]
        box_inds = self._box_inds[self._box_chunks[idx]]
        blobs = self._blob_gen.get_blobs(images, box_inds)

        return blobs

    def next( self ):
        if self._cur >= len(self._chunks):
            self._cur = 0
            # regenerate the input pairs
            self._prepare()
        blobs = self[self._cur]
        self._cur = self._cur+1
        return blobs
