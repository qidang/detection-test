
import numpy as np
import math
from tools import pil_tools
from PIL.ImageDraw import Draw
from dnn.blobs.blobs import data_blobs
from PIL import Image

import torch
import torchvision.transforms as transforms
from torchvision.models.detection.image_list import ImageList

class cifar10_blobs( data_blobs ):
    def __init__( self, dnn_cfg ):
        super().__init__( dnn_cfg )
        self._toTensor = transforms.ToTensor()
        self._normalizeTensor = transforms.Normalize(mean=(0.5, 0.5, 0.5),
                                                     std=(0.5, 0.5, 0.5))
        self._randomCrop = transforms.RandomCrop(32, padding=4)
        self._randomHorizontalFlip = transforms.RandomHorizontalFlip()

    def get_blobs(self, images, labels, training=True):
        im_all = []
        for im in images:
            #print(im.shape)
            #print(im.dtype)
             
            im_new = Image.fromarray(im.reshape((3, 32, 32)).transpose([1,2,0]), mode='RGB')

            #im = torch.from_numpy(im)
            if training:
                im_new = self._randomCrop(im_new)
                im_new = self._randomHorizontalFlip(im_new)
            im = self._toTensor(im_new)
            im = self._normalizeTensor(im)
            im_all.append(im)
        ims = torch.stack(im_all)

        labels = np.array(labels).astype(int)
        labels = torch.from_numpy(labels)

        inputs = {}
        inputs['data']=ims

        targets = {}
        targets['category'] = labels
        return inputs, targets