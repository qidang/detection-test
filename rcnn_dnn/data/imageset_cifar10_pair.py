import os
import gzip
import numpy as np
from .cifar10_pair_blobs import cifar10_pair_blobs

import torchvision.transforms as transforms
from .imageset_mnist_pair import imageset_mnist_pair

class imageset_cifar10_pair(imageset_mnist_pair):
    def __init__(self, cfg, randomize=False, is_training=True, sample_num_pos=-1):
        self._randomize = randomize
        self._training = is_training
        self._sample_num_pos = sample_num_pos
        self._root_path = os.path.join(cfg.dataset['ROOT'], 'datasets/cifar10')
        self._batchsize = cfg.dnn.NETWORK.BATCH_SIZE

        self._blob_gen = cifar10_pair_blobs(cfg )
        if is_training:
            self.load_cifar10(kind='train')
        else:
            self.load_cifar10(kind='test')
        self._prepare()
    
    def __len__(self):
        return len(self._chunks)

    def __getitem__(self, idx):
        ims = self._images[self._chunks[idx]].reshape((-1,3,32,32))
        cat_labels = self._cat_labels[self._chunks[idx]]
        pair_labels = self._labels[self._label_chunks[idx]]
        inputs, targets = self._blob_gen.get_blobs(ims, pair_labels, cat_labels, self._training)

        out = {}
        out['inputs']={}
        out['inputs'] = inputs

        out['targets'] = {}
        out['targets'] = targets

        return out['inputs'], out['targets']

    def next(self):
        if self._cur >= len(self):
            self._cur=0
        blobs = self[self._cur]
        self._cur = self._cur + 1
        return blobs

    def load_cifar10(self, kind='train'):
        self._images= None
        self._cat_labels= None
        if kind=='train':
            for i in range(1,6):
                f_name = os.path.join(self._root_path, 'data_batch_{}'.format(i))
                the_dict = self.unpickle(f_name)
                ims = the_dict[b'data']
                labels = the_dict[b'labels']
                if i==1:
                    self._images = ims
                    self._cat_labels = labels
                else:
                    self._images = np.concatenate((self._images, ims), axis=0)
                    self._cat_labels.extend(labels)
        elif kind=='test':
            f_name = os.path.join(self._root_path, 'test_batch')
            the_dict = self.unpickle(f_name)
            ims = the_dict[b'data']
            labels = the_dict[b'labels']
            self._images = ims
            self._cat_labels = labels
        else:
            raise ValueError('cifar dataset kind error, {} is not a valid value'.format(kind))

        self._images = self._images.reshape(-1, 3, 32, 32)
        self._cat_labels = np.array(self._cat_labels, dtype=int)

    def unpickle(self, file):
        import pickle
        with open(file, 'rb') as fo:
            dict = pickle.load(fo, encoding='bytes')
        return dict
