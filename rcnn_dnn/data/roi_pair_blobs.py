import numpy as np
import math
from tools import pil_tools
from PIL.ImageDraw import Draw
from dnn.blobs.blobs import data_blobs
from PIL import Image
import random

import torch
import torchvision.transforms as transforms
from torchvision.models.detection.image_list import ImageList

class roi_pair_blobs( data_blobs ):
    def __init__( self, dnn_cfg ):
        super().__init__( dnn_cfg )
        self._toTensor = transforms.ToTensor()
        self._normalizeTensor = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                                     std=[0.229, 0.224, 0.225])

    def _find_scale(self, width, height, min_size, max_size):
            max_side = max(width, height)
            min_side = min(width, height)
            scale = float(min_size) / min_side
            if max_side * scale > max_size:
                scale = float(max_size) / max_side
            return scale

    def _cal_padding(self, images, size_devisable=32.0):
        max_size = tuple(max(s) for s in zip(*[image.shape for image in images]))
        stride = size_devisable

        max_size = list(max_size)
        max_size[0] = int(math.ceil(max_size[0] / stride) * stride)
        max_size[1] = int(math.ceil(max_size[1] / stride) * stride)

        for i, image in enumerate(images):
            if image.batch_ind != i:
                continue
            image.padding_y = int(max_size[0] - image.height)
            image.padding_x= int(max_size[1] - image.width)


    def _padding_batches(self, images):
        '''
        split the images to two groups as image pairs and 
        padding each group to same size
        '''
        images1 = []
        images2 = []
        for i, image in enumerate(images):
            if i%2 == 0:
                images1.append(image)
            else:
                images2.append(image)
        self._cal_padding(images1)
        self._cal_padding(images2)
        
    

    def _add_data( self, images, blobs, training ):
        data = []
        #the_images = [] # debug

        min_size = self._dnn_cfg.MIN_SIZE
        max_size = self._dnn_cfg.MAX_SIZE

        # This used to eliminate the repetitive images
        for image in images:
            image.batch_ind = -1
        # find scale for each image and padding for each image
        for i, image in enumerate(images) :
            if image.batch_ind == -1:
                image.batch_ind = i
            else:
                continue
            #resize the image using scale in im_PIL
            width = image.ori_width
            height = image.ori_height
            scale = self._find_scale(width, height, min_size, max_size) 
            image.scale = scale
            # important: reset padding here so next time calulate padding for different batches
            image.padding_x = 0
            image.padding_y = 0

        # padding image to same size to form batches
        # padding is operated at right down side
        #self._padding_batches(images)
        self._cal_padding(images)

        for i, image in enumerate(images) :
            #im = image.im_PIL.convert('RGB') # the image scale and padding are excuted here
            im = image.im_PIL # the image scale and padding are excuted here
            #the_images.append(im) #debug

            pt_im = self._toTensor( im )
            pt_im = self._normalizeTensor( pt_im )
            data.append(pt_im)

        blobs['data'] = torch.stack(data)
        #blobs['images'] = the_images #debug

    def _sample_one_box(self, box, im_width, im_height):
        sampled_boxes = np.zeros((self._sample_box_num,4), dtype=int)
        sampled_boxes[0] = box
        for i in range(1, self._sample_box_num):
            scale_x, scale_y, shift_x, shift_y = np.random.random_sample(4)
            scale_x = self._scale_min + scale_x*(self._scale_max - self._scale_min)
            scale_y = self._scale_min + scale_y*(self._scale_max - self._scale_min)
            w = box[2] - box[0]
            h = box[3] - box[1]

            x0 = max(0, int((1 - scale_x) * shift_x * w + box[0]))
            x1 = min(int(x0 + scale_x * w), im_width-1)
            y0 = max(0, int((1 - scale_y) * shift_y * h + box[1]))
            y1 = min(int(y0 + scale_y * h), im_height-1)
            sampled_boxes[i] = np.array([x0,y0,x1,y1], dtype=int)
        return sampled_boxes
            

    def _sample_boxes(self, image):
        ori_boxes = image.gtboxes
        im_width = image.width
        im_height = image.height
        self._scale_max = 1.15
        self._scale_min = 0.85
        sampled_boxes = []
        for box in ori_boxes: # (x1, y1, x2, y2)
            sampled_boxes.append(self._sample_one_box(box, im_width, im_height))
        return np.concatenate(sampled_boxes, axis=0).astype(np.float32)

    def _add_groundtruth(self, images, blobs, training):
        boxes = []
        cat_labels = [] # category labels

        #gtbatches = [[],[]]

        for i,image in enumerate( images ) :
            if self._sample_box_num > 1:
                im_boxes = self._sample_boxes(image)
                im_labels = image.gtlabels.reshape((-1,1))
                im_labels = np.ones((len(im_labels),self._sample_box_num), dtype=int) * im_labels
                im_labels = im_labels.reshape(-1)
            else:
                im_boxes = image.gtboxes
                im_labels = image.gtlabels.reshape((-1))

            if len( im_boxes ) > 0 :
                boxes.append(torch.from_numpy(im_boxes))
                cat_labels.append(im_labels)

        cat_labels = np.concatenate(cat_labels).astype(int) 
        cat_labels = torch.from_numpy(cat_labels) -1

        blobs['boxes'] = boxes
        blobs['cat_labels'] = cat_labels

    def _add_pair_labels(self, images, blobs):
        image_num = len(images)
        assert image_num%2 == 0
        box_base_ind1=0
        box_base_ind2=0
        pos_pairs = []
        neg_pairs = []
        for i in range(int(image_num/2)):
            im1 = images[2*i]
            im2 = images[2*i+1]
            box_base_ind2 = box_base_ind2 + len(im1.gtboxes)*self._sample_box_num
            assert im1.pair_id == im2.pair_id
            for j, style1 in enumerate(im1.styles):
                for k, style2 in enumerate(im2.styles):
                    temp_pairs = []
                    for n in range(self._sample_box_num):
                        index1 = j*self._sample_box_num + box_base_ind1 + n
                        index2 = k*self._sample_box_num + box_base_ind2 + n
                        index_pair = (index1, index2)
                        temp_pairs.append(index_pair)
                    if style1 > 0 and style1 == style2:
                        pos_pairs.extend(temp_pairs)
                    else:
                        neg_pairs.extend(temp_pairs)
            box_base_ind1 = box_base_ind2 + len(im2.gtboxes)*self._sample_box_num
            box_base_ind2 = box_base_ind1

        if len(neg_pairs) > 3*len(pos_pairs):
            neg_pairs = random.sample(neg_pairs, 3*len(pos_pairs))
        pos_labels = np.ones(len(pos_pairs), dtype=int)
        neg_labels = np.zeros(len(neg_pairs), dtype=int)
        if pos_pairs == []:
            pair_indexes = np.array(neg_pairs, dtype=int)
            pair_labels = neg_labels
        elif neg_pairs ==[]:
            pair_indexes = np.array(pos_pairs, dtype=int)
            pair_labels = pos_labels
        else:
            pair_indexes = np.concatenate((pos_pairs, neg_pairs), axis=0)
            pair_labels = np.concatenate((pos_labels, neg_labels))
        blobs['pair_indexes'] = torch.from_numpy(pair_indexes)
        blobs['pair_labels'] = torch.from_numpy(pair_labels)

    def get_blobs( self, images, training, sample_box_num=8 ):
        self._sample_box_num=sample_box_num
        blobs = {}

        # Converting the data to the propper format
        self._add_data( images, blobs, training )
        self._add_groundtruth( images, blobs, training)
        self._add_pair_labels(images, blobs)

        inputs = {}
        inputs['data'] = blobs['data']
        inputs['boxes'] = blobs['boxes']
        inputs['pair_indexes'] = blobs['pair_indexes']
        #inputs['images'] = blobs['images'] #debug

        targets = {}
        targets['category'] = blobs['cat_labels']
        targets['pair_labels'] = blobs['pair_labels']

        return inputs, targets
