import numpy as np
import h5py
import torch

class imageset_hdf5 :
    def __init__( self, cfg, dataset, randomize=False, is_training=False ):
        self._cfg = cfg
        self._dataset = dataset
        self._batchsize = cfg.dnn.NETWORK.BATCH_SIZE
        self._path = cfg.path.HDF5 % ( dataset.hash )
        self._file = None
        self._randomize = randomize
        self._is_training = is_training

        self.init()

    @property
    def file( self ):
        if self._file is None :
            self._file = h5py.File( self._path, 'r' )
        return self._file

    def init( self ):
        file = self.file
        ndata = file['anchor_data'].shape[0]

        inds = np.arange(0,ndata)

        if self._randomize :
            np.random.shuffle( inds )

        self._chunks = [ np.sort(inds[i:i+self._batchsize]) for i in np.arange(0,ndata,self._batchsize) ]
        self._cur = 0

    def _fix_batches( self, roibatches ):
        roibatches = roibatches.ravel()

        for i in range(len(roibatches)) :
            roibatches[i] = i

        return roibatches.reshape((-1,1)).astype(np.int32)

    def __len__( self ):
        return len(self._chunks)

    def __getitem__( self, idx ):
        c = self._chunks[idx].tolist()
        file = self.file

        out = {}
        out['inputs'] = {}
        out['inputs']['anchor_data'] = torch.from_numpy(file['anchor_data'][c])
        out['inputs']['pos_data'] = torch.from_numpy(file['pos_data'][c])
        out['inputs']['neg_data'] = torch.from_numpy(file['neg_data'][c])
        #out['inputs']['roibatches'] = self._fix_batches(file['roibatches'][c])

        out['targets'] = {}
        #out['targets']['keypoints'] = file['keypoints'][c]

        return out['inputs'], out['targets']

    def next( self ):
        if self._cur >= len(self._chunks):
            self._cur = 0
        blobs = self[self._cur]
        self._cur = self._cur+1
        return blobs