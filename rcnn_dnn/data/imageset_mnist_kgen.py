import os
import gzip
import numpy as np
from .mnist_blobs import mnist_blobs
from .mnist_pair_blobs import mnist_pair_blobs
from .imageset_mnist_pair import imageset_mnist_pair

import torchvision.transforms as transforms
import itertools
import random

class imageset_mnist_kgen( imageset_mnist_pair ):
    def __init__(self, cfg, k_list, is_training=True):
        self._k_list = k_list
        self._root_path = os.path.join(cfg.dataset['ROOT'], 'datasets/mnist')
        self._batchsize = cfg.dnn.NETWORK.BATCH_SIZE
        self._training = is_training

        self._train_images, self._train_labels = self.load_mnist(self._root_path, kind='train')
        #self._mean = self._train_images.mean()
        #self._std = self._train_images.std()
        self._blob_gen = mnist_blobs(cfg)
        if is_training:
            self._images = self._train_images
            self._cat_labels = self._train_labels
        else:
            self._images, self._cat_labels = self.load_mnist(self._root_path, kind='t10k')
        self._prepare()
    
    def _prepare(self):
        self.make_label_lists()
        self.sample_list()
                
        self._chunks = [np.array(x) for x in self.out_ind]
        self._cur = 0

    def make_label_lists(self):
        self._label_list = []
        for i in range(10):
            self._label_list.append([])
        for i, label in enumerate(self._cat_labels):
            self._label_list[label].append(i)

    def sample_list(self):
        self.out_ind = []
        for k in self._k_list:
            self.out_ind.append([])
            for i in range(10):
                inds = random.sample(self._label_list[i], k)
                self.out_ind[-1].append(inds)

    def __len__(self):
        return len(self._chunks)

    def __getitem__(self, idx):
        ims = self._images[self._chunks[idx]].reshape((-1,1,28,28))
        cat_labels = self._cat_labels[self._chunks[idx]]
        inputs, targets = self._blob_gen.get_blobs(ims, cat_labels)

        out = {}
        out['inputs']={}
        out['inputs'] = inputs

        out['targets'] = {}
        out['targets'] = targets

        return out['inputs'], out['targets']

    def next(self):
        if self._cur >= len(self):
            self._cur=0
        blobs = self[self._cur]
        self._cur = self._cur + 1
        return blobs

    def load_mnist(self, path, kind='train'):
        """Load MNIST data from `path`"""
        labels_path = os.path.join(path,
                                '%s-labels-idx1-ubyte.gz'
                                % kind)
        images_path = os.path.join(path,
                                '%s-images-idx3-ubyte.gz'
                                % kind)

        with gzip.open(labels_path, 'rb') as lbpath:
            labels = np.frombuffer(lbpath.read(), dtype=np.uint8,
                                offset=8)

        with gzip.open(images_path, 'rb') as imgpath:
            images = np.frombuffer(imgpath.read(), dtype=np.uint8,
                                offset=16).reshape(len(labels), 784)

        return images, labels
