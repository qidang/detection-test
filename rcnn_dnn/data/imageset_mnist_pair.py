import os
import gzip
import numpy as np
from .mnist_blobs import mnist_blobs
from .mnist_pair_blobs import mnist_pair_blobs

import torchvision.transforms as transforms
import itertools
import random

class imageset_mnist_pair:
    def __init__(self, cfg, randomize=False, is_training=True, sample_num_pos=-1):
        self._randomize = randomize
        self._sample_num_pos = sample_num_pos
        self._training = is_training
        self._root_path = os.path.join(cfg.dataset['ROOT'], 'datasets/fashion_mnist')
        self._batchsize = cfg.dnn.NETWORK.BATCH_SIZE

        self._train_images, self._train_labels = self.load_mnist(self._root_path, kind='train')
        #self._mean = self._train_images.mean()
        #self._std = self._train_images.std()
        self._blob_gen = mnist_pair_blobs(cfg)
        if is_training:
            self._images = self._train_images
            self._cat_labels = self._train_labels
        else:
            self._images, self._cat_labels = self.load_mnist(self._root_path, kind='t10k')
        self._prepare()
    
    def _prepare(self):
        self.make_label_lists()
        self.make_posi_pairs(sample_num=self._sample_num_pos)
        self.make_neg_pairs()
        pos_labels = np.ones_like(self._pos_pairs)
        self._pos_pairs_with_label = np.concatenate((self._pos_pairs, pos_labels),axis=-1)

        neg_labels = np.zeros_like(self._neg_pairs)
        self._neg_pairs_with_label = np.concatenate((self._neg_pairs, neg_labels),axis=-1)
        #print('neg shape: ',self._neg_pairs_with_label.shape)
        #print('pos shape: ',self._pos_pairs_with_label.shape)

        self._pairs = np.concatenate((self._neg_pairs_with_label, self._pos_pairs_with_label), axis = 0)
        npair = len(self._pairs)

        #print('shape of pairs is: ', self._pairs.shape)
        #self._pairs = np.array(self._pairs)
        if self._randomize:
            np.random.shuffle(self._pairs)
        #rand_pairs = np.random.choice(np.arange(npair), size=100)
        #for pair in rand_pairs:
        #    if ori_pairs[pair] not in self._pairs:
        #        print('something is wrong')
                
        self._labels = self._pairs[:,2:] # labels for positive/negative pairs
        self._pairs = self._pairs[:,:2]
        self._labels = self._labels.flatten()
        for i in range(int(len(self._labels)/2)):
            if self._labels[2*i] != self._labels[2*i+1]:
                print('pair labels have problems')
        self._pairs = self._pairs.flatten()

        inds = self._pairs
        ndata = len(inds)
        inds_label = np.arange(ndata)
        batchsize = self._batchsize
        
        #if self._randomize:
        #    np.random.shuffle(inds)
        
        self._chunks = [inds[i:i+batchsize] for i in np.arange(0, ndata, batchsize)]
        self._label_chunks = [inds_label[i:i+batchsize] for i in np.arange(0, ndata, batchsize)]
        self._cur = 0

    def make_label_lists(self):
        self._label_list = []
        for i in range(10):
            self._label_list.append([])
        for i, label in enumerate(self._cat_labels):
            self._label_list[label].append(i)

    def make_posi_pairs(self, sample_num = 10000):
        self._pos_pairs=[]
        for i in range(10):
            self._pos_pairs.extend(list(itertools.combinations(self._label_list[i], 2)))
        self._pos_pairs = np.array(self._pos_pairs)
        #print('pos pair shape is ', self._pos_pairs.shape)
        rand_index = np.random.choice(self._pos_pairs.shape[0], sample_num, replace=False)
        self._pos_pairs = self._pos_pairs[rand_index,:]
        #print('pos pair shape is ', self._pos_pairs.shape)
        #self._pos_pairs = np.random.choice(self._pos_pairs, sample_num, replace=False)

    def make_neg_pairs(self, ratio=1):
        neg_num = int(ratio*len(self._pos_pairs))
        diff_pair_ind = list(itertools.combinations(range(10),2))
        self._neg_pairs=[]
        for i in range(neg_num):
            ind = random.choice(diff_pair_ind)
            ind1 = random.choice(self._label_list[ind[0]])
            ind2 = random.choice(self._label_list[ind[1]])
            self._neg_pairs.append([ind1, ind2])
        self._neg_pairs = np.array(self._neg_pairs)

        

    def __len__(self):
        return len(self._chunks)

    def __getitem__(self, idx):
        ims = self._images[self._chunks[idx]].reshape((-1,1,28,28))
        cat_labels = self._cat_labels[self._chunks[idx]]
        pair_labels = self._labels[self._label_chunks[idx]]
        inputs, targets = self._blob_gen.get_blobs(ims, pair_labels, cat_labels)

        out = {}
        out['inputs']={}
        out['inputs'] = inputs

        out['targets'] = {}
        out['targets'] = targets

        return out['inputs'], out['targets']

    def next(self):
        if self._cur >= len(self):
            self._cur=0
        blobs = self[self._cur]
        self._cur = self._cur + 1
        return blobs

    def load_mnist(self, path, kind='train'):
        """Load MNIST data from `path`"""
        labels_path = os.path.join(path,
                                '%s-labels-idx1-ubyte.gz'
                                % kind)
        images_path = os.path.join(path,
                                '%s-images-idx3-ubyte.gz'
                                % kind)

        with gzip.open(labels_path, 'rb') as lbpath:
            labels = np.frombuffer(lbpath.read(), dtype=np.uint8,
                                offset=8)

        with gzip.open(images_path, 'rb') as imgpath:
            images = np.frombuffer(imgpath.read(), dtype=np.uint8,
                                offset=16).reshape(len(labels), 784)

        return images, labels
