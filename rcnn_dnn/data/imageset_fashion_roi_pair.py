import numpy as np
import h5py
import torch
import random
import time
import datetime
from data import imageset


class imageset_fashion_roi_pair(imageset) :
    def __init__( self, cfg, dataset, blob_gen, randomize=True, is_training=False, benchmark=None, random_sample_num = -1, random_mirrow=False ):
        self._cfg = cfg
        self._dataset = dataset
        self._blob_gen = blob_gen
        self._images = dataset.images
        self._batchsize = cfg.dnn.NETWORK.BATCH_SIZE
        self._file = None
        self._randomize = randomize
        self._is_training = is_training
        self._benchmark = benchmark
        self._random_sample_num = random_sample_num
        self._random_mirror = random_mirrow

        self._prepare()

    def _make_dicts(self):
        if hasattr(self, '_pair_dict') and hasattr(self, '_pair_ids') and hasattr(self,'_item_dict'):
            #print('pair_dicts was prepared')
            return
        self._pair_dict = {'shop':{}, 'user':{}} # 
        self._pair_ids = [] # a dict to record all the ids
        self._item_dict = {'shop':[], 'user':[]}
        for ind, image in enumerate(self._images):
            source = image.source
            self._item_dict[source].append(ind)
            pair_id = image.pair_id
            if pair_id not in self._pair_ids:
                self._pair_ids.append(pair_id)
            if pair_id not in self._pair_dict[source]:
                self._pair_dict[source][pair_id]={}
            for style in image.styles:
                if style not in self._pair_dict[source][pair_id]:
                    self._pair_dict[source][pair_id][style] = []
                self._pair_dict[source][pair_id][style].append(ind)

    def _make_pos_pairs(self):
        if hasattr(self, '_pos_pairs'):
            #print('pos_pairs was prepared')
            return
        self._pos_pairs = []
        for pair_id in self._pair_ids:
            if pair_id not in self._pair_dict['user']:
                print('pair id {} is not available in user data'.format(pair_id))
                continue
            for style in self._pair_dict['shop'][pair_id]:
                if style == 0 or style not in self._pair_dict['user'][pair_id]:
                    continue
                shop_items = self._pair_dict['shop'][pair_id][style]
                user_items = self._pair_dict['user'][pair_id][style]
                for shop_item in shop_items:
                    for user_item in user_items:
                        self._pos_pairs.append([shop_item, user_item ])
    
    def _make_neg_pairs(self, times=3):
        total_num = times*len(self._pos_pairs)
        self._neg_pairs = []
        cur_num = 0
        while cur_num < total_num:
            shop_ind = random.choice(self._item_dict['shop'])
            user_ind = random.choice(self._item_dict['user'])
            if self._images[shop_ind].pair_id != self._images[user_ind].pair_id:
                self._neg_pairs.append([shop_ind, user_ind])
            cur_num+=1

                
    def _prepare(self):
        self._make_dicts()
        self._make_pos_pairs()

        self._pairs = np.array(self._pos_pairs)

        npair = len(self._pairs)
        if self._random_sample_num > 0 and self._random_sample_num < npair:
            ind = np.random.choice(npair, self._random_sample_num, replace=False)
            self._pairs = self._pairs[ind,:]

        if self._randomize:
            np.random.shuffle(self._pairs)

        self._pairs = self._pairs.flatten()

        inds = self._pairs
        ndata = len(inds)
        inds_label = np.arange(ndata)
        batchsize = self._batchsize
        
        
        self._chunks = [inds[i:i+batchsize] for i in np.arange(0, ndata, batchsize)]
        self._label_chunks = [inds_label[i:i+batchsize] for i in np.arange(0, ndata, batchsize)]
        self._cur = 0


    def _fix_batches( self, roibatches ):
        roibatches = roibatches.ravel()

        for i in range(len(roibatches)) :
            roibatches[i] = i

        return roibatches.reshape((-1,1)).astype(np.int32)

    def __len__( self ):
        return len(self._chunks)

    def __getitem__( self, idx ):
        images = self._images[self._chunks[idx]]
        blobs = self._blob_gen.get_blobs(images, training=self._is_training, sample_box_num=1)

        return blobs

    def next( self ):
        if self._cur >= len(self._chunks):
            self._cur = 0
            # regenerate the input pairs
            self._prepare()
        blobs = self[self._cur]
        self._cur = self._cur+1
        return blobs

    #just for debug
    def pair_checking(self):
        data_len = len(self._pairs)
        rand_ind = np.random.randint(data_len)
        if rand_ind%2 == 1:
            rand_ind = rand_ind - 1
        ind1 = self._pairs[rand_ind]
        ind2 = self._pairs[rand_ind+1]
        image1 = self._images[ind1].im_PIL
        image2 = self._images[ind2].im_PIL
        image1.save('test_im/im1.jpg')
        image2.save('test_im/im2.jpg')