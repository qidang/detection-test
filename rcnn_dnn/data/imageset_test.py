import pickle
import torch
from PIL import Image
import os
from torchvision.transforms import ToTensor
import numpy as np
import torch

_img_types = ['jpg', 'png']
class TestDataset():
    def __init__(self, pic_root, transforms=None):
        '''COCO Dataset for detection'''
        self.images = os.listdir(pic_root)
        self.images.sort()
        for img in self.images:
            img_type = img.split('.')[-1]
            if img_type not in _img_types:
                print('remove file {} from list'.format(img))
                self.images.remove(img)

        self.pic_root = pic_root
        
        if transforms==None:
            self._transforms = ToTensor()


    def __getitem__(self, idx):
        img_path = os.path.join(self.pic_root, self.images[idx])
        img = Image.open(img_path).convert('RGB')
        img = self._transforms(img)
        target = self.images[idx]

        return img, target

    def __len__(self):
        return len(self.images)