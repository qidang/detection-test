from torchvision.transforms import ToTensor
from torchcore.data.transforms import GeneralRCNNTransform
import numpy as np
import torch
def collate_fn(batch):
    images, targets = [list(s) for s in zip(*batch)]
    return images, targets

def collate_fn_test(batch):
    images = []
    targets = []
    for input_item in batch:
        images.append(input_item[0])
        target = input_item[1]
        if target is not None:
            targets.append(target)
    if len(targets) == 0:
        targets=None

    return images, targets

class CollateFnRCNN(object):
    '''apply general rcnn transform to batchs'''
    def __init__(self, min_size, max_size, image_mean=None, image_std=None):
        if isinstance(min_size, (list, tuple)):
            self.transforms = [GeneralRCNNTransform(min_size_i, max_size, image_mean=image_mean, image_std=image_std) for min_size_i in min_size]
            self.transform_num = len(min_size)
            self.multi_scale = True
        else:
            self.transforms = GeneralRCNNTransform(min_size, max_size,  
                                               image_mean=image_mean, image_std=image_std)
            self.multi_scale = False

    def __call__(self, batch):
        inputs, targets = [list(s) for s in zip(*batch)]
        ori_image = [input['ori_image'] for input in inputs]
        if self.multi_scale:
            i = np.random.randint(self.transform_num)
            transform = self.transforms[i]
        else:
            transform = self.transforms
        inputs, targets = transform(inputs, targets)
        inputs['ori_image'] = ori_image
        return inputs, targets

def collate_fn_hdf5(batch):
    inputs, targets = [list(s) for s in zip(*batch)]
    images = []
    image_sizes= []
    scales = np.zeros(len(inputs))
    im_ids = []
    mirrored = []

    for i, ainput in enumerate(inputs):
        images.append(ainput['data'])
        image_sizes.append(ainput['image_sizes'])
        scales[i] = ainput['scale']
        mirrored.append(ainput['mirrored'])

    images = torch.stack(images)
    inputs = {}
    inputs['data'] = images
    inputs['scale'] = scales
    inputs['mirrored'] = mirrored
    inputs['image_sizes'] = image_sizes
    return inputs, targets


def collate_fn_modahuman(batch):
    images = []
    targets = {}
    im_ids = []
    #print('batch 0: ', batch[0])
    for inputs, target in batch:
        images.append(ToTensor()(inputs['data']))
        im_ids.append(target['image_id'])
    targets['image_id'] = im_ids
    return images, targets