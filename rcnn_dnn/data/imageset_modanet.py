import pickle
import torch
from PIL import Image
import os
from torchvision.transforms import ToTensor
import numpy as np
import torch

class ModanetDataset():
    def __init__(self, anno_path, part, root, transforms=None):
        '''COCO Dataset for detection'''
        self._root = root # root folder for image
        self._part = part
        # load annotations
        with open(anno_path, 'rb') as f:
            self._images = pickle.load(f)[part] 
        
        self._transforms = transforms
        
        self.to_tensor = ToTensor()


    def __getitem__(self, idx):
        image = self._images[idx]
        img_path = os.path.join(self._root, 'val', image['file_name'] )
        image_id=image['id']
        img = Image.open(img_path).convert('RGB')
        #img = self._transforms(img)
        boxes = []
        labels = []
        for obj in image['objects']:
            # convert the bbox from xywh to xyxy
            obj['bbox'][2]+=obj['bbox'][0]
            obj['bbox'][3]+=obj['bbox'][1]
            boxes.append(obj['bbox'])
            labels.append(obj['category_id'])
        boxes = np.array(boxes, dtype=np.float32)
        labels = np.array(labels, dtype=np.int64) 

        #images (list[Tensor]): images to be processed
        #targets (list[Dict[Tensor]]): ground-truth boxes present in the image (optional)
        targets = {}
        targets["boxes"] = boxes
        targets["labels"] = labels
        #target["masks"] = masks
        targets["image_id"] = np.array(image_id)
        #target["area"] = area
        #target["iscrowd"] = iscrowd

        if self._transforms is not None:
            img, targets = self._transforms(img, targets)


        img = self.to_tensor(img)

        for item in targets:
            targets[item] = torch.from_numpy(targets[item])
        return img, targets

    def __len__(self):
        return len(self._images)