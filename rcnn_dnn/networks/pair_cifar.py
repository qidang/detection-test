import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

from dnn.networks import networks
from dnn.networks import common as networks_common
from tools import Logger
import datetime
from torchvision.models import vgg16_bn, resnet50

class Match_Head(nn.Module):
    def _init_weights( self ):
        for name, m in self.named_modules() :
            #if len(name)>0 and name.split('.')[0] not in self._child_networks :
            networks_common.init(m)

    def __init__(self, feature_dim):
        super().__init__()
        self.mn_conv1 = nn.Conv2d(2048, 512, 1)
        self.mn_conv2 = nn.Conv2d(512, 512, 1)
        self.mn_fc1 = nn.Linear(512, 512)
        self.mn_fc2 = nn.Linear(512, feature_dim)
        self._init_weights()
        #self.mn_fc = nn.Linear(7*7*128,256)

    def forward(self, x):
        x=F.relu(self.mn_conv1(x))
        x=F.relu(self.mn_conv2(x))
        x = x.view(-1, 512)

        x = F.relu(self.mn_fc1(x))
        x = self.mn_fc2(x)
        return x

class PairEasyNet( nn.Module):
    def _init_weights( self ):
        for name, m in self.named_modules() :
            if len(name)>0 and name.split('.')[0] not in self._child_networks :
                networks_common.init(m)

    def __init__( self, cfg ):
        super().__init__()
        self._cfg = cfg
        self._batch_size = cfg.dnn.NETWORK.BATCH_SIZE
        self._feature_dim = cfg.dnn.NETWORK.FEATURE_DIM
        self._pair_batch_size = int(cfg.dnn.NETWORK.BATCH_SIZE / 2)

        #self.feature = networks.feature['EasyNetD']( feature=False, RGB=True )
        # resnet feature
        resnet = resnet50(pretrained=True)
        modules = list(resnet.children())[:-1]      # delete the last fc layer.
        resnet = nn.Sequential(*modules)
        self.feature = resnet
        #self.feature = vgg16_bn(pretrained=True).features
        self._child_networks = ['feature']

        #self.dropout = nn.Dropout(p=0.5)
        self.fc1 = nn.Linear(2048, 10)

        # Match Net
        self.mn_head = Match_Head(self._feature_dim)
        self.mn_fc = nn.Linear(self._feature_dim, 2)
        self._init_weights()

    def forward(self, inputs, just_embedding=False):
        x_f = self.feature(inputs['data'])

        # match

        x_embedding = self.mn_head(x_f)

        if just_embedding:
            return {'embeddings': x_embedding}
        x1 = x_embedding[:self._pair_batch_size,:]
        x2 = x_embedding[self._pair_batch_size:,:]
        #diff = torch.pow(x2-x1, 2)
        diff = torch.abs(x2-x1)
        #diff = torch.pow(x2-x1,2)*10000
        match_out = self.mn_fc(diff)

        #category
        x = x_f.view(-1, 2048)
        #x = F.relu(self.fc1(x))
        x= self.fc1(x)

        outputs = {}
        outputs['category'] = x
        outputs['match'] = match_out
        outputs['embeddings'] = x_embedding
        #outputs['l1'] = l1
        return outputs

class Loss( nn.Module ) :
    def __init__( self, cfg ):
        super().__init__()
        self._cfg = cfg
        self.cat_criterion = nn.CrossEntropyLoss()
        self.match_criterion = nn.CrossEntropyLoss()

    def __call__( self, outputs, targets):
        losses = {}
        #losses['category'] = self.cat_criterion(outputs['category'], targets['category'])
        losses['match'] = self.match_criterion(outputs['match'], targets['pair_labels'])
        return losses

class Benchmark :
    def __init__( self, cfg, criterias=['category'] ):
        self._criterias = criterias
        self._cfg = cfg
        if 'match' in criterias:
            self._match_total = 0
            self._match_correct =  0

        if 'category' in criterias:
            self._total = 0
            self._correct = 0
        self.init_logger()

    def init_logger(self):
        bench_path = self._cfg.path['BENCHMARK_LOG']

        console_formatter = '{} {{}}'.format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        self._logger = Logger(level='info', file=bench_path, console=False, console_formatter=console_formatter)

        #self._logger.info('benchmark log path is: {}'.format(bench_path))
        print('The benchmark log path is {}'.format(bench_path))

    def update( self, targets, pred ):
        if 'category' in self._criterias:
            cat = targets['category'].detach().numpy()
            pre = pred['category'].detach().cpu().numpy()
            cls = np.argmax( pre, axis=1 ).ravel()

            total = len(cls)
            correct = len(np.where( cat == cls )[0])

            self._total = self._total + total
            self._correct = self._correct + correct
        if 'match' in self._criterias:
            match_label = targets['pair_labels'].detach().numpy()
            pre = pred['match'].detach().cpu().numpy()
            cls = np.argmax( pre, axis=1 ).ravel()
            total = len(cls)
            correct = len(np.where(match_label == cls)[0])
            self._match_total += total
            self._match_correct += correct

    def summary( self ):
        if 'match' in self._criterias:
            accuracy = float(self._match_correct)/ self._match_total
            self._logger.info('{} '.format(accuracy))
            print('Match Accuracy : {}'.format(accuracy))
        if 'category' in self._criterias:
            accuracy = float(self._correct)/self._total
            self._logger.info('{} '.format(accuracy))
            print('Category Accuracy : {}'.format(accuracy) )

        if 'match' in self._criterias:
            self._match_total = 0
            self._match_correct =  0

        if 'category' in self._criterias:
            self._total = 0
            self._correct = 0