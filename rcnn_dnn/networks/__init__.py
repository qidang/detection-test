from easydict import EasyDict as edict
from . import category_patch
from . import category_embedding
from . import fashion_rcnn
from . import pair_patch
#from . import category_gtbox
from . import category_easy
from . import category_cifar
from . import pair_mnist
from . import pair_cifar
from . import triplet_mnist
from . import triplet_cifar
from . import triplet_fashion_patch
from . import pair_roi
from . import yolo

networks = edict()
networks.yolo = edict()
networks.yolo.net = yolo.YOLONet
networks.yolo.loss = yolo.YOLOLoss

networks.category_patch = edict()
networks.category_patch.net = category_patch.Net
networks.category_patch.loss = category_patch.Loss
networks.category_patch.benchmark = category_patch.Benchmark

networks.category_embedding = edict()
networks.category_embedding.net = category_embedding.Net
networks.category_embedding.testnet = category_embedding.EmbeddingNet
networks.category_embedding.loss = category_embedding.Loss
networks.category_embedding.benchmark = category_embedding.Benchmark
#networks.category_gtbox = edict()
#networks.category_gtbox.net = category_gtbox.Net
#networks.category_gtbox.loss = category_gtbox.Loss
#networks.category_gtbox.benchmark = category_gtbox.Benchmark
networks.fashion_rcnn = edict()
networks.fashion_rcnn.loss = fashion_rcnn.Fashion_RCNN

networks.pair_patch = edict()
networks.pair_patch.net = pair_patch.PairNet
networks.pair_patch.loss = pair_patch.Loss

networks.category_easy = edict()
networks.category_easy.net = category_easy.EasyNet
networks.category_easy.loss = category_easy.Loss

networks.category_cifar = edict()
networks.category_cifar.net = category_cifar.EasyNet
networks.category_cifar.loss = category_cifar.Loss

networks.pair_mnist = edict()
networks.pair_mnist.net = pair_mnist.PairEasyNet
networks.pair_mnist.loss = pair_mnist.Loss

networks.pair_cifar = edict()
networks.pair_cifar.net = pair_cifar.PairEasyNet
networks.pair_cifar.loss = pair_cifar.Loss

networks.triplet_mnist = edict()
networks.triplet_mnist.net = triplet_mnist.TripletEasyNet
networks.triplet_mnist.loss = triplet_mnist.Loss

networks.triplet_cifar = edict()
networks.triplet_cifar.net = triplet_cifar.TripletNet
networks.triplet_cifar.loss = triplet_cifar.Loss

networks.triplet_fashion = edict()
networks.triplet_fashion.net = triplet_fashion_patch.TripletNet
networks.triplet_fashion.loss = triplet_fashion_patch.Loss

networks.pair_roi = edict()
networks.pair_roi.net = pair_roi.PairNet
networks.pair_roi.loss = pair_roi.Loss