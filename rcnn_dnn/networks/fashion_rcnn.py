import torch 
from torch import nn
from torchvision.models.detection.rpn import RegionProposalNetwork, AnchorGenerator, RPNHead
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor, TwoMLPHead
from torchvision.models.detection.roi_heads import RoIHeads
from torchvision.ops.roi_align import RoIAlign
from torchvision.ops import MultiScaleRoIAlign
from collections import OrderedDict
import torchvision

class Fashion_RCNN(nn.Module):
    def __init__(self, cfg):
        super().__init__()
        self._cfg = cfg
        self._faster_rcnn = Faster_RCNN(cfg) #TODO: add faster rcnn parameters
        self.training = True #TODO: change here to parameter

    def forward(self, inputs, targets):
        losses = {}
        # Faster RCNN part
        for i in range(1,3):
            images = inputs['data{}'.format(i)]
            tmp_targets = targets['ground_truth{}'.format(i)]
            if self.training:
                tmp_loss = self._faster_rcnn(images, tmp_targets)
                # change the name for keys
                for key in tmp_loss.keys():
                    tmp_loss['{}_{}'.format(key, i)] = tmp_loss.pop(key)
                losses.update(tmp_loss)
        
        # Pair wise part

        return losses




class Faster_RCNN(nn.Module):
    def __init__(self, cfg ):
        super().__init__()
        self._cfg = cfg
        #self.backbone = torchvision.models.vgg16(pretrained=True).features
        out_channels = 512
        num_classes = 13
        resnet_children = list(torchvision.models.resnet50(pretrained=True).children())[:-1]
        resnet_feature = nn.Sequential(*resnet_children)
        self.backbone = resnet_feature

        #init rpn
        rpn_anchor_generator = AnchorGenerator(sizes=(128,256, 512), aspect_ratios=(0.5, 1.0, 2.0))
        rpn_head = RPNHead(out_channels, rpn_anchor_generator.num_anchors_per_location()[0]) # initialization can be varies
        rpn_pre_nms_top_n_train=2000
        rpn_pre_nms_top_n_test=1000
        rpn_post_nms_top_n_train=2000
        rpn_post_nms_top_n_test=1000
        rpn_nms_thresh=0.7
        rpn_fg_iou_thresh=0.7
        rpn_bg_iou_thresh=0.3
        rpn_batch_size_per_image=256
        rpn_positive_fraction=0.5
        rpn_pre_nms_top_n = dict(training=rpn_pre_nms_top_n_train, testing=rpn_pre_nms_top_n_test)
        rpn_post_nms_top_n = dict(training=rpn_post_nms_top_n_train, testing=rpn_post_nms_top_n_test)
        self.rpn = RegionProposalNetwork(
            rpn_anchor_generator, rpn_head,
            rpn_fg_iou_thresh, rpn_bg_iou_thresh,
            rpn_batch_size_per_image, rpn_positive_fraction,
            rpn_pre_nms_top_n, rpn_post_nms_top_n, rpn_nms_thresh)
        
        # init box
        stride = 16.0
        sampling_ratio = 1 # can also be 4 in Mask rcnn, -1 for adaptive
        # TODO
        #box_roi_pool=RoIAlign(output_size=7, spatial_scale=1.0/stride, sampling_ratio=sampling_ratio)
        box_roi_pool=None
        box_head=None
        box_predictor=None
        box_score_thresh=0.05
        box_nms_thresh=0.5
        box_detections_per_img=100
        box_fg_iou_thresh=0.5
        box_bg_iou_thresh=0.5
        box_batch_size_per_image=512
        box_positive_fraction=0.25
        bbox_reg_weights=None

        if box_roi_pool is None:
            box_roi_pool = MultiScaleRoIAlign(
                featmap_names=[0, 1, 2, 3],
                output_size=7,
                sampling_ratio=2)

        # init box predictor
        if box_head is None:
            resolution = box_roi_pool.output_size[0]
            print('resolution is {}'.format(resolution))
            representation_size = 1024
            box_head = TwoMLPHead(
                out_channels * resolution ** 2,
                representation_size)

        if box_predictor is None:
            representation_size = 1024
            box_predictor = FastRCNNPredictor(
                representation_size,
                num_classes)

        roi_heads = RoIHeads(
            # Box
            box_roi_pool, box_head, box_predictor,
            box_fg_iou_thresh, box_bg_iou_thresh,
            box_batch_size_per_image, box_positive_fraction,
            bbox_reg_weights,
            box_score_thresh, box_nms_thresh, box_detections_per_img)
        self.roi_heads = roi_heads

    def forward(self, images, targets=None):
        """
        Arguments:
            images (list[Tensor]): images has been preprocessed
            targets (list[Dict[Tensor]]): ground-truth boxes present in the image (optional)

        Returns:
            result (list[BoxList] or dict[Tensor]): the output from the model.
                During training, it returns a dict[Tensor] which contains the losses.
                During testing, it returns list[BoxList] contains additional fields
                like `scores`, `labels` and `mask` (for Mask R-CNN models).

        """
        if self.training and targets is None:
            raise ValueError("In training mode, targets should be passed")
        #original_image_sizes = [img.shape[-2:] for img in images]
        #images, targets = self.transform(images, targets)
        features = self.backbone(images.tensors)
        if isinstance(features, torch.Tensor):
            features = OrderedDict([(0, features)])
        print('features are ready')
        proposals, proposal_losses = self.rpn(images, features, targets)
        print('proposals are ready')
        detections, detector_losses = self.roi_heads(features, proposals, images.image_sizes, targets=targets)
        print('detections are ready')
        #detections = self.transform.postprocess(detections, images.image_sizes, original_image_sizes)

        losses = {}
        losses.update(detector_losses)
        losses.update(proposal_losses)

        if self.training:
            return losses

        return detections