import torch
import torch.nn as nn
import torchvision

class YOLONet(nn.Module):
    def __init__(self, class_num, backbone=None):
        super().__init__()
        if backbone is None:
            self.backbone = resnet50_backbone(pretrained=True)

    def forward(self, inputs, targets=None):
        feature = self.backbone(inputs['data'])
        print(feature.shape)

class YOLOLoss(nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, inputs, targets):
        '''return a dict of loss'''
        loss = {}

        return loss

class yolo_backbone(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3)

    def forward(self, input):
        pass

class resnet50_backbone(nn.Module):
    def __init__(self, pretrained=False):
        super().__init__()
        resnet = torchvision.models.resnet50(pretrained=pretrained)
        modules = list(resnet.children())[:-2]      # delete the last fc layer and the adaptive pool layere.
        self.backbone = nn.Sequential(*modules)

    def forward(self, input):
        return self.backbone(input)
