import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

from dnn.networks import networks
from dnn.networks import common as networks_common
from tools import Logger
import datetime

class EasyNet( nn.Module):
    def _init_weights( self ):
        for name, m in self.named_modules() :
            if len(name)>0 and name.split('.')[0] not in self._child_networks :
                networks_common.init(m)

    def __init__( self, cfg ):
        super().__init__()
        self._cfg = cfg

        self.feature = networks.feature['EasyNetA']( feature=False, RGB=True )
        self._child_networks = ['feature']

        self.dropout = nn.Dropout(p=0.5)
        self.fc1 = nn.Linear(4*4*50, 500)
        self.fc2 = nn.Linear(500, 10)
        #self.fc3 = nn.Linear(84, 10)
        self._init_weights()

    def forward(self, inputs):
        # VGG16
        #print(x.shape)
        x = self.feature(inputs['data'])

        x = x.view(-1, 50*4*4)
        x = F.relu(self.fc1(x))
        x = self.dropout(x)
        #x= F.relu(self.fc2(x))
        x= self.fc2(x)
        x = F.log_softmax(x, dim=1)

        outputs = {}
        outputs['category'] = x
        return outputs

class Loss( nn.Module ) :
    def __init__( self, cfg ):
        super().__init__()
        self._cfg = cfg
        self.criterion = nn.NLLLoss()

    def __call__( self, outputs, targets):
        losses = {}
        losses['category'] = self.criterion(outputs['category'], targets['category'])
        return losses

class Benchmark :
    def __init__( self, cfg ):
        self._cfg = cfg
        self._total = 0
        self._correct = 0
        self.init_logger()

    def init_logger(self):
        bench_path = self._cfg.path['BENCHMARK_LOG']

        console_formatter = '{} {{}}'.format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        self._logger = Logger(level='info', file=bench_path, console=False, console_formatter=console_formatter)

        #self._logger.info('benchmark log path is: {}'.format(bench_path))
        print('The benchmark log path is {}'.format(bench_path))

    def update( self, targets, pred ):
        cat = targets['category'].detach().numpy()
        pre = pred['category'].detach().cpu().numpy()
        cls = np.argmax( pre, axis=1 ).ravel()

        total = len(cls)
        correct = len(np.where( cat == cls )[0])

        self._total = self._total + total
        self._correct = self._correct + correct

    def summary( self ):
        accuracy = float(self._correct)/self._total
        print('Validation Accuracy : %f' % ( accuracy ))
        self._logger.info('{} '.format(accuracy))
        self._total = 0
        self._correct = 0
