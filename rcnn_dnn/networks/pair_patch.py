import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

from dnn.networks import networks
from dnn.networks import common as networks_common
from tools import Logger
import datetime
from torchvision.models import vgg16_bn, resnet50

class Match_Head(nn.Module):
    def _init_weights( self ):
        for name, m in self.named_modules() :
            #if len(name)>0 and name.split('.')[0] not in self._child_networks :
            networks_common.init(m)

    def __init__(self, feature_dim):
        super().__init__()
        self.mn_conv1 = nn.Conv2d(2048, 512, 1)
        self.mn_conv2 = nn.Conv2d(512, 512, 1)
        self.mn_fc1 = nn.Linear(512, 512)
        self.mn_fc2 = nn.Linear(512, feature_dim)
        self._init_weights()
        #self.mn_fc = nn.Linear(7*7*128,256)

    def forward(self, x):
        x=F.relu(self.mn_conv1(x))
        x=F.relu(self.mn_conv2(x))
        x = x.view(-1, 512)

        x = F.relu(self.mn_fc1(x))
        x = self.mn_fc2(x)
        return x

class PairNet( nn.Module):
    def _init_weights( self ):
        for name, m in self.named_modules() :
            if len(name)>0 and name.split('.')[0] not in self._child_networks :
                networks_common.init(m)

    def __init__( self, cfg ):
        super().__init__()
        self._cfg = cfg
        self._batch_size = cfg.dnn.NETWORK.BATCH_SIZE
        self._feature_dim = cfg.dnn.NETWORK.FEATURE_DIM
        self._pair_batch_size = int(cfg.dnn.NETWORK.BATCH_SIZE / 2)
        self._nclasses = cfg.dnn.NETWORK.NCLASSES

        #self.feature = networks.feature['EasyNetD']( feature=False, RGB=True )
        # resnet feature
        resnet = resnet50(pretrained=True)
        modules = list(resnet.children())[:-1]      # delete the last fc layer.
        resnet = nn.Sequential(*modules)
        self.feature = resnet
        #self.feature = vgg16_bn(pretrained=True).features
        self._child_networks = ['feature']

        #self.dropout = nn.Dropout(p=0.5)
        self.fc1 = nn.Linear(2048, self._nclasses)

        # Match Net
        self.mn_head = Match_Head(self._feature_dim)
        self.mn_fc = nn.Linear(self._feature_dim, 2)
        self._init_weights()

    def forward(self, inputs, just_embedding=False):
        x_f = self.feature(inputs['data'])

        x_embedding = self.mn_head(x_f)

        #category
        x = x_f.view(-1, 2048)
        #x = F.relu(self.fc1(x))
        x= self.fc1(x)

        if just_embedding:
            return {'embeddings': x_embedding, 'category': x}
        batch_size_half = int(x_embedding.shape[0] / 2)
        #x1 = x_embedding[:self._pair_batch_size,:]
        #x2 = x_embedding[self._pair_batch_size:,:]
        x1 = x_embedding[:batch_size_half,:]
        x2 = x_embedding[batch_size_half:,:]
        #diff = torch.pow(x2-x1, 2)
        diff = torch.abs(x2-x1)
        #diff = torch.pow(x2-x1,2)*10000
        match_out = self.mn_fc(diff)


        outputs = {}
        outputs['category'] = x
        outputs['match'] = match_out
        outputs['embeddings'] = x_embedding
        #outputs['l1'] = l1
        return outputs

class Loss( nn.Module ) :
    def __init__( self, cfg ):
        super().__init__()
        self._cfg = cfg
        self.cat_criterion = nn.CrossEntropyLoss()
        self.match_criterion = nn.CrossEntropyLoss()

    def __call__( self, outputs, targets):
        losses = {}
        losses['category'] = 0.001*self.cat_criterion(outputs['category'], targets['category'])
        losses['match'] = self.match_criterion(outputs['match'], targets['pair_labels'])
        return losses