import os
import argparse
from torchcore.tools.hdf5_generater import *

#anno_path = os.path.expanduser('~/data/annotations/modanet2018_instances_revised.pkl')
#root = os.path.expanduser('~/data/datasets/modanet/Images')
def parse_argument(args=''):
    parser = argparse.ArgumentParser()
    parser.add_argument('--part', default='train',
            help='The part of dataset, can be val or train')
    parser.add_argument('--width', type=int, default=128,
            help='the width of the image')
    cfg = parser.parse_args()
    return cfg

if __name__=='__main__':
    cfg = parse_argument()
    part = cfg.part
    width = cfg.width
    height = width*2
    out_size = (width, height)
    human_det_path = 'modanet_human_{}.pkl'.format(part)
    anno_path = '/ssd/data/annotations/modanet2018_instances_revised.pkl'
    root = '/ssd/data/datasets/modanet/Images'
    h5_out_path = '/ssd/data/datasets/modanet/modanet_{}_{}_{}_hdf5.hdf5'.format(width, height, part)
    if os.path.exists(h5_out_path):
        os.remove(h5_out_path)

    with open(anno_path, 'rb') as f:
        imageset = pickle.load(f)[part]
    with open(human_det_path, 'rb') as f:
        human_detections = pickle.load(f)
    if part == 'val':
        generate_hdf5_patch(h5_out_path, part, human_detections, imageset, root, out_size, expand_rate=0.2, add_mirror=False)
    else:
        generate_hdf5_patch(h5_out_path, part, human_detections, imageset, root, out_size, expand_rate=0.2)
